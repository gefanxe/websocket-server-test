/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
"use strict";

var $protobuf = require("protobufjs/minimal");

// Common aliases
var $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;

// Exported root namespace
var $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});

$root.aslivecasino = (function() {

    /**
     * Namespace aslivecasino.
     * @exports aslivecasino
     * @namespace
     */
    var aslivecasino = {};

    aslivecasino.PlatformInfo = (function() {

        /**
         * Properties of a PlatformInfo.
         * @memberof aslivecasino
         * @interface IPlatformInfo
         * @property {number|null} [ccu] PlatformInfo ccu
         * @property {number|null} [num_rooms] PlatformInfo num_rooms
         */

        /**
         * Constructs a new PlatformInfo.
         * @memberof aslivecasino
         * @classdesc Represents a PlatformInfo.
         * @implements IPlatformInfo
         * @constructor
         * @param {aslivecasino.IPlatformInfo=} [properties] Properties to set
         */
        function PlatformInfo(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * PlatformInfo ccu.
         * @member {number} ccu
         * @memberof aslivecasino.PlatformInfo
         * @instance
         */
        PlatformInfo.prototype.ccu = 0;

        /**
         * PlatformInfo num_rooms.
         * @member {number} num_rooms
         * @memberof aslivecasino.PlatformInfo
         * @instance
         */
        PlatformInfo.prototype.num_rooms = 0;

        /**
         * Creates a new PlatformInfo instance using the specified properties.
         * @function create
         * @memberof aslivecasino.PlatformInfo
         * @static
         * @param {aslivecasino.IPlatformInfo=} [properties] Properties to set
         * @returns {aslivecasino.PlatformInfo} PlatformInfo instance
         */
        PlatformInfo.create = function create(properties) {
            return new PlatformInfo(properties);
        };

        /**
         * Encodes the specified PlatformInfo message. Does not implicitly {@link aslivecasino.PlatformInfo.verify|verify} messages.
         * @function encode
         * @memberof aslivecasino.PlatformInfo
         * @static
         * @param {aslivecasino.IPlatformInfo} message PlatformInfo message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        PlatformInfo.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.ccu != null && message.hasOwnProperty("ccu"))
                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.ccu);
            if (message.num_rooms != null && message.hasOwnProperty("num_rooms"))
                writer.uint32(/* id 2, wireType 0 =*/16).int32(message.num_rooms);
            return writer;
        };

        /**
         * Encodes the specified PlatformInfo message, length delimited. Does not implicitly {@link aslivecasino.PlatformInfo.verify|verify} messages.
         * @function encodeDelimited
         * @memberof aslivecasino.PlatformInfo
         * @static
         * @param {aslivecasino.IPlatformInfo} message PlatformInfo message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        PlatformInfo.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a PlatformInfo message from the specified reader or buffer.
         * @function decode
         * @memberof aslivecasino.PlatformInfo
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {aslivecasino.PlatformInfo} PlatformInfo
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        PlatformInfo.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.aslivecasino.PlatformInfo();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.ccu = reader.int32();
                    break;
                case 2:
                    message.num_rooms = reader.int32();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a PlatformInfo message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof aslivecasino.PlatformInfo
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {aslivecasino.PlatformInfo} PlatformInfo
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        PlatformInfo.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a PlatformInfo message.
         * @function verify
         * @memberof aslivecasino.PlatformInfo
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        PlatformInfo.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.ccu != null && message.hasOwnProperty("ccu"))
                if (!$util.isInteger(message.ccu))
                    return "ccu: integer expected";
            if (message.num_rooms != null && message.hasOwnProperty("num_rooms"))
                if (!$util.isInteger(message.num_rooms))
                    return "num_rooms: integer expected";
            return null;
        };

        /**
         * Creates a PlatformInfo message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof aslivecasino.PlatformInfo
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {aslivecasino.PlatformInfo} PlatformInfo
         */
        PlatformInfo.fromObject = function fromObject(object) {
            if (object instanceof $root.aslivecasino.PlatformInfo)
                return object;
            var message = new $root.aslivecasino.PlatformInfo();
            if (object.ccu != null)
                message.ccu = object.ccu | 0;
            if (object.num_rooms != null)
                message.num_rooms = object.num_rooms | 0;
            return message;
        };

        /**
         * Creates a plain object from a PlatformInfo message. Also converts values to other types if specified.
         * @function toObject
         * @memberof aslivecasino.PlatformInfo
         * @static
         * @param {aslivecasino.PlatformInfo} message PlatformInfo
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        PlatformInfo.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.ccu = 0;
                object.num_rooms = 0;
            }
            if (message.ccu != null && message.hasOwnProperty("ccu"))
                object.ccu = message.ccu;
            if (message.num_rooms != null && message.hasOwnProperty("num_rooms"))
                object.num_rooms = message.num_rooms;
            return object;
        };

        /**
         * Converts this PlatformInfo to JSON.
         * @function toJSON
         * @memberof aslivecasino.PlatformInfo
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        PlatformInfo.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return PlatformInfo;
    })();

    aslivecasino.UserBaseInfo = (function() {

        /**
         * Properties of a UserBaseInfo.
         * @memberof aslivecasino
         * @interface IUserBaseInfo
         * @property {string|null} [user_name] UserBaseInfo user_name
         * @property {number|null} [balance] UserBaseInfo balance
         */

        /**
         * Constructs a new UserBaseInfo.
         * @memberof aslivecasino
         * @classdesc Represents a UserBaseInfo.
         * @implements IUserBaseInfo
         * @constructor
         * @param {aslivecasino.IUserBaseInfo=} [properties] Properties to set
         */
        function UserBaseInfo(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * UserBaseInfo user_name.
         * @member {string} user_name
         * @memberof aslivecasino.UserBaseInfo
         * @instance
         */
        UserBaseInfo.prototype.user_name = "";

        /**
         * UserBaseInfo balance.
         * @member {number} balance
         * @memberof aslivecasino.UserBaseInfo
         * @instance
         */
        UserBaseInfo.prototype.balance = 0;

        /**
         * Creates a new UserBaseInfo instance using the specified properties.
         * @function create
         * @memberof aslivecasino.UserBaseInfo
         * @static
         * @param {aslivecasino.IUserBaseInfo=} [properties] Properties to set
         * @returns {aslivecasino.UserBaseInfo} UserBaseInfo instance
         */
        UserBaseInfo.create = function create(properties) {
            return new UserBaseInfo(properties);
        };

        /**
         * Encodes the specified UserBaseInfo message. Does not implicitly {@link aslivecasino.UserBaseInfo.verify|verify} messages.
         * @function encode
         * @memberof aslivecasino.UserBaseInfo
         * @static
         * @param {aslivecasino.IUserBaseInfo} message UserBaseInfo message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        UserBaseInfo.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.user_name != null && message.hasOwnProperty("user_name"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.user_name);
            if (message.balance != null && message.hasOwnProperty("balance"))
                writer.uint32(/* id 2, wireType 1 =*/17).double(message.balance);
            return writer;
        };

        /**
         * Encodes the specified UserBaseInfo message, length delimited. Does not implicitly {@link aslivecasino.UserBaseInfo.verify|verify} messages.
         * @function encodeDelimited
         * @memberof aslivecasino.UserBaseInfo
         * @static
         * @param {aslivecasino.IUserBaseInfo} message UserBaseInfo message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        UserBaseInfo.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a UserBaseInfo message from the specified reader or buffer.
         * @function decode
         * @memberof aslivecasino.UserBaseInfo
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {aslivecasino.UserBaseInfo} UserBaseInfo
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        UserBaseInfo.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.aslivecasino.UserBaseInfo();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.user_name = reader.string();
                    break;
                case 2:
                    message.balance = reader.double();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a UserBaseInfo message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof aslivecasino.UserBaseInfo
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {aslivecasino.UserBaseInfo} UserBaseInfo
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        UserBaseInfo.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a UserBaseInfo message.
         * @function verify
         * @memberof aslivecasino.UserBaseInfo
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        UserBaseInfo.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.user_name != null && message.hasOwnProperty("user_name"))
                if (!$util.isString(message.user_name))
                    return "user_name: string expected";
            if (message.balance != null && message.hasOwnProperty("balance"))
                if (typeof message.balance !== "number")
                    return "balance: number expected";
            return null;
        };

        /**
         * Creates a UserBaseInfo message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof aslivecasino.UserBaseInfo
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {aslivecasino.UserBaseInfo} UserBaseInfo
         */
        UserBaseInfo.fromObject = function fromObject(object) {
            if (object instanceof $root.aslivecasino.UserBaseInfo)
                return object;
            var message = new $root.aslivecasino.UserBaseInfo();
            if (object.user_name != null)
                message.user_name = String(object.user_name);
            if (object.balance != null)
                message.balance = Number(object.balance);
            return message;
        };

        /**
         * Creates a plain object from a UserBaseInfo message. Also converts values to other types if specified.
         * @function toObject
         * @memberof aslivecasino.UserBaseInfo
         * @static
         * @param {aslivecasino.UserBaseInfo} message UserBaseInfo
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        UserBaseInfo.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.user_name = "";
                object.balance = 0;
            }
            if (message.user_name != null && message.hasOwnProperty("user_name"))
                object.user_name = message.user_name;
            if (message.balance != null && message.hasOwnProperty("balance"))
                object.balance = options.json && !isFinite(message.balance) ? String(message.balance) : message.balance;
            return object;
        };

        /**
         * Converts this UserBaseInfo to JSON.
         * @function toJSON
         * @memberof aslivecasino.UserBaseInfo
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        UserBaseInfo.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return UserBaseInfo;
    })();

    aslivecasino.LobbyList = (function() {

        /**
         * Properties of a LobbyList.
         * @memberof aslivecasino
         * @interface ILobbyList
         * @property {string|null} [json_lobbylist] LobbyList json_lobbylist
         */

        /**
         * Constructs a new LobbyList.
         * @memberof aslivecasino
         * @classdesc Represents a LobbyList.
         * @implements ILobbyList
         * @constructor
         * @param {aslivecasino.ILobbyList=} [properties] Properties to set
         */
        function LobbyList(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * LobbyList json_lobbylist.
         * @member {string} json_lobbylist
         * @memberof aslivecasino.LobbyList
         * @instance
         */
        LobbyList.prototype.json_lobbylist = "";

        /**
         * Creates a new LobbyList instance using the specified properties.
         * @function create
         * @memberof aslivecasino.LobbyList
         * @static
         * @param {aslivecasino.ILobbyList=} [properties] Properties to set
         * @returns {aslivecasino.LobbyList} LobbyList instance
         */
        LobbyList.create = function create(properties) {
            return new LobbyList(properties);
        };

        /**
         * Encodes the specified LobbyList message. Does not implicitly {@link aslivecasino.LobbyList.verify|verify} messages.
         * @function encode
         * @memberof aslivecasino.LobbyList
         * @static
         * @param {aslivecasino.ILobbyList} message LobbyList message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        LobbyList.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.json_lobbylist != null && message.hasOwnProperty("json_lobbylist"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.json_lobbylist);
            return writer;
        };

        /**
         * Encodes the specified LobbyList message, length delimited. Does not implicitly {@link aslivecasino.LobbyList.verify|verify} messages.
         * @function encodeDelimited
         * @memberof aslivecasino.LobbyList
         * @static
         * @param {aslivecasino.ILobbyList} message LobbyList message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        LobbyList.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a LobbyList message from the specified reader or buffer.
         * @function decode
         * @memberof aslivecasino.LobbyList
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {aslivecasino.LobbyList} LobbyList
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        LobbyList.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.aslivecasino.LobbyList();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.json_lobbylist = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a LobbyList message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof aslivecasino.LobbyList
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {aslivecasino.LobbyList} LobbyList
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        LobbyList.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a LobbyList message.
         * @function verify
         * @memberof aslivecasino.LobbyList
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        LobbyList.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.json_lobbylist != null && message.hasOwnProperty("json_lobbylist"))
                if (!$util.isString(message.json_lobbylist))
                    return "json_lobbylist: string expected";
            return null;
        };

        /**
         * Creates a LobbyList message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof aslivecasino.LobbyList
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {aslivecasino.LobbyList} LobbyList
         */
        LobbyList.fromObject = function fromObject(object) {
            if (object instanceof $root.aslivecasino.LobbyList)
                return object;
            var message = new $root.aslivecasino.LobbyList();
            if (object.json_lobbylist != null)
                message.json_lobbylist = String(object.json_lobbylist);
            return message;
        };

        /**
         * Creates a plain object from a LobbyList message. Also converts values to other types if specified.
         * @function toObject
         * @memberof aslivecasino.LobbyList
         * @static
         * @param {aslivecasino.LobbyList} message LobbyList
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        LobbyList.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults)
                object.json_lobbylist = "";
            if (message.json_lobbylist != null && message.hasOwnProperty("json_lobbylist"))
                object.json_lobbylist = message.json_lobbylist;
            return object;
        };

        /**
         * Converts this LobbyList to JSON.
         * @function toJSON
         * @memberof aslivecasino.LobbyList
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        LobbyList.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return LobbyList;
    })();

    aslivecasino.RoomList = (function() {

        /**
         * Properties of a RoomList.
         * @memberof aslivecasino
         * @interface IRoomList
         * @property {string|null} [json_roomlist] RoomList json_roomlist
         */

        /**
         * Constructs a new RoomList.
         * @memberof aslivecasino
         * @classdesc Represents a RoomList.
         * @implements IRoomList
         * @constructor
         * @param {aslivecasino.IRoomList=} [properties] Properties to set
         */
        function RoomList(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * RoomList json_roomlist.
         * @member {string} json_roomlist
         * @memberof aslivecasino.RoomList
         * @instance
         */
        RoomList.prototype.json_roomlist = "";

        /**
         * Creates a new RoomList instance using the specified properties.
         * @function create
         * @memberof aslivecasino.RoomList
         * @static
         * @param {aslivecasino.IRoomList=} [properties] Properties to set
         * @returns {aslivecasino.RoomList} RoomList instance
         */
        RoomList.create = function create(properties) {
            return new RoomList(properties);
        };

        /**
         * Encodes the specified RoomList message. Does not implicitly {@link aslivecasino.RoomList.verify|verify} messages.
         * @function encode
         * @memberof aslivecasino.RoomList
         * @static
         * @param {aslivecasino.IRoomList} message RoomList message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        RoomList.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.json_roomlist != null && message.hasOwnProperty("json_roomlist"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.json_roomlist);
            return writer;
        };

        /**
         * Encodes the specified RoomList message, length delimited. Does not implicitly {@link aslivecasino.RoomList.verify|verify} messages.
         * @function encodeDelimited
         * @memberof aslivecasino.RoomList
         * @static
         * @param {aslivecasino.IRoomList} message RoomList message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        RoomList.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a RoomList message from the specified reader or buffer.
         * @function decode
         * @memberof aslivecasino.RoomList
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {aslivecasino.RoomList} RoomList
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        RoomList.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.aslivecasino.RoomList();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.json_roomlist = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a RoomList message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof aslivecasino.RoomList
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {aslivecasino.RoomList} RoomList
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        RoomList.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a RoomList message.
         * @function verify
         * @memberof aslivecasino.RoomList
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        RoomList.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.json_roomlist != null && message.hasOwnProperty("json_roomlist"))
                if (!$util.isString(message.json_roomlist))
                    return "json_roomlist: string expected";
            return null;
        };

        /**
         * Creates a RoomList message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof aslivecasino.RoomList
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {aslivecasino.RoomList} RoomList
         */
        RoomList.fromObject = function fromObject(object) {
            if (object instanceof $root.aslivecasino.RoomList)
                return object;
            var message = new $root.aslivecasino.RoomList();
            if (object.json_roomlist != null)
                message.json_roomlist = String(object.json_roomlist);
            return message;
        };

        /**
         * Creates a plain object from a RoomList message. Also converts values to other types if specified.
         * @function toObject
         * @memberof aslivecasino.RoomList
         * @static
         * @param {aslivecasino.RoomList} message RoomList
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        RoomList.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults)
                object.json_roomlist = "";
            if (message.json_roomlist != null && message.hasOwnProperty("json_roomlist"))
                object.json_roomlist = message.json_roomlist;
            return object;
        };

        /**
         * Converts this RoomList to JSON.
         * @function toJSON
         * @memberof aslivecasino.RoomList
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        RoomList.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return RoomList;
    })();

    aslivecasino.Preference = (function() {

        /**
         * Properties of a Preference.
         * @memberof aslivecasino
         * @interface IPreference
         */

        /**
         * Constructs a new Preference.
         * @memberof aslivecasino
         * @classdesc Represents a Preference.
         * @implements IPreference
         * @constructor
         * @param {aslivecasino.IPreference=} [properties] Properties to set
         */
        function Preference(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Creates a new Preference instance using the specified properties.
         * @function create
         * @memberof aslivecasino.Preference
         * @static
         * @param {aslivecasino.IPreference=} [properties] Properties to set
         * @returns {aslivecasino.Preference} Preference instance
         */
        Preference.create = function create(properties) {
            return new Preference(properties);
        };

        /**
         * Encodes the specified Preference message. Does not implicitly {@link aslivecasino.Preference.verify|verify} messages.
         * @function encode
         * @memberof aslivecasino.Preference
         * @static
         * @param {aslivecasino.IPreference} message Preference message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Preference.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            return writer;
        };

        /**
         * Encodes the specified Preference message, length delimited. Does not implicitly {@link aslivecasino.Preference.verify|verify} messages.
         * @function encodeDelimited
         * @memberof aslivecasino.Preference
         * @static
         * @param {aslivecasino.IPreference} message Preference message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Preference.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Preference message from the specified reader or buffer.
         * @function decode
         * @memberof aslivecasino.Preference
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {aslivecasino.Preference} Preference
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Preference.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.aslivecasino.Preference();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Preference message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof aslivecasino.Preference
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {aslivecasino.Preference} Preference
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Preference.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Preference message.
         * @function verify
         * @memberof aslivecasino.Preference
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Preference.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            return null;
        };

        /**
         * Creates a Preference message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof aslivecasino.Preference
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {aslivecasino.Preference} Preference
         */
        Preference.fromObject = function fromObject(object) {
            if (object instanceof $root.aslivecasino.Preference)
                return object;
            return new $root.aslivecasino.Preference();
        };

        /**
         * Creates a plain object from a Preference message. Also converts values to other types if specified.
         * @function toObject
         * @memberof aslivecasino.Preference
         * @static
         * @param {aslivecasino.Preference} message Preference
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Preference.toObject = function toObject() {
            return {};
        };

        /**
         * Converts this Preference to JSON.
         * @function toJSON
         * @memberof aslivecasino.Preference
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Preference.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return Preference;
    })();

    /**
     * EnumProtocol enum.
     * @name aslivecasino.EnumProtocol
     * @enum {string}
     * @property {number} UNKNOWN=0 UNKNOWN value
     * @property {number} Disconnect=1 Disconnect value
     * @property {number} EnterLobby=10 EnterLobby value
     * @property {number} EnterRoom=11 EnterRoom value
     * @property {number} LeaveRoom=12 LeaveRoom value
     * @property {number} ShowRoomTablePage=13 ShowRoomTablePage value
     * @property {number} Update=20 Update value
     * @property {number} Notify=21 Notify value
     * @property {number} Chat=22 Chat value
     * @property {number} Bac_Bet=51 Bac_Bet value
     * @property {number} Bac_GetRoutingUpdate_Inside=52 Bac_GetRoutingUpdate_Inside value
     * @property {number} Bac_ExtendBettingTime=61 Bac_ExtendBettingTime value
     * @property {number} Bac_MeCard=62 Bac_MeCard value
     * @property {number} Bac_WishToExtBet=63 Bac_WishToExtBet value
     */
    aslivecasino.EnumProtocol = (function() {
        var valuesById = {}, values = Object.create(valuesById);
        values[valuesById[0] = "UNKNOWN"] = 0;
        values[valuesById[1] = "Disconnect"] = 1;
        values[valuesById[10] = "EnterLobby"] = 10;
        values[valuesById[11] = "EnterRoom"] = 11;
        values[valuesById[12] = "LeaveRoom"] = 12;
        values[valuesById[13] = "ShowRoomTablePage"] = 13;
        values[valuesById[20] = "Update"] = 20;
        values[valuesById[21] = "Notify"] = 21;
        values[valuesById[22] = "Chat"] = 22;
        values[valuesById[51] = "Bac_Bet"] = 51;
        values[valuesById[52] = "Bac_GetRoutingUpdate_Inside"] = 52;
        values[valuesById[61] = "Bac_ExtendBettingTime"] = 61;
        values[valuesById[62] = "Bac_MeCard"] = 62;
        values[valuesById[63] = "Bac_WishToExtBet"] = 63;
        return values;
    })();

    aslivecasino.TurnTo = (function() {

        /**
         * Properties of a TurnTo.
         * @memberof aslivecasino
         * @interface ITurnTo
         * @property {string|null} [lobby_no] TurnTo lobby_no
         * @property {string|null} [room_no] TurnTo room_no
         * @property {number|null} [table_no] TurnTo table_no
         * @property {number|null} [seat_no] TurnTo seat_no
         * @property {boolean|null} [in_sel_seat] TurnTo in_sel_seat
         */

        /**
         * Constructs a new TurnTo.
         * @memberof aslivecasino
         * @classdesc Represents a TurnTo.
         * @implements ITurnTo
         * @constructor
         * @param {aslivecasino.ITurnTo=} [properties] Properties to set
         */
        function TurnTo(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * TurnTo lobby_no.
         * @member {string} lobby_no
         * @memberof aslivecasino.TurnTo
         * @instance
         */
        TurnTo.prototype.lobby_no = "";

        /**
         * TurnTo room_no.
         * @member {string} room_no
         * @memberof aslivecasino.TurnTo
         * @instance
         */
        TurnTo.prototype.room_no = "";

        /**
         * TurnTo table_no.
         * @member {number} table_no
         * @memberof aslivecasino.TurnTo
         * @instance
         */
        TurnTo.prototype.table_no = 0;

        /**
         * TurnTo seat_no.
         * @member {number} seat_no
         * @memberof aslivecasino.TurnTo
         * @instance
         */
        TurnTo.prototype.seat_no = 0;

        /**
         * TurnTo in_sel_seat.
         * @member {boolean} in_sel_seat
         * @memberof aslivecasino.TurnTo
         * @instance
         */
        TurnTo.prototype.in_sel_seat = false;

        /**
         * Creates a new TurnTo instance using the specified properties.
         * @function create
         * @memberof aslivecasino.TurnTo
         * @static
         * @param {aslivecasino.ITurnTo=} [properties] Properties to set
         * @returns {aslivecasino.TurnTo} TurnTo instance
         */
        TurnTo.create = function create(properties) {
            return new TurnTo(properties);
        };

        /**
         * Encodes the specified TurnTo message. Does not implicitly {@link aslivecasino.TurnTo.verify|verify} messages.
         * @function encode
         * @memberof aslivecasino.TurnTo
         * @static
         * @param {aslivecasino.ITurnTo} message TurnTo message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        TurnTo.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.lobby_no != null && message.hasOwnProperty("lobby_no"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.lobby_no);
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                writer.uint32(/* id 2, wireType 2 =*/18).string(message.room_no);
            if (message.table_no != null && message.hasOwnProperty("table_no"))
                writer.uint32(/* id 3, wireType 0 =*/24).sint32(message.table_no);
            if (message.seat_no != null && message.hasOwnProperty("seat_no"))
                writer.uint32(/* id 4, wireType 0 =*/32).sint32(message.seat_no);
            if (message.in_sel_seat != null && message.hasOwnProperty("in_sel_seat"))
                writer.uint32(/* id 5, wireType 0 =*/40).bool(message.in_sel_seat);
            return writer;
        };

        /**
         * Encodes the specified TurnTo message, length delimited. Does not implicitly {@link aslivecasino.TurnTo.verify|verify} messages.
         * @function encodeDelimited
         * @memberof aslivecasino.TurnTo
         * @static
         * @param {aslivecasino.ITurnTo} message TurnTo message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        TurnTo.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a TurnTo message from the specified reader or buffer.
         * @function decode
         * @memberof aslivecasino.TurnTo
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {aslivecasino.TurnTo} TurnTo
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        TurnTo.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.aslivecasino.TurnTo();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.lobby_no = reader.string();
                    break;
                case 2:
                    message.room_no = reader.string();
                    break;
                case 3:
                    message.table_no = reader.sint32();
                    break;
                case 4:
                    message.seat_no = reader.sint32();
                    break;
                case 5:
                    message.in_sel_seat = reader.bool();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a TurnTo message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof aslivecasino.TurnTo
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {aslivecasino.TurnTo} TurnTo
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        TurnTo.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a TurnTo message.
         * @function verify
         * @memberof aslivecasino.TurnTo
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        TurnTo.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.lobby_no != null && message.hasOwnProperty("lobby_no"))
                if (!$util.isString(message.lobby_no))
                    return "lobby_no: string expected";
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                if (!$util.isString(message.room_no))
                    return "room_no: string expected";
            if (message.table_no != null && message.hasOwnProperty("table_no"))
                if (!$util.isInteger(message.table_no))
                    return "table_no: integer expected";
            if (message.seat_no != null && message.hasOwnProperty("seat_no"))
                if (!$util.isInteger(message.seat_no))
                    return "seat_no: integer expected";
            if (message.in_sel_seat != null && message.hasOwnProperty("in_sel_seat"))
                if (typeof message.in_sel_seat !== "boolean")
                    return "in_sel_seat: boolean expected";
            return null;
        };

        /**
         * Creates a TurnTo message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof aslivecasino.TurnTo
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {aslivecasino.TurnTo} TurnTo
         */
        TurnTo.fromObject = function fromObject(object) {
            if (object instanceof $root.aslivecasino.TurnTo)
                return object;
            var message = new $root.aslivecasino.TurnTo();
            if (object.lobby_no != null)
                message.lobby_no = String(object.lobby_no);
            if (object.room_no != null)
                message.room_no = String(object.room_no);
            if (object.table_no != null)
                message.table_no = object.table_no | 0;
            if (object.seat_no != null)
                message.seat_no = object.seat_no | 0;
            if (object.in_sel_seat != null)
                message.in_sel_seat = Boolean(object.in_sel_seat);
            return message;
        };

        /**
         * Creates a plain object from a TurnTo message. Also converts values to other types if specified.
         * @function toObject
         * @memberof aslivecasino.TurnTo
         * @static
         * @param {aslivecasino.TurnTo} message TurnTo
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        TurnTo.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.lobby_no = "";
                object.room_no = "";
                object.table_no = 0;
                object.seat_no = 0;
                object.in_sel_seat = false;
            }
            if (message.lobby_no != null && message.hasOwnProperty("lobby_no"))
                object.lobby_no = message.lobby_no;
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                object.room_no = message.room_no;
            if (message.table_no != null && message.hasOwnProperty("table_no"))
                object.table_no = message.table_no;
            if (message.seat_no != null && message.hasOwnProperty("seat_no"))
                object.seat_no = message.seat_no;
            if (message.in_sel_seat != null && message.hasOwnProperty("in_sel_seat"))
                object.in_sel_seat = message.in_sel_seat;
            return object;
        };

        /**
         * Converts this TurnTo to JSON.
         * @function toJSON
         * @memberof aslivecasino.TurnTo
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        TurnTo.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return TurnTo;
    })();

    aslivecasino.RoomSeatsInfo = (function() {

        /**
         * Properties of a RoomSeatsInfo.
         * @memberof aslivecasino
         * @interface IRoomSeatsInfo
         * @property {string|null} [room_no] RoomSeatsInfo room_no
         */

        /**
         * Constructs a new RoomSeatsInfo.
         * @memberof aslivecasino
         * @classdesc Represents a RoomSeatsInfo.
         * @implements IRoomSeatsInfo
         * @constructor
         * @param {aslivecasino.IRoomSeatsInfo=} [properties] Properties to set
         */
        function RoomSeatsInfo(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * RoomSeatsInfo room_no.
         * @member {string} room_no
         * @memberof aslivecasino.RoomSeatsInfo
         * @instance
         */
        RoomSeatsInfo.prototype.room_no = "";

        /**
         * Creates a new RoomSeatsInfo instance using the specified properties.
         * @function create
         * @memberof aslivecasino.RoomSeatsInfo
         * @static
         * @param {aslivecasino.IRoomSeatsInfo=} [properties] Properties to set
         * @returns {aslivecasino.RoomSeatsInfo} RoomSeatsInfo instance
         */
        RoomSeatsInfo.create = function create(properties) {
            return new RoomSeatsInfo(properties);
        };

        /**
         * Encodes the specified RoomSeatsInfo message. Does not implicitly {@link aslivecasino.RoomSeatsInfo.verify|verify} messages.
         * @function encode
         * @memberof aslivecasino.RoomSeatsInfo
         * @static
         * @param {aslivecasino.IRoomSeatsInfo} message RoomSeatsInfo message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        RoomSeatsInfo.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                writer.uint32(/* id 2, wireType 2 =*/18).string(message.room_no);
            return writer;
        };

        /**
         * Encodes the specified RoomSeatsInfo message, length delimited. Does not implicitly {@link aslivecasino.RoomSeatsInfo.verify|verify} messages.
         * @function encodeDelimited
         * @memberof aslivecasino.RoomSeatsInfo
         * @static
         * @param {aslivecasino.IRoomSeatsInfo} message RoomSeatsInfo message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        RoomSeatsInfo.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a RoomSeatsInfo message from the specified reader or buffer.
         * @function decode
         * @memberof aslivecasino.RoomSeatsInfo
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {aslivecasino.RoomSeatsInfo} RoomSeatsInfo
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        RoomSeatsInfo.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.aslivecasino.RoomSeatsInfo();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 2:
                    message.room_no = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a RoomSeatsInfo message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof aslivecasino.RoomSeatsInfo
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {aslivecasino.RoomSeatsInfo} RoomSeatsInfo
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        RoomSeatsInfo.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a RoomSeatsInfo message.
         * @function verify
         * @memberof aslivecasino.RoomSeatsInfo
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        RoomSeatsInfo.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                if (!$util.isString(message.room_no))
                    return "room_no: string expected";
            return null;
        };

        /**
         * Creates a RoomSeatsInfo message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof aslivecasino.RoomSeatsInfo
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {aslivecasino.RoomSeatsInfo} RoomSeatsInfo
         */
        RoomSeatsInfo.fromObject = function fromObject(object) {
            if (object instanceof $root.aslivecasino.RoomSeatsInfo)
                return object;
            var message = new $root.aslivecasino.RoomSeatsInfo();
            if (object.room_no != null)
                message.room_no = String(object.room_no);
            return message;
        };

        /**
         * Creates a plain object from a RoomSeatsInfo message. Also converts values to other types if specified.
         * @function toObject
         * @memberof aslivecasino.RoomSeatsInfo
         * @static
         * @param {aslivecasino.RoomSeatsInfo} message RoomSeatsInfo
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        RoomSeatsInfo.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults)
                object.room_no = "";
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                object.room_no = message.room_no;
            return object;
        };

        /**
         * Converts this RoomSeatsInfo to JSON.
         * @function toJSON
         * @memberof aslivecasino.RoomSeatsInfo
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        RoomSeatsInfo.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return RoomSeatsInfo;
    })();

    aslivecasino.OnSiteUpdate = (function() {

        /**
         * Properties of an OnSiteUpdate.
         * @memberof aslivecasino
         * @interface IOnSiteUpdate
         * @property {string|null} [room_no] OnSiteUpdate room_no
         * @property {aslivecasino.OnSiteUpdate.EnumInfoType|null} [info_type] OnSiteUpdate info_type
         * @property {number|null} [no] OnSiteUpdate no
         * @property {string|null} [data] OnSiteUpdate data
         */

        /**
         * Constructs a new OnSiteUpdate.
         * @memberof aslivecasino
         * @classdesc Represents an OnSiteUpdate.
         * @implements IOnSiteUpdate
         * @constructor
         * @param {aslivecasino.IOnSiteUpdate=} [properties] Properties to set
         */
        function OnSiteUpdate(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * OnSiteUpdate room_no.
         * @member {string} room_no
         * @memberof aslivecasino.OnSiteUpdate
         * @instance
         */
        OnSiteUpdate.prototype.room_no = "";

        /**
         * OnSiteUpdate info_type.
         * @member {aslivecasino.OnSiteUpdate.EnumInfoType} info_type
         * @memberof aslivecasino.OnSiteUpdate
         * @instance
         */
        OnSiteUpdate.prototype.info_type = 0;

        /**
         * OnSiteUpdate no.
         * @member {number} no
         * @memberof aslivecasino.OnSiteUpdate
         * @instance
         */
        OnSiteUpdate.prototype.no = 0;

        /**
         * OnSiteUpdate data.
         * @member {string} data
         * @memberof aslivecasino.OnSiteUpdate
         * @instance
         */
        OnSiteUpdate.prototype.data = "";

        /**
         * Creates a new OnSiteUpdate instance using the specified properties.
         * @function create
         * @memberof aslivecasino.OnSiteUpdate
         * @static
         * @param {aslivecasino.IOnSiteUpdate=} [properties] Properties to set
         * @returns {aslivecasino.OnSiteUpdate} OnSiteUpdate instance
         */
        OnSiteUpdate.create = function create(properties) {
            return new OnSiteUpdate(properties);
        };

        /**
         * Encodes the specified OnSiteUpdate message. Does not implicitly {@link aslivecasino.OnSiteUpdate.verify|verify} messages.
         * @function encode
         * @memberof aslivecasino.OnSiteUpdate
         * @static
         * @param {aslivecasino.IOnSiteUpdate} message OnSiteUpdate message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        OnSiteUpdate.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.room_no);
            if (message.info_type != null && message.hasOwnProperty("info_type"))
                writer.uint32(/* id 2, wireType 0 =*/16).int32(message.info_type);
            if (message.no != null && message.hasOwnProperty("no"))
                writer.uint32(/* id 3, wireType 0 =*/24).int32(message.no);
            if (message.data != null && message.hasOwnProperty("data"))
                writer.uint32(/* id 4, wireType 2 =*/34).string(message.data);
            return writer;
        };

        /**
         * Encodes the specified OnSiteUpdate message, length delimited. Does not implicitly {@link aslivecasino.OnSiteUpdate.verify|verify} messages.
         * @function encodeDelimited
         * @memberof aslivecasino.OnSiteUpdate
         * @static
         * @param {aslivecasino.IOnSiteUpdate} message OnSiteUpdate message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        OnSiteUpdate.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes an OnSiteUpdate message from the specified reader or buffer.
         * @function decode
         * @memberof aslivecasino.OnSiteUpdate
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {aslivecasino.OnSiteUpdate} OnSiteUpdate
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        OnSiteUpdate.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.aslivecasino.OnSiteUpdate();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.room_no = reader.string();
                    break;
                case 2:
                    message.info_type = reader.int32();
                    break;
                case 3:
                    message.no = reader.int32();
                    break;
                case 4:
                    message.data = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes an OnSiteUpdate message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof aslivecasino.OnSiteUpdate
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {aslivecasino.OnSiteUpdate} OnSiteUpdate
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        OnSiteUpdate.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies an OnSiteUpdate message.
         * @function verify
         * @memberof aslivecasino.OnSiteUpdate
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        OnSiteUpdate.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                if (!$util.isString(message.room_no))
                    return "room_no: string expected";
            if (message.info_type != null && message.hasOwnProperty("info_type"))
                switch (message.info_type) {
                default:
                    return "info_type: enum value expected";
                case 0:
                case 1:
                case 2:
                    break;
                }
            if (message.no != null && message.hasOwnProperty("no"))
                if (!$util.isInteger(message.no))
                    return "no: integer expected";
            if (message.data != null && message.hasOwnProperty("data"))
                if (!$util.isString(message.data))
                    return "data: string expected";
            return null;
        };

        /**
         * Creates an OnSiteUpdate message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof aslivecasino.OnSiteUpdate
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {aslivecasino.OnSiteUpdate} OnSiteUpdate
         */
        OnSiteUpdate.fromObject = function fromObject(object) {
            if (object instanceof $root.aslivecasino.OnSiteUpdate)
                return object;
            var message = new $root.aslivecasino.OnSiteUpdate();
            if (object.room_no != null)
                message.room_no = String(object.room_no);
            switch (object.info_type) {
            case "DEALER_NAME":
            case 0:
                message.info_type = 0;
                break;
            case "DEALER_PIC":
            case 1:
                message.info_type = 1;
                break;
            case "VIDEO_URL":
            case 2:
                message.info_type = 2;
                break;
            }
            if (object.no != null)
                message.no = object.no | 0;
            if (object.data != null)
                message.data = String(object.data);
            return message;
        };

        /**
         * Creates a plain object from an OnSiteUpdate message. Also converts values to other types if specified.
         * @function toObject
         * @memberof aslivecasino.OnSiteUpdate
         * @static
         * @param {aslivecasino.OnSiteUpdate} message OnSiteUpdate
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        OnSiteUpdate.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.room_no = "";
                object.info_type = options.enums === String ? "DEALER_NAME" : 0;
                object.no = 0;
                object.data = "";
            }
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                object.room_no = message.room_no;
            if (message.info_type != null && message.hasOwnProperty("info_type"))
                object.info_type = options.enums === String ? $root.aslivecasino.OnSiteUpdate.EnumInfoType[message.info_type] : message.info_type;
            if (message.no != null && message.hasOwnProperty("no"))
                object.no = message.no;
            if (message.data != null && message.hasOwnProperty("data"))
                object.data = message.data;
            return object;
        };

        /**
         * Converts this OnSiteUpdate to JSON.
         * @function toJSON
         * @memberof aslivecasino.OnSiteUpdate
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        OnSiteUpdate.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * EnumInfoType enum.
         * @name aslivecasino.OnSiteUpdate.EnumInfoType
         * @enum {string}
         * @property {number} DEALER_NAME=0 DEALER_NAME value
         * @property {number} DEALER_PIC=1 DEALER_PIC value
         * @property {number} VIDEO_URL=2 VIDEO_URL value
         */
        OnSiteUpdate.EnumInfoType = (function() {
            var valuesById = {}, values = Object.create(valuesById);
            values[valuesById[0] = "DEALER_NAME"] = 0;
            values[valuesById[1] = "DEALER_PIC"] = 1;
            values[valuesById[2] = "VIDEO_URL"] = 2;
            return values;
        })();

        return OnSiteUpdate;
    })();

    aslivecasino.ChatMessage = (function() {

        /**
         * Properties of a ChatMessage.
         * @memberof aslivecasino
         * @interface IChatMessage
         * @property {Array.<aslivecasino.ChatMessage.IOneMsg>|null} [msg_list] ChatMessage msg_list
         */

        /**
         * Constructs a new ChatMessage.
         * @memberof aslivecasino
         * @classdesc Represents a ChatMessage.
         * @implements IChatMessage
         * @constructor
         * @param {aslivecasino.IChatMessage=} [properties] Properties to set
         */
        function ChatMessage(properties) {
            this.msg_list = [];
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * ChatMessage msg_list.
         * @member {Array.<aslivecasino.ChatMessage.IOneMsg>} msg_list
         * @memberof aslivecasino.ChatMessage
         * @instance
         */
        ChatMessage.prototype.msg_list = $util.emptyArray;

        /**
         * Creates a new ChatMessage instance using the specified properties.
         * @function create
         * @memberof aslivecasino.ChatMessage
         * @static
         * @param {aslivecasino.IChatMessage=} [properties] Properties to set
         * @returns {aslivecasino.ChatMessage} ChatMessage instance
         */
        ChatMessage.create = function create(properties) {
            return new ChatMessage(properties);
        };

        /**
         * Encodes the specified ChatMessage message. Does not implicitly {@link aslivecasino.ChatMessage.verify|verify} messages.
         * @function encode
         * @memberof aslivecasino.ChatMessage
         * @static
         * @param {aslivecasino.IChatMessage} message ChatMessage message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        ChatMessage.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.msg_list != null && message.msg_list.length)
                for (var i = 0; i < message.msg_list.length; ++i)
                    $root.aslivecasino.ChatMessage.OneMsg.encode(message.msg_list[i], writer.uint32(/* id 1, wireType 2 =*/10).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified ChatMessage message, length delimited. Does not implicitly {@link aslivecasino.ChatMessage.verify|verify} messages.
         * @function encodeDelimited
         * @memberof aslivecasino.ChatMessage
         * @static
         * @param {aslivecasino.IChatMessage} message ChatMessage message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        ChatMessage.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a ChatMessage message from the specified reader or buffer.
         * @function decode
         * @memberof aslivecasino.ChatMessage
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {aslivecasino.ChatMessage} ChatMessage
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        ChatMessage.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.aslivecasino.ChatMessage();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    if (!(message.msg_list && message.msg_list.length))
                        message.msg_list = [];
                    message.msg_list.push($root.aslivecasino.ChatMessage.OneMsg.decode(reader, reader.uint32()));
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a ChatMessage message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof aslivecasino.ChatMessage
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {aslivecasino.ChatMessage} ChatMessage
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        ChatMessage.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a ChatMessage message.
         * @function verify
         * @memberof aslivecasino.ChatMessage
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        ChatMessage.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.msg_list != null && message.hasOwnProperty("msg_list")) {
                if (!Array.isArray(message.msg_list))
                    return "msg_list: array expected";
                for (var i = 0; i < message.msg_list.length; ++i) {
                    var error = $root.aslivecasino.ChatMessage.OneMsg.verify(message.msg_list[i]);
                    if (error)
                        return "msg_list." + error;
                }
            }
            return null;
        };

        /**
         * Creates a ChatMessage message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof aslivecasino.ChatMessage
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {aslivecasino.ChatMessage} ChatMessage
         */
        ChatMessage.fromObject = function fromObject(object) {
            if (object instanceof $root.aslivecasino.ChatMessage)
                return object;
            var message = new $root.aslivecasino.ChatMessage();
            if (object.msg_list) {
                if (!Array.isArray(object.msg_list))
                    throw TypeError(".aslivecasino.ChatMessage.msg_list: array expected");
                message.msg_list = [];
                for (var i = 0; i < object.msg_list.length; ++i) {
                    if (typeof object.msg_list[i] !== "object")
                        throw TypeError(".aslivecasino.ChatMessage.msg_list: object expected");
                    message.msg_list[i] = $root.aslivecasino.ChatMessage.OneMsg.fromObject(object.msg_list[i]);
                }
            }
            return message;
        };

        /**
         * Creates a plain object from a ChatMessage message. Also converts values to other types if specified.
         * @function toObject
         * @memberof aslivecasino.ChatMessage
         * @static
         * @param {aslivecasino.ChatMessage} message ChatMessage
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        ChatMessage.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.arrays || options.defaults)
                object.msg_list = [];
            if (message.msg_list && message.msg_list.length) {
                object.msg_list = [];
                for (var j = 0; j < message.msg_list.length; ++j)
                    object.msg_list[j] = $root.aslivecasino.ChatMessage.OneMsg.toObject(message.msg_list[j], options);
            }
            return object;
        };

        /**
         * Converts this ChatMessage to JSON.
         * @function toJSON
         * @memberof aslivecasino.ChatMessage
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        ChatMessage.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        ChatMessage.OneMsg = (function() {

            /**
             * Properties of an OneMsg.
             * @memberof aslivecasino.ChatMessage
             * @interface IOneMsg
             * @property {number|Long|null} [msg_id] OneMsg msg_id
             * @property {number|Long|null} [from_user_id] OneMsg from_user_id
             * @property {string|null} [from_user_name] OneMsg from_user_name
             * @property {number|Long|null} [msg_time] OneMsg msg_time
             * @property {string|null} [message] OneMsg message
             */

            /**
             * Constructs a new OneMsg.
             * @memberof aslivecasino.ChatMessage
             * @classdesc Represents an OneMsg.
             * @implements IOneMsg
             * @constructor
             * @param {aslivecasino.ChatMessage.IOneMsg=} [properties] Properties to set
             */
            function OneMsg(properties) {
                if (properties)
                    for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                        if (properties[keys[i]] != null)
                            this[keys[i]] = properties[keys[i]];
            }

            /**
             * OneMsg msg_id.
             * @member {number|Long} msg_id
             * @memberof aslivecasino.ChatMessage.OneMsg
             * @instance
             */
            OneMsg.prototype.msg_id = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

            /**
             * OneMsg from_user_id.
             * @member {number|Long} from_user_id
             * @memberof aslivecasino.ChatMessage.OneMsg
             * @instance
             */
            OneMsg.prototype.from_user_id = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

            /**
             * OneMsg from_user_name.
             * @member {string} from_user_name
             * @memberof aslivecasino.ChatMessage.OneMsg
             * @instance
             */
            OneMsg.prototype.from_user_name = "";

            /**
             * OneMsg msg_time.
             * @member {number|Long} msg_time
             * @memberof aslivecasino.ChatMessage.OneMsg
             * @instance
             */
            OneMsg.prototype.msg_time = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

            /**
             * OneMsg message.
             * @member {string} message
             * @memberof aslivecasino.ChatMessage.OneMsg
             * @instance
             */
            OneMsg.prototype.message = "";

            /**
             * Creates a new OneMsg instance using the specified properties.
             * @function create
             * @memberof aslivecasino.ChatMessage.OneMsg
             * @static
             * @param {aslivecasino.ChatMessage.IOneMsg=} [properties] Properties to set
             * @returns {aslivecasino.ChatMessage.OneMsg} OneMsg instance
             */
            OneMsg.create = function create(properties) {
                return new OneMsg(properties);
            };

            /**
             * Encodes the specified OneMsg message. Does not implicitly {@link aslivecasino.ChatMessage.OneMsg.verify|verify} messages.
             * @function encode
             * @memberof aslivecasino.ChatMessage.OneMsg
             * @static
             * @param {aslivecasino.ChatMessage.IOneMsg} message OneMsg message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            OneMsg.encode = function encode(message, writer) {
                if (!writer)
                    writer = $Writer.create();
                if (message.msg_id != null && message.hasOwnProperty("msg_id"))
                    writer.uint32(/* id 1, wireType 0 =*/8).int64(message.msg_id);
                if (message.from_user_id != null && message.hasOwnProperty("from_user_id"))
                    writer.uint32(/* id 2, wireType 0 =*/16).int64(message.from_user_id);
                if (message.from_user_name != null && message.hasOwnProperty("from_user_name"))
                    writer.uint32(/* id 3, wireType 2 =*/26).string(message.from_user_name);
                if (message.msg_time != null && message.hasOwnProperty("msg_time"))
                    writer.uint32(/* id 4, wireType 0 =*/32).int64(message.msg_time);
                if (message.message != null && message.hasOwnProperty("message"))
                    writer.uint32(/* id 5, wireType 2 =*/42).string(message.message);
                return writer;
            };

            /**
             * Encodes the specified OneMsg message, length delimited. Does not implicitly {@link aslivecasino.ChatMessage.OneMsg.verify|verify} messages.
             * @function encodeDelimited
             * @memberof aslivecasino.ChatMessage.OneMsg
             * @static
             * @param {aslivecasino.ChatMessage.IOneMsg} message OneMsg message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            OneMsg.encodeDelimited = function encodeDelimited(message, writer) {
                return this.encode(message, writer).ldelim();
            };

            /**
             * Decodes an OneMsg message from the specified reader or buffer.
             * @function decode
             * @memberof aslivecasino.ChatMessage.OneMsg
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @param {number} [length] Message length if known beforehand
             * @returns {aslivecasino.ChatMessage.OneMsg} OneMsg
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            OneMsg.decode = function decode(reader, length) {
                if (!(reader instanceof $Reader))
                    reader = $Reader.create(reader);
                var end = length === undefined ? reader.len : reader.pos + length, message = new $root.aslivecasino.ChatMessage.OneMsg();
                while (reader.pos < end) {
                    var tag = reader.uint32();
                    switch (tag >>> 3) {
                    case 1:
                        message.msg_id = reader.int64();
                        break;
                    case 2:
                        message.from_user_id = reader.int64();
                        break;
                    case 3:
                        message.from_user_name = reader.string();
                        break;
                    case 4:
                        message.msg_time = reader.int64();
                        break;
                    case 5:
                        message.message = reader.string();
                        break;
                    default:
                        reader.skipType(tag & 7);
                        break;
                    }
                }
                return message;
            };

            /**
             * Decodes an OneMsg message from the specified reader or buffer, length delimited.
             * @function decodeDelimited
             * @memberof aslivecasino.ChatMessage.OneMsg
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @returns {aslivecasino.ChatMessage.OneMsg} OneMsg
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            OneMsg.decodeDelimited = function decodeDelimited(reader) {
                if (!(reader instanceof $Reader))
                    reader = new $Reader(reader);
                return this.decode(reader, reader.uint32());
            };

            /**
             * Verifies an OneMsg message.
             * @function verify
             * @memberof aslivecasino.ChatMessage.OneMsg
             * @static
             * @param {Object.<string,*>} message Plain object to verify
             * @returns {string|null} `null` if valid, otherwise the reason why it is not
             */
            OneMsg.verify = function verify(message) {
                if (typeof message !== "object" || message === null)
                    return "object expected";
                if (message.msg_id != null && message.hasOwnProperty("msg_id"))
                    if (!$util.isInteger(message.msg_id) && !(message.msg_id && $util.isInteger(message.msg_id.low) && $util.isInteger(message.msg_id.high)))
                        return "msg_id: integer|Long expected";
                if (message.from_user_id != null && message.hasOwnProperty("from_user_id"))
                    if (!$util.isInteger(message.from_user_id) && !(message.from_user_id && $util.isInteger(message.from_user_id.low) && $util.isInteger(message.from_user_id.high)))
                        return "from_user_id: integer|Long expected";
                if (message.from_user_name != null && message.hasOwnProperty("from_user_name"))
                    if (!$util.isString(message.from_user_name))
                        return "from_user_name: string expected";
                if (message.msg_time != null && message.hasOwnProperty("msg_time"))
                    if (!$util.isInteger(message.msg_time) && !(message.msg_time && $util.isInteger(message.msg_time.low) && $util.isInteger(message.msg_time.high)))
                        return "msg_time: integer|Long expected";
                if (message.message != null && message.hasOwnProperty("message"))
                    if (!$util.isString(message.message))
                        return "message: string expected";
                return null;
            };

            /**
             * Creates an OneMsg message from a plain object. Also converts values to their respective internal types.
             * @function fromObject
             * @memberof aslivecasino.ChatMessage.OneMsg
             * @static
             * @param {Object.<string,*>} object Plain object
             * @returns {aslivecasino.ChatMessage.OneMsg} OneMsg
             */
            OneMsg.fromObject = function fromObject(object) {
                if (object instanceof $root.aslivecasino.ChatMessage.OneMsg)
                    return object;
                var message = new $root.aslivecasino.ChatMessage.OneMsg();
                if (object.msg_id != null)
                    if ($util.Long)
                        (message.msg_id = $util.Long.fromValue(object.msg_id)).unsigned = false;
                    else if (typeof object.msg_id === "string")
                        message.msg_id = parseInt(object.msg_id, 10);
                    else if (typeof object.msg_id === "number")
                        message.msg_id = object.msg_id;
                    else if (typeof object.msg_id === "object")
                        message.msg_id = new $util.LongBits(object.msg_id.low >>> 0, object.msg_id.high >>> 0).toNumber();
                if (object.from_user_id != null)
                    if ($util.Long)
                        (message.from_user_id = $util.Long.fromValue(object.from_user_id)).unsigned = false;
                    else if (typeof object.from_user_id === "string")
                        message.from_user_id = parseInt(object.from_user_id, 10);
                    else if (typeof object.from_user_id === "number")
                        message.from_user_id = object.from_user_id;
                    else if (typeof object.from_user_id === "object")
                        message.from_user_id = new $util.LongBits(object.from_user_id.low >>> 0, object.from_user_id.high >>> 0).toNumber();
                if (object.from_user_name != null)
                    message.from_user_name = String(object.from_user_name);
                if (object.msg_time != null)
                    if ($util.Long)
                        (message.msg_time = $util.Long.fromValue(object.msg_time)).unsigned = false;
                    else if (typeof object.msg_time === "string")
                        message.msg_time = parseInt(object.msg_time, 10);
                    else if (typeof object.msg_time === "number")
                        message.msg_time = object.msg_time;
                    else if (typeof object.msg_time === "object")
                        message.msg_time = new $util.LongBits(object.msg_time.low >>> 0, object.msg_time.high >>> 0).toNumber();
                if (object.message != null)
                    message.message = String(object.message);
                return message;
            };

            /**
             * Creates a plain object from an OneMsg message. Also converts values to other types if specified.
             * @function toObject
             * @memberof aslivecasino.ChatMessage.OneMsg
             * @static
             * @param {aslivecasino.ChatMessage.OneMsg} message OneMsg
             * @param {$protobuf.IConversionOptions} [options] Conversion options
             * @returns {Object.<string,*>} Plain object
             */
            OneMsg.toObject = function toObject(message, options) {
                if (!options)
                    options = {};
                var object = {};
                if (options.defaults) {
                    if ($util.Long) {
                        var long = new $util.Long(0, 0, false);
                        object.msg_id = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                    } else
                        object.msg_id = options.longs === String ? "0" : 0;
                    if ($util.Long) {
                        var long = new $util.Long(0, 0, false);
                        object.from_user_id = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                    } else
                        object.from_user_id = options.longs === String ? "0" : 0;
                    object.from_user_name = "";
                    if ($util.Long) {
                        var long = new $util.Long(0, 0, false);
                        object.msg_time = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                    } else
                        object.msg_time = options.longs === String ? "0" : 0;
                    object.message = "";
                }
                if (message.msg_id != null && message.hasOwnProperty("msg_id"))
                    if (typeof message.msg_id === "number")
                        object.msg_id = options.longs === String ? String(message.msg_id) : message.msg_id;
                    else
                        object.msg_id = options.longs === String ? $util.Long.prototype.toString.call(message.msg_id) : options.longs === Number ? new $util.LongBits(message.msg_id.low >>> 0, message.msg_id.high >>> 0).toNumber() : message.msg_id;
                if (message.from_user_id != null && message.hasOwnProperty("from_user_id"))
                    if (typeof message.from_user_id === "number")
                        object.from_user_id = options.longs === String ? String(message.from_user_id) : message.from_user_id;
                    else
                        object.from_user_id = options.longs === String ? $util.Long.prototype.toString.call(message.from_user_id) : options.longs === Number ? new $util.LongBits(message.from_user_id.low >>> 0, message.from_user_id.high >>> 0).toNumber() : message.from_user_id;
                if (message.from_user_name != null && message.hasOwnProperty("from_user_name"))
                    object.from_user_name = message.from_user_name;
                if (message.msg_time != null && message.hasOwnProperty("msg_time"))
                    if (typeof message.msg_time === "number")
                        object.msg_time = options.longs === String ? String(message.msg_time) : message.msg_time;
                    else
                        object.msg_time = options.longs === String ? $util.Long.prototype.toString.call(message.msg_time) : options.longs === Number ? new $util.LongBits(message.msg_time.low >>> 0, message.msg_time.high >>> 0).toNumber() : message.msg_time;
                if (message.message != null && message.hasOwnProperty("message"))
                    object.message = message.message;
                return object;
            };

            /**
             * Converts this OneMsg to JSON.
             * @function toJSON
             * @memberof aslivecasino.ChatMessage.OneMsg
             * @instance
             * @returns {Object.<string,*>} JSON object
             */
            OneMsg.prototype.toJSON = function toJSON() {
                return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
            };

            return OneMsg;
        })();

        return ChatMessage;
    })();

    aslivecasino.CardData = (function() {

        /**
         * Properties of a CardData.
         * @memberof aslivecasino
         * @interface ICardData
         * @property {aslivecasino.CardData.EnumCardStyle|null} [style] CardData style
         * @property {number|null} [num] CardData num
         * @property {aslivecasino.CardData.EnumCardOpenStatus|null} [open_status] CardData open_status
         */

        /**
         * Constructs a new CardData.
         * @memberof aslivecasino
         * @classdesc Represents a CardData.
         * @implements ICardData
         * @constructor
         * @param {aslivecasino.ICardData=} [properties] Properties to set
         */
        function CardData(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * CardData style.
         * @member {aslivecasino.CardData.EnumCardStyle} style
         * @memberof aslivecasino.CardData
         * @instance
         */
        CardData.prototype.style = 0;

        /**
         * CardData num.
         * @member {number} num
         * @memberof aslivecasino.CardData
         * @instance
         */
        CardData.prototype.num = 0;

        /**
         * CardData open_status.
         * @member {aslivecasino.CardData.EnumCardOpenStatus} open_status
         * @memberof aslivecasino.CardData
         * @instance
         */
        CardData.prototype.open_status = 0;

        /**
         * Creates a new CardData instance using the specified properties.
         * @function create
         * @memberof aslivecasino.CardData
         * @static
         * @param {aslivecasino.ICardData=} [properties] Properties to set
         * @returns {aslivecasino.CardData} CardData instance
         */
        CardData.create = function create(properties) {
            return new CardData(properties);
        };

        /**
         * Encodes the specified CardData message. Does not implicitly {@link aslivecasino.CardData.verify|verify} messages.
         * @function encode
         * @memberof aslivecasino.CardData
         * @static
         * @param {aslivecasino.ICardData} message CardData message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        CardData.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.style != null && message.hasOwnProperty("style"))
                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.style);
            if (message.num != null && message.hasOwnProperty("num"))
                writer.uint32(/* id 2, wireType 0 =*/16).int32(message.num);
            if (message.open_status != null && message.hasOwnProperty("open_status"))
                writer.uint32(/* id 3, wireType 0 =*/24).int32(message.open_status);
            return writer;
        };

        /**
         * Encodes the specified CardData message, length delimited. Does not implicitly {@link aslivecasino.CardData.verify|verify} messages.
         * @function encodeDelimited
         * @memberof aslivecasino.CardData
         * @static
         * @param {aslivecasino.ICardData} message CardData message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        CardData.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a CardData message from the specified reader or buffer.
         * @function decode
         * @memberof aslivecasino.CardData
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {aslivecasino.CardData} CardData
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        CardData.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.aslivecasino.CardData();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.style = reader.int32();
                    break;
                case 2:
                    message.num = reader.int32();
                    break;
                case 3:
                    message.open_status = reader.int32();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a CardData message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof aslivecasino.CardData
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {aslivecasino.CardData} CardData
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        CardData.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a CardData message.
         * @function verify
         * @memberof aslivecasino.CardData
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        CardData.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.style != null && message.hasOwnProperty("style"))
                switch (message.style) {
                default:
                    return "style: enum value expected";
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                    break;
                }
            if (message.num != null && message.hasOwnProperty("num"))
                if (!$util.isInteger(message.num))
                    return "num: integer expected";
            if (message.open_status != null && message.hasOwnProperty("open_status"))
                switch (message.open_status) {
                default:
                    return "open_status: enum value expected";
                case 0:
                case 1:
                case 2:
                    break;
                }
            return null;
        };

        /**
         * Creates a CardData message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof aslivecasino.CardData
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {aslivecasino.CardData} CardData
         */
        CardData.fromObject = function fromObject(object) {
            if (object instanceof $root.aslivecasino.CardData)
                return object;
            var message = new $root.aslivecasino.CardData();
            switch (object.style) {
            case "STYLENONE":
            case 0:
                message.style = 0;
                break;
            case "SPADE":
            case 1:
                message.style = 1;
                break;
            case "HEART":
            case 2:
                message.style = 2;
                break;
            case "DIAMOND":
            case 3:
                message.style = 3;
                break;
            case "CLUB":
            case 4:
                message.style = 4;
                break;
            }
            if (object.num != null)
                message.num = object.num | 0;
            switch (object.open_status) {
            case "NOCARD":
            case 0:
                message.open_status = 0;
                break;
            case "UNOPEN":
            case 1:
                message.open_status = 1;
                break;
            case "OPENED":
            case 2:
                message.open_status = 2;
                break;
            }
            return message;
        };

        /**
         * Creates a plain object from a CardData message. Also converts values to other types if specified.
         * @function toObject
         * @memberof aslivecasino.CardData
         * @static
         * @param {aslivecasino.CardData} message CardData
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        CardData.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.style = options.enums === String ? "STYLENONE" : 0;
                object.num = 0;
                object.open_status = options.enums === String ? "NOCARD" : 0;
            }
            if (message.style != null && message.hasOwnProperty("style"))
                object.style = options.enums === String ? $root.aslivecasino.CardData.EnumCardStyle[message.style] : message.style;
            if (message.num != null && message.hasOwnProperty("num"))
                object.num = message.num;
            if (message.open_status != null && message.hasOwnProperty("open_status"))
                object.open_status = options.enums === String ? $root.aslivecasino.CardData.EnumCardOpenStatus[message.open_status] : message.open_status;
            return object;
        };

        /**
         * Converts this CardData to JSON.
         * @function toJSON
         * @memberof aslivecasino.CardData
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        CardData.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * EnumCardStyle enum.
         * @name aslivecasino.CardData.EnumCardStyle
         * @enum {string}
         * @property {number} STYLENONE=0 STYLENONE value
         * @property {number} SPADE=1 SPADE value
         * @property {number} HEART=2 HEART value
         * @property {number} DIAMOND=3 DIAMOND value
         * @property {number} CLUB=4 CLUB value
         */
        CardData.EnumCardStyle = (function() {
            var valuesById = {}, values = Object.create(valuesById);
            values[valuesById[0] = "STYLENONE"] = 0;
            values[valuesById[1] = "SPADE"] = 1;
            values[valuesById[2] = "HEART"] = 2;
            values[valuesById[3] = "DIAMOND"] = 3;
            values[valuesById[4] = "CLUB"] = 4;
            return values;
        })();

        /**
         * EnumCardOpenStatus enum.
         * @name aslivecasino.CardData.EnumCardOpenStatus
         * @enum {string}
         * @property {number} NOCARD=0 NOCARD value
         * @property {number} UNOPEN=1 UNOPEN value
         * @property {number} OPENED=2 OPENED value
         */
        CardData.EnumCardOpenStatus = (function() {
            var valuesById = {}, values = Object.create(valuesById);
            values[valuesById[0] = "NOCARD"] = 0;
            values[valuesById[1] = "UNOPEN"] = 1;
            values[valuesById[2] = "OPENED"] = 2;
            return values;
        })();

        return CardData;
    })();

    /**
     * Bac_EnumDivType enum.
     * @name aslivecasino.Bac_EnumDivType
     * @enum {string}
     * @property {number} PLAYER=0 PLAYER value
     * @property {number} BANKER=1 BANKER value
     * @property {number} TIE=2 TIE value
     * @property {number} PPAIR=3 PPAIR value
     * @property {number} BPAIR=4 BPAIR value
     */
    aslivecasino.Bac_EnumDivType = (function() {
        var valuesById = {}, values = Object.create(valuesById);
        values[valuesById[0] = "PLAYER"] = 0;
        values[valuesById[1] = "BANKER"] = 1;
        values[valuesById[2] = "TIE"] = 2;
        values[valuesById[3] = "PPAIR"] = 3;
        values[valuesById[4] = "BPAIR"] = 4;
        return values;
    })();

    aslivecasino.Bac_BettingDivStat = (function() {

        /**
         * Properties of a Bac_BettingDivStat.
         * @memberof aslivecasino
         * @interface IBac_BettingDivStat
         * @property {aslivecasino.Bac_EnumDivType|null} [div_type] Bac_BettingDivStat div_type
         * @property {number|null} [num_users] Bac_BettingDivStat num_users
         * @property {number|null} [total_bet_chips] Bac_BettingDivStat total_bet_chips
         */

        /**
         * Constructs a new Bac_BettingDivStat.
         * @memberof aslivecasino
         * @classdesc Represents a Bac_BettingDivStat.
         * @implements IBac_BettingDivStat
         * @constructor
         * @param {aslivecasino.IBac_BettingDivStat=} [properties] Properties to set
         */
        function Bac_BettingDivStat(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Bac_BettingDivStat div_type.
         * @member {aslivecasino.Bac_EnumDivType} div_type
         * @memberof aslivecasino.Bac_BettingDivStat
         * @instance
         */
        Bac_BettingDivStat.prototype.div_type = 0;

        /**
         * Bac_BettingDivStat num_users.
         * @member {number} num_users
         * @memberof aslivecasino.Bac_BettingDivStat
         * @instance
         */
        Bac_BettingDivStat.prototype.num_users = 0;

        /**
         * Bac_BettingDivStat total_bet_chips.
         * @member {number} total_bet_chips
         * @memberof aslivecasino.Bac_BettingDivStat
         * @instance
         */
        Bac_BettingDivStat.prototype.total_bet_chips = 0;

        /**
         * Creates a new Bac_BettingDivStat instance using the specified properties.
         * @function create
         * @memberof aslivecasino.Bac_BettingDivStat
         * @static
         * @param {aslivecasino.IBac_BettingDivStat=} [properties] Properties to set
         * @returns {aslivecasino.Bac_BettingDivStat} Bac_BettingDivStat instance
         */
        Bac_BettingDivStat.create = function create(properties) {
            return new Bac_BettingDivStat(properties);
        };

        /**
         * Encodes the specified Bac_BettingDivStat message. Does not implicitly {@link aslivecasino.Bac_BettingDivStat.verify|verify} messages.
         * @function encode
         * @memberof aslivecasino.Bac_BettingDivStat
         * @static
         * @param {aslivecasino.IBac_BettingDivStat} message Bac_BettingDivStat message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Bac_BettingDivStat.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.div_type != null && message.hasOwnProperty("div_type"))
                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.div_type);
            if (message.num_users != null && message.hasOwnProperty("num_users"))
                writer.uint32(/* id 2, wireType 0 =*/16).int32(message.num_users);
            if (message.total_bet_chips != null && message.hasOwnProperty("total_bet_chips"))
                writer.uint32(/* id 3, wireType 0 =*/24).int32(message.total_bet_chips);
            return writer;
        };

        /**
         * Encodes the specified Bac_BettingDivStat message, length delimited. Does not implicitly {@link aslivecasino.Bac_BettingDivStat.verify|verify} messages.
         * @function encodeDelimited
         * @memberof aslivecasino.Bac_BettingDivStat
         * @static
         * @param {aslivecasino.IBac_BettingDivStat} message Bac_BettingDivStat message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Bac_BettingDivStat.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Bac_BettingDivStat message from the specified reader or buffer.
         * @function decode
         * @memberof aslivecasino.Bac_BettingDivStat
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {aslivecasino.Bac_BettingDivStat} Bac_BettingDivStat
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Bac_BettingDivStat.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.aslivecasino.Bac_BettingDivStat();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.div_type = reader.int32();
                    break;
                case 2:
                    message.num_users = reader.int32();
                    break;
                case 3:
                    message.total_bet_chips = reader.int32();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Bac_BettingDivStat message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof aslivecasino.Bac_BettingDivStat
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {aslivecasino.Bac_BettingDivStat} Bac_BettingDivStat
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Bac_BettingDivStat.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Bac_BettingDivStat message.
         * @function verify
         * @memberof aslivecasino.Bac_BettingDivStat
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Bac_BettingDivStat.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.div_type != null && message.hasOwnProperty("div_type"))
                switch (message.div_type) {
                default:
                    return "div_type: enum value expected";
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                    break;
                }
            if (message.num_users != null && message.hasOwnProperty("num_users"))
                if (!$util.isInteger(message.num_users))
                    return "num_users: integer expected";
            if (message.total_bet_chips != null && message.hasOwnProperty("total_bet_chips"))
                if (!$util.isInteger(message.total_bet_chips))
                    return "total_bet_chips: integer expected";
            return null;
        };

        /**
         * Creates a Bac_BettingDivStat message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof aslivecasino.Bac_BettingDivStat
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {aslivecasino.Bac_BettingDivStat} Bac_BettingDivStat
         */
        Bac_BettingDivStat.fromObject = function fromObject(object) {
            if (object instanceof $root.aslivecasino.Bac_BettingDivStat)
                return object;
            var message = new $root.aslivecasino.Bac_BettingDivStat();
            switch (object.div_type) {
            case "PLAYER":
            case 0:
                message.div_type = 0;
                break;
            case "BANKER":
            case 1:
                message.div_type = 1;
                break;
            case "TIE":
            case 2:
                message.div_type = 2;
                break;
            case "PPAIR":
            case 3:
                message.div_type = 3;
                break;
            case "BPAIR":
            case 4:
                message.div_type = 4;
                break;
            }
            if (object.num_users != null)
                message.num_users = object.num_users | 0;
            if (object.total_bet_chips != null)
                message.total_bet_chips = object.total_bet_chips | 0;
            return message;
        };

        /**
         * Creates a plain object from a Bac_BettingDivStat message. Also converts values to other types if specified.
         * @function toObject
         * @memberof aslivecasino.Bac_BettingDivStat
         * @static
         * @param {aslivecasino.Bac_BettingDivStat} message Bac_BettingDivStat
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Bac_BettingDivStat.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.div_type = options.enums === String ? "PLAYER" : 0;
                object.num_users = 0;
                object.total_bet_chips = 0;
            }
            if (message.div_type != null && message.hasOwnProperty("div_type"))
                object.div_type = options.enums === String ? $root.aslivecasino.Bac_EnumDivType[message.div_type] : message.div_type;
            if (message.num_users != null && message.hasOwnProperty("num_users"))
                object.num_users = message.num_users;
            if (message.total_bet_chips != null && message.hasOwnProperty("total_bet_chips"))
                object.total_bet_chips = message.total_bet_chips;
            return object;
        };

        /**
         * Converts this Bac_BettingDivStat to JSON.
         * @function toJSON
         * @memberof aslivecasino.Bac_BettingDivStat
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Bac_BettingDivStat.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return Bac_BettingDivStat;
    })();

    aslivecasino.Bac_RoomRoutingUpdate_Outside = (function() {

        /**
         * Properties of a Bac_RoomRoutingUpdate_Outside.
         * @memberof aslivecasino
         * @interface IBac_RoomRoutingUpdate_Outside
         * @property {string|null} [room_no] Bac_RoomRoutingUpdate_Outside room_no
         * @property {number|null} [room_state] Bac_RoomRoutingUpdate_Outside room_state
         * @property {number|null} [count_down] Bac_RoomRoutingUpdate_Outside count_down
         * @property {number|null} [num_room_users] Bac_RoomRoutingUpdate_Outside num_room_users
         * @property {number|null} [total_user_credit] Bac_RoomRoutingUpdate_Outside total_user_credit
         * @property {number|null} [num_empty_seats] Bac_RoomRoutingUpdate_Outside num_empty_seats
         * @property {Array.<aslivecasino.IBac_BettingDivStat>|null} [bet_div_stat_list] Bac_RoomRoutingUpdate_Outside bet_div_stat_list
         */

        /**
         * Constructs a new Bac_RoomRoutingUpdate_Outside.
         * @memberof aslivecasino
         * @classdesc Represents a Bac_RoomRoutingUpdate_Outside.
         * @implements IBac_RoomRoutingUpdate_Outside
         * @constructor
         * @param {aslivecasino.IBac_RoomRoutingUpdate_Outside=} [properties] Properties to set
         */
        function Bac_RoomRoutingUpdate_Outside(properties) {
            this.bet_div_stat_list = [];
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Bac_RoomRoutingUpdate_Outside room_no.
         * @member {string} room_no
         * @memberof aslivecasino.Bac_RoomRoutingUpdate_Outside
         * @instance
         */
        Bac_RoomRoutingUpdate_Outside.prototype.room_no = "";

        /**
         * Bac_RoomRoutingUpdate_Outside room_state.
         * @member {number} room_state
         * @memberof aslivecasino.Bac_RoomRoutingUpdate_Outside
         * @instance
         */
        Bac_RoomRoutingUpdate_Outside.prototype.room_state = 0;

        /**
         * Bac_RoomRoutingUpdate_Outside count_down.
         * @member {number} count_down
         * @memberof aslivecasino.Bac_RoomRoutingUpdate_Outside
         * @instance
         */
        Bac_RoomRoutingUpdate_Outside.prototype.count_down = 0;

        /**
         * Bac_RoomRoutingUpdate_Outside num_room_users.
         * @member {number} num_room_users
         * @memberof aslivecasino.Bac_RoomRoutingUpdate_Outside
         * @instance
         */
        Bac_RoomRoutingUpdate_Outside.prototype.num_room_users = 0;

        /**
         * Bac_RoomRoutingUpdate_Outside total_user_credit.
         * @member {number} total_user_credit
         * @memberof aslivecasino.Bac_RoomRoutingUpdate_Outside
         * @instance
         */
        Bac_RoomRoutingUpdate_Outside.prototype.total_user_credit = 0;

        /**
         * Bac_RoomRoutingUpdate_Outside num_empty_seats.
         * @member {number} num_empty_seats
         * @memberof aslivecasino.Bac_RoomRoutingUpdate_Outside
         * @instance
         */
        Bac_RoomRoutingUpdate_Outside.prototype.num_empty_seats = 0;

        /**
         * Bac_RoomRoutingUpdate_Outside bet_div_stat_list.
         * @member {Array.<aslivecasino.IBac_BettingDivStat>} bet_div_stat_list
         * @memberof aslivecasino.Bac_RoomRoutingUpdate_Outside
         * @instance
         */
        Bac_RoomRoutingUpdate_Outside.prototype.bet_div_stat_list = $util.emptyArray;

        /**
         * Creates a new Bac_RoomRoutingUpdate_Outside instance using the specified properties.
         * @function create
         * @memberof aslivecasino.Bac_RoomRoutingUpdate_Outside
         * @static
         * @param {aslivecasino.IBac_RoomRoutingUpdate_Outside=} [properties] Properties to set
         * @returns {aslivecasino.Bac_RoomRoutingUpdate_Outside} Bac_RoomRoutingUpdate_Outside instance
         */
        Bac_RoomRoutingUpdate_Outside.create = function create(properties) {
            return new Bac_RoomRoutingUpdate_Outside(properties);
        };

        /**
         * Encodes the specified Bac_RoomRoutingUpdate_Outside message. Does not implicitly {@link aslivecasino.Bac_RoomRoutingUpdate_Outside.verify|verify} messages.
         * @function encode
         * @memberof aslivecasino.Bac_RoomRoutingUpdate_Outside
         * @static
         * @param {aslivecasino.IBac_RoomRoutingUpdate_Outside} message Bac_RoomRoutingUpdate_Outside message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Bac_RoomRoutingUpdate_Outside.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.room_no);
            if (message.room_state != null && message.hasOwnProperty("room_state"))
                writer.uint32(/* id 2, wireType 0 =*/16).int32(message.room_state);
            if (message.count_down != null && message.hasOwnProperty("count_down"))
                writer.uint32(/* id 3, wireType 0 =*/24).int32(message.count_down);
            if (message.num_room_users != null && message.hasOwnProperty("num_room_users"))
                writer.uint32(/* id 4, wireType 0 =*/32).int32(message.num_room_users);
            if (message.total_user_credit != null && message.hasOwnProperty("total_user_credit"))
                writer.uint32(/* id 5, wireType 1 =*/41).double(message.total_user_credit);
            if (message.num_empty_seats != null && message.hasOwnProperty("num_empty_seats"))
                writer.uint32(/* id 6, wireType 0 =*/48).int32(message.num_empty_seats);
            if (message.bet_div_stat_list != null && message.bet_div_stat_list.length)
                for (var i = 0; i < message.bet_div_stat_list.length; ++i)
                    $root.aslivecasino.Bac_BettingDivStat.encode(message.bet_div_stat_list[i], writer.uint32(/* id 7, wireType 2 =*/58).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified Bac_RoomRoutingUpdate_Outside message, length delimited. Does not implicitly {@link aslivecasino.Bac_RoomRoutingUpdate_Outside.verify|verify} messages.
         * @function encodeDelimited
         * @memberof aslivecasino.Bac_RoomRoutingUpdate_Outside
         * @static
         * @param {aslivecasino.IBac_RoomRoutingUpdate_Outside} message Bac_RoomRoutingUpdate_Outside message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Bac_RoomRoutingUpdate_Outside.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Bac_RoomRoutingUpdate_Outside message from the specified reader or buffer.
         * @function decode
         * @memberof aslivecasino.Bac_RoomRoutingUpdate_Outside
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {aslivecasino.Bac_RoomRoutingUpdate_Outside} Bac_RoomRoutingUpdate_Outside
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Bac_RoomRoutingUpdate_Outside.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.aslivecasino.Bac_RoomRoutingUpdate_Outside();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.room_no = reader.string();
                    break;
                case 2:
                    message.room_state = reader.int32();
                    break;
                case 3:
                    message.count_down = reader.int32();
                    break;
                case 4:
                    message.num_room_users = reader.int32();
                    break;
                case 5:
                    message.total_user_credit = reader.double();
                    break;
                case 6:
                    message.num_empty_seats = reader.int32();
                    break;
                case 7:
                    if (!(message.bet_div_stat_list && message.bet_div_stat_list.length))
                        message.bet_div_stat_list = [];
                    message.bet_div_stat_list.push($root.aslivecasino.Bac_BettingDivStat.decode(reader, reader.uint32()));
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Bac_RoomRoutingUpdate_Outside message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof aslivecasino.Bac_RoomRoutingUpdate_Outside
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {aslivecasino.Bac_RoomRoutingUpdate_Outside} Bac_RoomRoutingUpdate_Outside
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Bac_RoomRoutingUpdate_Outside.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Bac_RoomRoutingUpdate_Outside message.
         * @function verify
         * @memberof aslivecasino.Bac_RoomRoutingUpdate_Outside
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Bac_RoomRoutingUpdate_Outside.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                if (!$util.isString(message.room_no))
                    return "room_no: string expected";
            if (message.room_state != null && message.hasOwnProperty("room_state"))
                if (!$util.isInteger(message.room_state))
                    return "room_state: integer expected";
            if (message.count_down != null && message.hasOwnProperty("count_down"))
                if (!$util.isInteger(message.count_down))
                    return "count_down: integer expected";
            if (message.num_room_users != null && message.hasOwnProperty("num_room_users"))
                if (!$util.isInteger(message.num_room_users))
                    return "num_room_users: integer expected";
            if (message.total_user_credit != null && message.hasOwnProperty("total_user_credit"))
                if (typeof message.total_user_credit !== "number")
                    return "total_user_credit: number expected";
            if (message.num_empty_seats != null && message.hasOwnProperty("num_empty_seats"))
                if (!$util.isInteger(message.num_empty_seats))
                    return "num_empty_seats: integer expected";
            if (message.bet_div_stat_list != null && message.hasOwnProperty("bet_div_stat_list")) {
                if (!Array.isArray(message.bet_div_stat_list))
                    return "bet_div_stat_list: array expected";
                for (var i = 0; i < message.bet_div_stat_list.length; ++i) {
                    var error = $root.aslivecasino.Bac_BettingDivStat.verify(message.bet_div_stat_list[i]);
                    if (error)
                        return "bet_div_stat_list." + error;
                }
            }
            return null;
        };

        /**
         * Creates a Bac_RoomRoutingUpdate_Outside message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof aslivecasino.Bac_RoomRoutingUpdate_Outside
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {aslivecasino.Bac_RoomRoutingUpdate_Outside} Bac_RoomRoutingUpdate_Outside
         */
        Bac_RoomRoutingUpdate_Outside.fromObject = function fromObject(object) {
            if (object instanceof $root.aslivecasino.Bac_RoomRoutingUpdate_Outside)
                return object;
            var message = new $root.aslivecasino.Bac_RoomRoutingUpdate_Outside();
            if (object.room_no != null)
                message.room_no = String(object.room_no);
            if (object.room_state != null)
                message.room_state = object.room_state | 0;
            if (object.count_down != null)
                message.count_down = object.count_down | 0;
            if (object.num_room_users != null)
                message.num_room_users = object.num_room_users | 0;
            if (object.total_user_credit != null)
                message.total_user_credit = Number(object.total_user_credit);
            if (object.num_empty_seats != null)
                message.num_empty_seats = object.num_empty_seats | 0;
            if (object.bet_div_stat_list) {
                if (!Array.isArray(object.bet_div_stat_list))
                    throw TypeError(".aslivecasino.Bac_RoomRoutingUpdate_Outside.bet_div_stat_list: array expected");
                message.bet_div_stat_list = [];
                for (var i = 0; i < object.bet_div_stat_list.length; ++i) {
                    if (typeof object.bet_div_stat_list[i] !== "object")
                        throw TypeError(".aslivecasino.Bac_RoomRoutingUpdate_Outside.bet_div_stat_list: object expected");
                    message.bet_div_stat_list[i] = $root.aslivecasino.Bac_BettingDivStat.fromObject(object.bet_div_stat_list[i]);
                }
            }
            return message;
        };

        /**
         * Creates a plain object from a Bac_RoomRoutingUpdate_Outside message. Also converts values to other types if specified.
         * @function toObject
         * @memberof aslivecasino.Bac_RoomRoutingUpdate_Outside
         * @static
         * @param {aslivecasino.Bac_RoomRoutingUpdate_Outside} message Bac_RoomRoutingUpdate_Outside
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Bac_RoomRoutingUpdate_Outside.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.arrays || options.defaults)
                object.bet_div_stat_list = [];
            if (options.defaults) {
                object.room_no = "";
                object.room_state = 0;
                object.count_down = 0;
                object.num_room_users = 0;
                object.total_user_credit = 0;
                object.num_empty_seats = 0;
            }
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                object.room_no = message.room_no;
            if (message.room_state != null && message.hasOwnProperty("room_state"))
                object.room_state = message.room_state;
            if (message.count_down != null && message.hasOwnProperty("count_down"))
                object.count_down = message.count_down;
            if (message.num_room_users != null && message.hasOwnProperty("num_room_users"))
                object.num_room_users = message.num_room_users;
            if (message.total_user_credit != null && message.hasOwnProperty("total_user_credit"))
                object.total_user_credit = options.json && !isFinite(message.total_user_credit) ? String(message.total_user_credit) : message.total_user_credit;
            if (message.num_empty_seats != null && message.hasOwnProperty("num_empty_seats"))
                object.num_empty_seats = message.num_empty_seats;
            if (message.bet_div_stat_list && message.bet_div_stat_list.length) {
                object.bet_div_stat_list = [];
                for (var j = 0; j < message.bet_div_stat_list.length; ++j)
                    object.bet_div_stat_list[j] = $root.aslivecasino.Bac_BettingDivStat.toObject(message.bet_div_stat_list[j], options);
            }
            return object;
        };

        /**
         * Converts this Bac_RoomRoutingUpdate_Outside to JSON.
         * @function toJSON
         * @memberof aslivecasino.Bac_RoomRoutingUpdate_Outside
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Bac_RoomRoutingUpdate_Outside.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return Bac_RoomRoutingUpdate_Outside;
    })();

    aslivecasino.Bac_RoomRoutingUpdate_Inside = (function() {

        /**
         * Properties of a Bac_RoomRoutingUpdate_Inside.
         * @memberof aslivecasino
         * @interface IBac_RoomRoutingUpdate_Inside
         * @property {string|null} [room_no] Bac_RoomRoutingUpdate_Inside room_no
         * @property {number|null} [num_room_users] Bac_RoomRoutingUpdate_Inside num_room_users
         * @property {number|null} [total_user_credit] Bac_RoomRoutingUpdate_Inside total_user_credit
         * @property {Array.<aslivecasino.IBac_BettingDivStat>|null} [bet_div_stat_list] Bac_RoomRoutingUpdate_Inside bet_div_stat_list
         * @property {Array.<aslivecasino.Bac_RoomRoutingUpdate_Inside.IBettingLeaderBoard>|null} [leaderboard] Bac_RoomRoutingUpdate_Inside leaderboard
         */

        /**
         * Constructs a new Bac_RoomRoutingUpdate_Inside.
         * @memberof aslivecasino
         * @classdesc Represents a Bac_RoomRoutingUpdate_Inside.
         * @implements IBac_RoomRoutingUpdate_Inside
         * @constructor
         * @param {aslivecasino.IBac_RoomRoutingUpdate_Inside=} [properties] Properties to set
         */
        function Bac_RoomRoutingUpdate_Inside(properties) {
            this.bet_div_stat_list = [];
            this.leaderboard = [];
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Bac_RoomRoutingUpdate_Inside room_no.
         * @member {string} room_no
         * @memberof aslivecasino.Bac_RoomRoutingUpdate_Inside
         * @instance
         */
        Bac_RoomRoutingUpdate_Inside.prototype.room_no = "";

        /**
         * Bac_RoomRoutingUpdate_Inside num_room_users.
         * @member {number} num_room_users
         * @memberof aslivecasino.Bac_RoomRoutingUpdate_Inside
         * @instance
         */
        Bac_RoomRoutingUpdate_Inside.prototype.num_room_users = 0;

        /**
         * Bac_RoomRoutingUpdate_Inside total_user_credit.
         * @member {number} total_user_credit
         * @memberof aslivecasino.Bac_RoomRoutingUpdate_Inside
         * @instance
         */
        Bac_RoomRoutingUpdate_Inside.prototype.total_user_credit = 0;

        /**
         * Bac_RoomRoutingUpdate_Inside bet_div_stat_list.
         * @member {Array.<aslivecasino.IBac_BettingDivStat>} bet_div_stat_list
         * @memberof aslivecasino.Bac_RoomRoutingUpdate_Inside
         * @instance
         */
        Bac_RoomRoutingUpdate_Inside.prototype.bet_div_stat_list = $util.emptyArray;

        /**
         * Bac_RoomRoutingUpdate_Inside leaderboard.
         * @member {Array.<aslivecasino.Bac_RoomRoutingUpdate_Inside.IBettingLeaderBoard>} leaderboard
         * @memberof aslivecasino.Bac_RoomRoutingUpdate_Inside
         * @instance
         */
        Bac_RoomRoutingUpdate_Inside.prototype.leaderboard = $util.emptyArray;

        /**
         * Creates a new Bac_RoomRoutingUpdate_Inside instance using the specified properties.
         * @function create
         * @memberof aslivecasino.Bac_RoomRoutingUpdate_Inside
         * @static
         * @param {aslivecasino.IBac_RoomRoutingUpdate_Inside=} [properties] Properties to set
         * @returns {aslivecasino.Bac_RoomRoutingUpdate_Inside} Bac_RoomRoutingUpdate_Inside instance
         */
        Bac_RoomRoutingUpdate_Inside.create = function create(properties) {
            return new Bac_RoomRoutingUpdate_Inside(properties);
        };

        /**
         * Encodes the specified Bac_RoomRoutingUpdate_Inside message. Does not implicitly {@link aslivecasino.Bac_RoomRoutingUpdate_Inside.verify|verify} messages.
         * @function encode
         * @memberof aslivecasino.Bac_RoomRoutingUpdate_Inside
         * @static
         * @param {aslivecasino.IBac_RoomRoutingUpdate_Inside} message Bac_RoomRoutingUpdate_Inside message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Bac_RoomRoutingUpdate_Inside.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.room_no);
            if (message.num_room_users != null && message.hasOwnProperty("num_room_users"))
                writer.uint32(/* id 2, wireType 0 =*/16).int32(message.num_room_users);
            if (message.total_user_credit != null && message.hasOwnProperty("total_user_credit"))
                writer.uint32(/* id 3, wireType 1 =*/25).double(message.total_user_credit);
            if (message.bet_div_stat_list != null && message.bet_div_stat_list.length)
                for (var i = 0; i < message.bet_div_stat_list.length; ++i)
                    $root.aslivecasino.Bac_BettingDivStat.encode(message.bet_div_stat_list[i], writer.uint32(/* id 4, wireType 2 =*/34).fork()).ldelim();
            if (message.leaderboard != null && message.leaderboard.length)
                for (var i = 0; i < message.leaderboard.length; ++i)
                    $root.aslivecasino.Bac_RoomRoutingUpdate_Inside.BettingLeaderBoard.encode(message.leaderboard[i], writer.uint32(/* id 5, wireType 2 =*/42).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified Bac_RoomRoutingUpdate_Inside message, length delimited. Does not implicitly {@link aslivecasino.Bac_RoomRoutingUpdate_Inside.verify|verify} messages.
         * @function encodeDelimited
         * @memberof aslivecasino.Bac_RoomRoutingUpdate_Inside
         * @static
         * @param {aslivecasino.IBac_RoomRoutingUpdate_Inside} message Bac_RoomRoutingUpdate_Inside message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Bac_RoomRoutingUpdate_Inside.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Bac_RoomRoutingUpdate_Inside message from the specified reader or buffer.
         * @function decode
         * @memberof aslivecasino.Bac_RoomRoutingUpdate_Inside
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {aslivecasino.Bac_RoomRoutingUpdate_Inside} Bac_RoomRoutingUpdate_Inside
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Bac_RoomRoutingUpdate_Inside.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.aslivecasino.Bac_RoomRoutingUpdate_Inside();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.room_no = reader.string();
                    break;
                case 2:
                    message.num_room_users = reader.int32();
                    break;
                case 3:
                    message.total_user_credit = reader.double();
                    break;
                case 4:
                    if (!(message.bet_div_stat_list && message.bet_div_stat_list.length))
                        message.bet_div_stat_list = [];
                    message.bet_div_stat_list.push($root.aslivecasino.Bac_BettingDivStat.decode(reader, reader.uint32()));
                    break;
                case 5:
                    if (!(message.leaderboard && message.leaderboard.length))
                        message.leaderboard = [];
                    message.leaderboard.push($root.aslivecasino.Bac_RoomRoutingUpdate_Inside.BettingLeaderBoard.decode(reader, reader.uint32()));
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Bac_RoomRoutingUpdate_Inside message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof aslivecasino.Bac_RoomRoutingUpdate_Inside
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {aslivecasino.Bac_RoomRoutingUpdate_Inside} Bac_RoomRoutingUpdate_Inside
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Bac_RoomRoutingUpdate_Inside.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Bac_RoomRoutingUpdate_Inside message.
         * @function verify
         * @memberof aslivecasino.Bac_RoomRoutingUpdate_Inside
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Bac_RoomRoutingUpdate_Inside.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                if (!$util.isString(message.room_no))
                    return "room_no: string expected";
            if (message.num_room_users != null && message.hasOwnProperty("num_room_users"))
                if (!$util.isInteger(message.num_room_users))
                    return "num_room_users: integer expected";
            if (message.total_user_credit != null && message.hasOwnProperty("total_user_credit"))
                if (typeof message.total_user_credit !== "number")
                    return "total_user_credit: number expected";
            if (message.bet_div_stat_list != null && message.hasOwnProperty("bet_div_stat_list")) {
                if (!Array.isArray(message.bet_div_stat_list))
                    return "bet_div_stat_list: array expected";
                for (var i = 0; i < message.bet_div_stat_list.length; ++i) {
                    var error = $root.aslivecasino.Bac_BettingDivStat.verify(message.bet_div_stat_list[i]);
                    if (error)
                        return "bet_div_stat_list." + error;
                }
            }
            if (message.leaderboard != null && message.hasOwnProperty("leaderboard")) {
                if (!Array.isArray(message.leaderboard))
                    return "leaderboard: array expected";
                for (var i = 0; i < message.leaderboard.length; ++i) {
                    var error = $root.aslivecasino.Bac_RoomRoutingUpdate_Inside.BettingLeaderBoard.verify(message.leaderboard[i]);
                    if (error)
                        return "leaderboard." + error;
                }
            }
            return null;
        };

        /**
         * Creates a Bac_RoomRoutingUpdate_Inside message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof aslivecasino.Bac_RoomRoutingUpdate_Inside
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {aslivecasino.Bac_RoomRoutingUpdate_Inside} Bac_RoomRoutingUpdate_Inside
         */
        Bac_RoomRoutingUpdate_Inside.fromObject = function fromObject(object) {
            if (object instanceof $root.aslivecasino.Bac_RoomRoutingUpdate_Inside)
                return object;
            var message = new $root.aslivecasino.Bac_RoomRoutingUpdate_Inside();
            if (object.room_no != null)
                message.room_no = String(object.room_no);
            if (object.num_room_users != null)
                message.num_room_users = object.num_room_users | 0;
            if (object.total_user_credit != null)
                message.total_user_credit = Number(object.total_user_credit);
            if (object.bet_div_stat_list) {
                if (!Array.isArray(object.bet_div_stat_list))
                    throw TypeError(".aslivecasino.Bac_RoomRoutingUpdate_Inside.bet_div_stat_list: array expected");
                message.bet_div_stat_list = [];
                for (var i = 0; i < object.bet_div_stat_list.length; ++i) {
                    if (typeof object.bet_div_stat_list[i] !== "object")
                        throw TypeError(".aslivecasino.Bac_RoomRoutingUpdate_Inside.bet_div_stat_list: object expected");
                    message.bet_div_stat_list[i] = $root.aslivecasino.Bac_BettingDivStat.fromObject(object.bet_div_stat_list[i]);
                }
            }
            if (object.leaderboard) {
                if (!Array.isArray(object.leaderboard))
                    throw TypeError(".aslivecasino.Bac_RoomRoutingUpdate_Inside.leaderboard: array expected");
                message.leaderboard = [];
                for (var i = 0; i < object.leaderboard.length; ++i) {
                    if (typeof object.leaderboard[i] !== "object")
                        throw TypeError(".aslivecasino.Bac_RoomRoutingUpdate_Inside.leaderboard: object expected");
                    message.leaderboard[i] = $root.aslivecasino.Bac_RoomRoutingUpdate_Inside.BettingLeaderBoard.fromObject(object.leaderboard[i]);
                }
            }
            return message;
        };

        /**
         * Creates a plain object from a Bac_RoomRoutingUpdate_Inside message. Also converts values to other types if specified.
         * @function toObject
         * @memberof aslivecasino.Bac_RoomRoutingUpdate_Inside
         * @static
         * @param {aslivecasino.Bac_RoomRoutingUpdate_Inside} message Bac_RoomRoutingUpdate_Inside
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Bac_RoomRoutingUpdate_Inside.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.arrays || options.defaults) {
                object.bet_div_stat_list = [];
                object.leaderboard = [];
            }
            if (options.defaults) {
                object.room_no = "";
                object.num_room_users = 0;
                object.total_user_credit = 0;
            }
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                object.room_no = message.room_no;
            if (message.num_room_users != null && message.hasOwnProperty("num_room_users"))
                object.num_room_users = message.num_room_users;
            if (message.total_user_credit != null && message.hasOwnProperty("total_user_credit"))
                object.total_user_credit = options.json && !isFinite(message.total_user_credit) ? String(message.total_user_credit) : message.total_user_credit;
            if (message.bet_div_stat_list && message.bet_div_stat_list.length) {
                object.bet_div_stat_list = [];
                for (var j = 0; j < message.bet_div_stat_list.length; ++j)
                    object.bet_div_stat_list[j] = $root.aslivecasino.Bac_BettingDivStat.toObject(message.bet_div_stat_list[j], options);
            }
            if (message.leaderboard && message.leaderboard.length) {
                object.leaderboard = [];
                for (var j = 0; j < message.leaderboard.length; ++j)
                    object.leaderboard[j] = $root.aslivecasino.Bac_RoomRoutingUpdate_Inside.BettingLeaderBoard.toObject(message.leaderboard[j], options);
            }
            return object;
        };

        /**
         * Converts this Bac_RoomRoutingUpdate_Inside to JSON.
         * @function toJSON
         * @memberof aslivecasino.Bac_RoomRoutingUpdate_Inside
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Bac_RoomRoutingUpdate_Inside.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        Bac_RoomRoutingUpdate_Inside.BettingLeaderBoard = (function() {

            /**
             * Properties of a BettingLeaderBoard.
             * @memberof aslivecasino.Bac_RoomRoutingUpdate_Inside
             * @interface IBettingLeaderBoard
             * @property {string|null} [table_name] BettingLeaderBoard table_name
             * @property {number|null} [seat_no] BettingLeaderBoard seat_no
             * @property {string|null} [user_name] BettingLeaderBoard user_name
             * @property {number|null} [total_bet_chips] BettingLeaderBoard total_bet_chips
             */

            /**
             * Constructs a new BettingLeaderBoard.
             * @memberof aslivecasino.Bac_RoomRoutingUpdate_Inside
             * @classdesc Represents a BettingLeaderBoard.
             * @implements IBettingLeaderBoard
             * @constructor
             * @param {aslivecasino.Bac_RoomRoutingUpdate_Inside.IBettingLeaderBoard=} [properties] Properties to set
             */
            function BettingLeaderBoard(properties) {
                if (properties)
                    for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                        if (properties[keys[i]] != null)
                            this[keys[i]] = properties[keys[i]];
            }

            /**
             * BettingLeaderBoard table_name.
             * @member {string} table_name
             * @memberof aslivecasino.Bac_RoomRoutingUpdate_Inside.BettingLeaderBoard
             * @instance
             */
            BettingLeaderBoard.prototype.table_name = "";

            /**
             * BettingLeaderBoard seat_no.
             * @member {number} seat_no
             * @memberof aslivecasino.Bac_RoomRoutingUpdate_Inside.BettingLeaderBoard
             * @instance
             */
            BettingLeaderBoard.prototype.seat_no = 0;

            /**
             * BettingLeaderBoard user_name.
             * @member {string} user_name
             * @memberof aslivecasino.Bac_RoomRoutingUpdate_Inside.BettingLeaderBoard
             * @instance
             */
            BettingLeaderBoard.prototype.user_name = "";

            /**
             * BettingLeaderBoard total_bet_chips.
             * @member {number} total_bet_chips
             * @memberof aslivecasino.Bac_RoomRoutingUpdate_Inside.BettingLeaderBoard
             * @instance
             */
            BettingLeaderBoard.prototype.total_bet_chips = 0;

            /**
             * Creates a new BettingLeaderBoard instance using the specified properties.
             * @function create
             * @memberof aslivecasino.Bac_RoomRoutingUpdate_Inside.BettingLeaderBoard
             * @static
             * @param {aslivecasino.Bac_RoomRoutingUpdate_Inside.IBettingLeaderBoard=} [properties] Properties to set
             * @returns {aslivecasino.Bac_RoomRoutingUpdate_Inside.BettingLeaderBoard} BettingLeaderBoard instance
             */
            BettingLeaderBoard.create = function create(properties) {
                return new BettingLeaderBoard(properties);
            };

            /**
             * Encodes the specified BettingLeaderBoard message. Does not implicitly {@link aslivecasino.Bac_RoomRoutingUpdate_Inside.BettingLeaderBoard.verify|verify} messages.
             * @function encode
             * @memberof aslivecasino.Bac_RoomRoutingUpdate_Inside.BettingLeaderBoard
             * @static
             * @param {aslivecasino.Bac_RoomRoutingUpdate_Inside.IBettingLeaderBoard} message BettingLeaderBoard message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            BettingLeaderBoard.encode = function encode(message, writer) {
                if (!writer)
                    writer = $Writer.create();
                if (message.table_name != null && message.hasOwnProperty("table_name"))
                    writer.uint32(/* id 1, wireType 2 =*/10).string(message.table_name);
                if (message.seat_no != null && message.hasOwnProperty("seat_no"))
                    writer.uint32(/* id 2, wireType 0 =*/16).sint32(message.seat_no);
                if (message.user_name != null && message.hasOwnProperty("user_name"))
                    writer.uint32(/* id 3, wireType 2 =*/26).string(message.user_name);
                if (message.total_bet_chips != null && message.hasOwnProperty("total_bet_chips"))
                    writer.uint32(/* id 4, wireType 0 =*/32).int32(message.total_bet_chips);
                return writer;
            };

            /**
             * Encodes the specified BettingLeaderBoard message, length delimited. Does not implicitly {@link aslivecasino.Bac_RoomRoutingUpdate_Inside.BettingLeaderBoard.verify|verify} messages.
             * @function encodeDelimited
             * @memberof aslivecasino.Bac_RoomRoutingUpdate_Inside.BettingLeaderBoard
             * @static
             * @param {aslivecasino.Bac_RoomRoutingUpdate_Inside.IBettingLeaderBoard} message BettingLeaderBoard message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            BettingLeaderBoard.encodeDelimited = function encodeDelimited(message, writer) {
                return this.encode(message, writer).ldelim();
            };

            /**
             * Decodes a BettingLeaderBoard message from the specified reader or buffer.
             * @function decode
             * @memberof aslivecasino.Bac_RoomRoutingUpdate_Inside.BettingLeaderBoard
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @param {number} [length] Message length if known beforehand
             * @returns {aslivecasino.Bac_RoomRoutingUpdate_Inside.BettingLeaderBoard} BettingLeaderBoard
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            BettingLeaderBoard.decode = function decode(reader, length) {
                if (!(reader instanceof $Reader))
                    reader = $Reader.create(reader);
                var end = length === undefined ? reader.len : reader.pos + length, message = new $root.aslivecasino.Bac_RoomRoutingUpdate_Inside.BettingLeaderBoard();
                while (reader.pos < end) {
                    var tag = reader.uint32();
                    switch (tag >>> 3) {
                    case 1:
                        message.table_name = reader.string();
                        break;
                    case 2:
                        message.seat_no = reader.sint32();
                        break;
                    case 3:
                        message.user_name = reader.string();
                        break;
                    case 4:
                        message.total_bet_chips = reader.int32();
                        break;
                    default:
                        reader.skipType(tag & 7);
                        break;
                    }
                }
                return message;
            };

            /**
             * Decodes a BettingLeaderBoard message from the specified reader or buffer, length delimited.
             * @function decodeDelimited
             * @memberof aslivecasino.Bac_RoomRoutingUpdate_Inside.BettingLeaderBoard
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @returns {aslivecasino.Bac_RoomRoutingUpdate_Inside.BettingLeaderBoard} BettingLeaderBoard
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            BettingLeaderBoard.decodeDelimited = function decodeDelimited(reader) {
                if (!(reader instanceof $Reader))
                    reader = new $Reader(reader);
                return this.decode(reader, reader.uint32());
            };

            /**
             * Verifies a BettingLeaderBoard message.
             * @function verify
             * @memberof aslivecasino.Bac_RoomRoutingUpdate_Inside.BettingLeaderBoard
             * @static
             * @param {Object.<string,*>} message Plain object to verify
             * @returns {string|null} `null` if valid, otherwise the reason why it is not
             */
            BettingLeaderBoard.verify = function verify(message) {
                if (typeof message !== "object" || message === null)
                    return "object expected";
                if (message.table_name != null && message.hasOwnProperty("table_name"))
                    if (!$util.isString(message.table_name))
                        return "table_name: string expected";
                if (message.seat_no != null && message.hasOwnProperty("seat_no"))
                    if (!$util.isInteger(message.seat_no))
                        return "seat_no: integer expected";
                if (message.user_name != null && message.hasOwnProperty("user_name"))
                    if (!$util.isString(message.user_name))
                        return "user_name: string expected";
                if (message.total_bet_chips != null && message.hasOwnProperty("total_bet_chips"))
                    if (!$util.isInteger(message.total_bet_chips))
                        return "total_bet_chips: integer expected";
                return null;
            };

            /**
             * Creates a BettingLeaderBoard message from a plain object. Also converts values to their respective internal types.
             * @function fromObject
             * @memberof aslivecasino.Bac_RoomRoutingUpdate_Inside.BettingLeaderBoard
             * @static
             * @param {Object.<string,*>} object Plain object
             * @returns {aslivecasino.Bac_RoomRoutingUpdate_Inside.BettingLeaderBoard} BettingLeaderBoard
             */
            BettingLeaderBoard.fromObject = function fromObject(object) {
                if (object instanceof $root.aslivecasino.Bac_RoomRoutingUpdate_Inside.BettingLeaderBoard)
                    return object;
                var message = new $root.aslivecasino.Bac_RoomRoutingUpdate_Inside.BettingLeaderBoard();
                if (object.table_name != null)
                    message.table_name = String(object.table_name);
                if (object.seat_no != null)
                    message.seat_no = object.seat_no | 0;
                if (object.user_name != null)
                    message.user_name = String(object.user_name);
                if (object.total_bet_chips != null)
                    message.total_bet_chips = object.total_bet_chips | 0;
                return message;
            };

            /**
             * Creates a plain object from a BettingLeaderBoard message. Also converts values to other types if specified.
             * @function toObject
             * @memberof aslivecasino.Bac_RoomRoutingUpdate_Inside.BettingLeaderBoard
             * @static
             * @param {aslivecasino.Bac_RoomRoutingUpdate_Inside.BettingLeaderBoard} message BettingLeaderBoard
             * @param {$protobuf.IConversionOptions} [options] Conversion options
             * @returns {Object.<string,*>} Plain object
             */
            BettingLeaderBoard.toObject = function toObject(message, options) {
                if (!options)
                    options = {};
                var object = {};
                if (options.defaults) {
                    object.table_name = "";
                    object.seat_no = 0;
                    object.user_name = "";
                    object.total_bet_chips = 0;
                }
                if (message.table_name != null && message.hasOwnProperty("table_name"))
                    object.table_name = message.table_name;
                if (message.seat_no != null && message.hasOwnProperty("seat_no"))
                    object.seat_no = message.seat_no;
                if (message.user_name != null && message.hasOwnProperty("user_name"))
                    object.user_name = message.user_name;
                if (message.total_bet_chips != null && message.hasOwnProperty("total_bet_chips"))
                    object.total_bet_chips = message.total_bet_chips;
                return object;
            };

            /**
             * Converts this BettingLeaderBoard to JSON.
             * @function toJSON
             * @memberof aslivecasino.Bac_RoomRoutingUpdate_Inside.BettingLeaderBoard
             * @instance
             * @returns {Object.<string,*>} JSON object
             */
            BettingLeaderBoard.prototype.toJSON = function toJSON() {
                return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
            };

            return BettingLeaderBoard;
        })();

        return Bac_RoomRoutingUpdate_Inside;
    })();

    aslivecasino.Bac_EnterRoomInfo = (function() {

        /**
         * Properties of a Bac_EnterRoomInfo.
         * @memberof aslivecasino
         * @interface IBac_EnterRoomInfo
         * @property {string|null} [room_no] Bac_EnterRoomInfo room_no
         * @property {string|null} [table_no] Bac_EnterRoomInfo table_no
         * @property {string|null} [bet_limit] Bac_EnterRoomInfo bet_limit
         */

        /**
         * Constructs a new Bac_EnterRoomInfo.
         * @memberof aslivecasino
         * @classdesc Represents a Bac_EnterRoomInfo.
         * @implements IBac_EnterRoomInfo
         * @constructor
         * @param {aslivecasino.IBac_EnterRoomInfo=} [properties] Properties to set
         */
        function Bac_EnterRoomInfo(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Bac_EnterRoomInfo room_no.
         * @member {string} room_no
         * @memberof aslivecasino.Bac_EnterRoomInfo
         * @instance
         */
        Bac_EnterRoomInfo.prototype.room_no = "";

        /**
         * Bac_EnterRoomInfo table_no.
         * @member {string} table_no
         * @memberof aslivecasino.Bac_EnterRoomInfo
         * @instance
         */
        Bac_EnterRoomInfo.prototype.table_no = "";

        /**
         * Bac_EnterRoomInfo bet_limit.
         * @member {string} bet_limit
         * @memberof aslivecasino.Bac_EnterRoomInfo
         * @instance
         */
        Bac_EnterRoomInfo.prototype.bet_limit = "";

        /**
         * Creates a new Bac_EnterRoomInfo instance using the specified properties.
         * @function create
         * @memberof aslivecasino.Bac_EnterRoomInfo
         * @static
         * @param {aslivecasino.IBac_EnterRoomInfo=} [properties] Properties to set
         * @returns {aslivecasino.Bac_EnterRoomInfo} Bac_EnterRoomInfo instance
         */
        Bac_EnterRoomInfo.create = function create(properties) {
            return new Bac_EnterRoomInfo(properties);
        };

        /**
         * Encodes the specified Bac_EnterRoomInfo message. Does not implicitly {@link aslivecasino.Bac_EnterRoomInfo.verify|verify} messages.
         * @function encode
         * @memberof aslivecasino.Bac_EnterRoomInfo
         * @static
         * @param {aslivecasino.IBac_EnterRoomInfo} message Bac_EnterRoomInfo message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Bac_EnterRoomInfo.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.room_no);
            if (message.table_no != null && message.hasOwnProperty("table_no"))
                writer.uint32(/* id 2, wireType 2 =*/18).string(message.table_no);
            if (message.bet_limit != null && message.hasOwnProperty("bet_limit"))
                writer.uint32(/* id 3, wireType 2 =*/26).string(message.bet_limit);
            return writer;
        };

        /**
         * Encodes the specified Bac_EnterRoomInfo message, length delimited. Does not implicitly {@link aslivecasino.Bac_EnterRoomInfo.verify|verify} messages.
         * @function encodeDelimited
         * @memberof aslivecasino.Bac_EnterRoomInfo
         * @static
         * @param {aslivecasino.IBac_EnterRoomInfo} message Bac_EnterRoomInfo message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Bac_EnterRoomInfo.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Bac_EnterRoomInfo message from the specified reader or buffer.
         * @function decode
         * @memberof aslivecasino.Bac_EnterRoomInfo
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {aslivecasino.Bac_EnterRoomInfo} Bac_EnterRoomInfo
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Bac_EnterRoomInfo.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.aslivecasino.Bac_EnterRoomInfo();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.room_no = reader.string();
                    break;
                case 2:
                    message.table_no = reader.string();
                    break;
                case 3:
                    message.bet_limit = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Bac_EnterRoomInfo message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof aslivecasino.Bac_EnterRoomInfo
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {aslivecasino.Bac_EnterRoomInfo} Bac_EnterRoomInfo
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Bac_EnterRoomInfo.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Bac_EnterRoomInfo message.
         * @function verify
         * @memberof aslivecasino.Bac_EnterRoomInfo
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Bac_EnterRoomInfo.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                if (!$util.isString(message.room_no))
                    return "room_no: string expected";
            if (message.table_no != null && message.hasOwnProperty("table_no"))
                if (!$util.isString(message.table_no))
                    return "table_no: string expected";
            if (message.bet_limit != null && message.hasOwnProperty("bet_limit"))
                if (!$util.isString(message.bet_limit))
                    return "bet_limit: string expected";
            return null;
        };

        /**
         * Creates a Bac_EnterRoomInfo message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof aslivecasino.Bac_EnterRoomInfo
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {aslivecasino.Bac_EnterRoomInfo} Bac_EnterRoomInfo
         */
        Bac_EnterRoomInfo.fromObject = function fromObject(object) {
            if (object instanceof $root.aslivecasino.Bac_EnterRoomInfo)
                return object;
            var message = new $root.aslivecasino.Bac_EnterRoomInfo();
            if (object.room_no != null)
                message.room_no = String(object.room_no);
            if (object.table_no != null)
                message.table_no = String(object.table_no);
            if (object.bet_limit != null)
                message.bet_limit = String(object.bet_limit);
            return message;
        };

        /**
         * Creates a plain object from a Bac_EnterRoomInfo message. Also converts values to other types if specified.
         * @function toObject
         * @memberof aslivecasino.Bac_EnterRoomInfo
         * @static
         * @param {aslivecasino.Bac_EnterRoomInfo} message Bac_EnterRoomInfo
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Bac_EnterRoomInfo.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.room_no = "";
                object.table_no = "";
                object.bet_limit = "";
            }
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                object.room_no = message.room_no;
            if (message.table_no != null && message.hasOwnProperty("table_no"))
                object.table_no = message.table_no;
            if (message.bet_limit != null && message.hasOwnProperty("bet_limit"))
                object.bet_limit = message.bet_limit;
            return object;
        };

        /**
         * Converts this Bac_EnterRoomInfo to JSON.
         * @function toJSON
         * @memberof aslivecasino.Bac_EnterRoomInfo
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Bac_EnterRoomInfo.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return Bac_EnterRoomInfo;
    })();

    aslivecasino.Bac_RoomRoundUpdate = (function() {

        /**
         * Properties of a Bac_RoomRoundUpdate.
         * @memberof aslivecasino
         * @interface IBac_RoomRoundUpdate
         * @property {string|null} [room_no] Bac_RoomRoundUpdate room_no
         * @property {string|null} [round_no] Bac_RoomRoundUpdate round_no
         */

        /**
         * Constructs a new Bac_RoomRoundUpdate.
         * @memberof aslivecasino
         * @classdesc Represents a Bac_RoomRoundUpdate.
         * @implements IBac_RoomRoundUpdate
         * @constructor
         * @param {aslivecasino.IBac_RoomRoundUpdate=} [properties] Properties to set
         */
        function Bac_RoomRoundUpdate(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Bac_RoomRoundUpdate room_no.
         * @member {string} room_no
         * @memberof aslivecasino.Bac_RoomRoundUpdate
         * @instance
         */
        Bac_RoomRoundUpdate.prototype.room_no = "";

        /**
         * Bac_RoomRoundUpdate round_no.
         * @member {string} round_no
         * @memberof aslivecasino.Bac_RoomRoundUpdate
         * @instance
         */
        Bac_RoomRoundUpdate.prototype.round_no = "";

        /**
         * Creates a new Bac_RoomRoundUpdate instance using the specified properties.
         * @function create
         * @memberof aslivecasino.Bac_RoomRoundUpdate
         * @static
         * @param {aslivecasino.IBac_RoomRoundUpdate=} [properties] Properties to set
         * @returns {aslivecasino.Bac_RoomRoundUpdate} Bac_RoomRoundUpdate instance
         */
        Bac_RoomRoundUpdate.create = function create(properties) {
            return new Bac_RoomRoundUpdate(properties);
        };

        /**
         * Encodes the specified Bac_RoomRoundUpdate message. Does not implicitly {@link aslivecasino.Bac_RoomRoundUpdate.verify|verify} messages.
         * @function encode
         * @memberof aslivecasino.Bac_RoomRoundUpdate
         * @static
         * @param {aslivecasino.IBac_RoomRoundUpdate} message Bac_RoomRoundUpdate message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Bac_RoomRoundUpdate.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.room_no);
            if (message.round_no != null && message.hasOwnProperty("round_no"))
                writer.uint32(/* id 2, wireType 2 =*/18).string(message.round_no);
            return writer;
        };

        /**
         * Encodes the specified Bac_RoomRoundUpdate message, length delimited. Does not implicitly {@link aslivecasino.Bac_RoomRoundUpdate.verify|verify} messages.
         * @function encodeDelimited
         * @memberof aslivecasino.Bac_RoomRoundUpdate
         * @static
         * @param {aslivecasino.IBac_RoomRoundUpdate} message Bac_RoomRoundUpdate message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Bac_RoomRoundUpdate.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Bac_RoomRoundUpdate message from the specified reader or buffer.
         * @function decode
         * @memberof aslivecasino.Bac_RoomRoundUpdate
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {aslivecasino.Bac_RoomRoundUpdate} Bac_RoomRoundUpdate
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Bac_RoomRoundUpdate.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.aslivecasino.Bac_RoomRoundUpdate();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.room_no = reader.string();
                    break;
                case 2:
                    message.round_no = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Bac_RoomRoundUpdate message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof aslivecasino.Bac_RoomRoundUpdate
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {aslivecasino.Bac_RoomRoundUpdate} Bac_RoomRoundUpdate
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Bac_RoomRoundUpdate.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Bac_RoomRoundUpdate message.
         * @function verify
         * @memberof aslivecasino.Bac_RoomRoundUpdate
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Bac_RoomRoundUpdate.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                if (!$util.isString(message.room_no))
                    return "room_no: string expected";
            if (message.round_no != null && message.hasOwnProperty("round_no"))
                if (!$util.isString(message.round_no))
                    return "round_no: string expected";
            return null;
        };

        /**
         * Creates a Bac_RoomRoundUpdate message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof aslivecasino.Bac_RoomRoundUpdate
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {aslivecasino.Bac_RoomRoundUpdate} Bac_RoomRoundUpdate
         */
        Bac_RoomRoundUpdate.fromObject = function fromObject(object) {
            if (object instanceof $root.aslivecasino.Bac_RoomRoundUpdate)
                return object;
            var message = new $root.aslivecasino.Bac_RoomRoundUpdate();
            if (object.room_no != null)
                message.room_no = String(object.room_no);
            if (object.round_no != null)
                message.round_no = String(object.round_no);
            return message;
        };

        /**
         * Creates a plain object from a Bac_RoomRoundUpdate message. Also converts values to other types if specified.
         * @function toObject
         * @memberof aslivecasino.Bac_RoomRoundUpdate
         * @static
         * @param {aslivecasino.Bac_RoomRoundUpdate} message Bac_RoomRoundUpdate
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Bac_RoomRoundUpdate.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.room_no = "";
                object.round_no = "";
            }
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                object.room_no = message.room_no;
            if (message.round_no != null && message.hasOwnProperty("round_no"))
                object.round_no = message.round_no;
            return object;
        };

        /**
         * Converts this Bac_RoomRoundUpdate to JSON.
         * @function toJSON
         * @memberof aslivecasino.Bac_RoomRoundUpdate
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Bac_RoomRoundUpdate.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return Bac_RoomRoundUpdate;
    })();

    aslivecasino.Bac_BeadPlateRoad = (function() {

        /**
         * Properties of a Bac_BeadPlateRoad.
         * @memberof aslivecasino
         * @interface IBac_BeadPlateRoad
         * @property {string|null} [room_no] Bac_BeadPlateRoad room_no
         * @property {number|null} [total_beads] Bac_BeadPlateRoad total_beads
         * @property {Array.<aslivecasino.Bac_BeadPlateRoad.IBeadInfo>|null} [bead_list] Bac_BeadPlateRoad bead_list
         */

        /**
         * Constructs a new Bac_BeadPlateRoad.
         * @memberof aslivecasino
         * @classdesc Represents a Bac_BeadPlateRoad.
         * @implements IBac_BeadPlateRoad
         * @constructor
         * @param {aslivecasino.IBac_BeadPlateRoad=} [properties] Properties to set
         */
        function Bac_BeadPlateRoad(properties) {
            this.bead_list = [];
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Bac_BeadPlateRoad room_no.
         * @member {string} room_no
         * @memberof aslivecasino.Bac_BeadPlateRoad
         * @instance
         */
        Bac_BeadPlateRoad.prototype.room_no = "";

        /**
         * Bac_BeadPlateRoad total_beads.
         * @member {number} total_beads
         * @memberof aslivecasino.Bac_BeadPlateRoad
         * @instance
         */
        Bac_BeadPlateRoad.prototype.total_beads = 0;

        /**
         * Bac_BeadPlateRoad bead_list.
         * @member {Array.<aslivecasino.Bac_BeadPlateRoad.IBeadInfo>} bead_list
         * @memberof aslivecasino.Bac_BeadPlateRoad
         * @instance
         */
        Bac_BeadPlateRoad.prototype.bead_list = $util.emptyArray;

        /**
         * Creates a new Bac_BeadPlateRoad instance using the specified properties.
         * @function create
         * @memberof aslivecasino.Bac_BeadPlateRoad
         * @static
         * @param {aslivecasino.IBac_BeadPlateRoad=} [properties] Properties to set
         * @returns {aslivecasino.Bac_BeadPlateRoad} Bac_BeadPlateRoad instance
         */
        Bac_BeadPlateRoad.create = function create(properties) {
            return new Bac_BeadPlateRoad(properties);
        };

        /**
         * Encodes the specified Bac_BeadPlateRoad message. Does not implicitly {@link aslivecasino.Bac_BeadPlateRoad.verify|verify} messages.
         * @function encode
         * @memberof aslivecasino.Bac_BeadPlateRoad
         * @static
         * @param {aslivecasino.IBac_BeadPlateRoad} message Bac_BeadPlateRoad message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Bac_BeadPlateRoad.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.room_no);
            if (message.total_beads != null && message.hasOwnProperty("total_beads"))
                writer.uint32(/* id 2, wireType 0 =*/16).int32(message.total_beads);
            if (message.bead_list != null && message.bead_list.length)
                for (var i = 0; i < message.bead_list.length; ++i)
                    $root.aslivecasino.Bac_BeadPlateRoad.BeadInfo.encode(message.bead_list[i], writer.uint32(/* id 3, wireType 2 =*/26).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified Bac_BeadPlateRoad message, length delimited. Does not implicitly {@link aslivecasino.Bac_BeadPlateRoad.verify|verify} messages.
         * @function encodeDelimited
         * @memberof aslivecasino.Bac_BeadPlateRoad
         * @static
         * @param {aslivecasino.IBac_BeadPlateRoad} message Bac_BeadPlateRoad message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Bac_BeadPlateRoad.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Bac_BeadPlateRoad message from the specified reader or buffer.
         * @function decode
         * @memberof aslivecasino.Bac_BeadPlateRoad
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {aslivecasino.Bac_BeadPlateRoad} Bac_BeadPlateRoad
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Bac_BeadPlateRoad.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.aslivecasino.Bac_BeadPlateRoad();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.room_no = reader.string();
                    break;
                case 2:
                    message.total_beads = reader.int32();
                    break;
                case 3:
                    if (!(message.bead_list && message.bead_list.length))
                        message.bead_list = [];
                    message.bead_list.push($root.aslivecasino.Bac_BeadPlateRoad.BeadInfo.decode(reader, reader.uint32()));
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Bac_BeadPlateRoad message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof aslivecasino.Bac_BeadPlateRoad
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {aslivecasino.Bac_BeadPlateRoad} Bac_BeadPlateRoad
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Bac_BeadPlateRoad.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Bac_BeadPlateRoad message.
         * @function verify
         * @memberof aslivecasino.Bac_BeadPlateRoad
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Bac_BeadPlateRoad.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                if (!$util.isString(message.room_no))
                    return "room_no: string expected";
            if (message.total_beads != null && message.hasOwnProperty("total_beads"))
                if (!$util.isInteger(message.total_beads))
                    return "total_beads: integer expected";
            if (message.bead_list != null && message.hasOwnProperty("bead_list")) {
                if (!Array.isArray(message.bead_list))
                    return "bead_list: array expected";
                for (var i = 0; i < message.bead_list.length; ++i) {
                    var error = $root.aslivecasino.Bac_BeadPlateRoad.BeadInfo.verify(message.bead_list[i]);
                    if (error)
                        return "bead_list." + error;
                }
            }
            return null;
        };

        /**
         * Creates a Bac_BeadPlateRoad message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof aslivecasino.Bac_BeadPlateRoad
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {aslivecasino.Bac_BeadPlateRoad} Bac_BeadPlateRoad
         */
        Bac_BeadPlateRoad.fromObject = function fromObject(object) {
            if (object instanceof $root.aslivecasino.Bac_BeadPlateRoad)
                return object;
            var message = new $root.aslivecasino.Bac_BeadPlateRoad();
            if (object.room_no != null)
                message.room_no = String(object.room_no);
            if (object.total_beads != null)
                message.total_beads = object.total_beads | 0;
            if (object.bead_list) {
                if (!Array.isArray(object.bead_list))
                    throw TypeError(".aslivecasino.Bac_BeadPlateRoad.bead_list: array expected");
                message.bead_list = [];
                for (var i = 0; i < object.bead_list.length; ++i) {
                    if (typeof object.bead_list[i] !== "object")
                        throw TypeError(".aslivecasino.Bac_BeadPlateRoad.bead_list: object expected");
                    message.bead_list[i] = $root.aslivecasino.Bac_BeadPlateRoad.BeadInfo.fromObject(object.bead_list[i]);
                }
            }
            return message;
        };

        /**
         * Creates a plain object from a Bac_BeadPlateRoad message. Also converts values to other types if specified.
         * @function toObject
         * @memberof aslivecasino.Bac_BeadPlateRoad
         * @static
         * @param {aslivecasino.Bac_BeadPlateRoad} message Bac_BeadPlateRoad
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Bac_BeadPlateRoad.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.arrays || options.defaults)
                object.bead_list = [];
            if (options.defaults) {
                object.room_no = "";
                object.total_beads = 0;
            }
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                object.room_no = message.room_no;
            if (message.total_beads != null && message.hasOwnProperty("total_beads"))
                object.total_beads = message.total_beads;
            if (message.bead_list && message.bead_list.length) {
                object.bead_list = [];
                for (var j = 0; j < message.bead_list.length; ++j)
                    object.bead_list[j] = $root.aslivecasino.Bac_BeadPlateRoad.BeadInfo.toObject(message.bead_list[j], options);
            }
            return object;
        };

        /**
         * Converts this Bac_BeadPlateRoad to JSON.
         * @function toJSON
         * @memberof aslivecasino.Bac_BeadPlateRoad
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Bac_BeadPlateRoad.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        Bac_BeadPlateRoad.BeadInfo = (function() {

            /**
             * Properties of a BeadInfo.
             * @memberof aslivecasino.Bac_BeadPlateRoad
             * @interface IBeadInfo
             * @property {number|null} [bead_no] BeadInfo bead_no
             * @property {number|null} [winner_point] BeadInfo winner_point
             * @property {Array.<aslivecasino.Bac_EnumDivType>|null} [div_type] BeadInfo div_type
             */

            /**
             * Constructs a new BeadInfo.
             * @memberof aslivecasino.Bac_BeadPlateRoad
             * @classdesc Represents a BeadInfo.
             * @implements IBeadInfo
             * @constructor
             * @param {aslivecasino.Bac_BeadPlateRoad.IBeadInfo=} [properties] Properties to set
             */
            function BeadInfo(properties) {
                this.div_type = [];
                if (properties)
                    for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                        if (properties[keys[i]] != null)
                            this[keys[i]] = properties[keys[i]];
            }

            /**
             * BeadInfo bead_no.
             * @member {number} bead_no
             * @memberof aslivecasino.Bac_BeadPlateRoad.BeadInfo
             * @instance
             */
            BeadInfo.prototype.bead_no = 0;

            /**
             * BeadInfo winner_point.
             * @member {number} winner_point
             * @memberof aslivecasino.Bac_BeadPlateRoad.BeadInfo
             * @instance
             */
            BeadInfo.prototype.winner_point = 0;

            /**
             * BeadInfo div_type.
             * @member {Array.<aslivecasino.Bac_EnumDivType>} div_type
             * @memberof aslivecasino.Bac_BeadPlateRoad.BeadInfo
             * @instance
             */
            BeadInfo.prototype.div_type = $util.emptyArray;

            /**
             * Creates a new BeadInfo instance using the specified properties.
             * @function create
             * @memberof aslivecasino.Bac_BeadPlateRoad.BeadInfo
             * @static
             * @param {aslivecasino.Bac_BeadPlateRoad.IBeadInfo=} [properties] Properties to set
             * @returns {aslivecasino.Bac_BeadPlateRoad.BeadInfo} BeadInfo instance
             */
            BeadInfo.create = function create(properties) {
                return new BeadInfo(properties);
            };

            /**
             * Encodes the specified BeadInfo message. Does not implicitly {@link aslivecasino.Bac_BeadPlateRoad.BeadInfo.verify|verify} messages.
             * @function encode
             * @memberof aslivecasino.Bac_BeadPlateRoad.BeadInfo
             * @static
             * @param {aslivecasino.Bac_BeadPlateRoad.IBeadInfo} message BeadInfo message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            BeadInfo.encode = function encode(message, writer) {
                if (!writer)
                    writer = $Writer.create();
                if (message.bead_no != null && message.hasOwnProperty("bead_no"))
                    writer.uint32(/* id 1, wireType 0 =*/8).int32(message.bead_no);
                if (message.winner_point != null && message.hasOwnProperty("winner_point"))
                    writer.uint32(/* id 2, wireType 0 =*/16).int32(message.winner_point);
                if (message.div_type != null && message.div_type.length) {
                    writer.uint32(/* id 3, wireType 2 =*/26).fork();
                    for (var i = 0; i < message.div_type.length; ++i)
                        writer.int32(message.div_type[i]);
                    writer.ldelim();
                }
                return writer;
            };

            /**
             * Encodes the specified BeadInfo message, length delimited. Does not implicitly {@link aslivecasino.Bac_BeadPlateRoad.BeadInfo.verify|verify} messages.
             * @function encodeDelimited
             * @memberof aslivecasino.Bac_BeadPlateRoad.BeadInfo
             * @static
             * @param {aslivecasino.Bac_BeadPlateRoad.IBeadInfo} message BeadInfo message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            BeadInfo.encodeDelimited = function encodeDelimited(message, writer) {
                return this.encode(message, writer).ldelim();
            };

            /**
             * Decodes a BeadInfo message from the specified reader or buffer.
             * @function decode
             * @memberof aslivecasino.Bac_BeadPlateRoad.BeadInfo
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @param {number} [length] Message length if known beforehand
             * @returns {aslivecasino.Bac_BeadPlateRoad.BeadInfo} BeadInfo
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            BeadInfo.decode = function decode(reader, length) {
                if (!(reader instanceof $Reader))
                    reader = $Reader.create(reader);
                var end = length === undefined ? reader.len : reader.pos + length, message = new $root.aslivecasino.Bac_BeadPlateRoad.BeadInfo();
                while (reader.pos < end) {
                    var tag = reader.uint32();
                    switch (tag >>> 3) {
                    case 1:
                        message.bead_no = reader.int32();
                        break;
                    case 2:
                        message.winner_point = reader.int32();
                        break;
                    case 3:
                        if (!(message.div_type && message.div_type.length))
                            message.div_type = [];
                        if ((tag & 7) === 2) {
                            var end2 = reader.uint32() + reader.pos;
                            while (reader.pos < end2)
                                message.div_type.push(reader.int32());
                        } else
                            message.div_type.push(reader.int32());
                        break;
                    default:
                        reader.skipType(tag & 7);
                        break;
                    }
                }
                return message;
            };

            /**
             * Decodes a BeadInfo message from the specified reader or buffer, length delimited.
             * @function decodeDelimited
             * @memberof aslivecasino.Bac_BeadPlateRoad.BeadInfo
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @returns {aslivecasino.Bac_BeadPlateRoad.BeadInfo} BeadInfo
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            BeadInfo.decodeDelimited = function decodeDelimited(reader) {
                if (!(reader instanceof $Reader))
                    reader = new $Reader(reader);
                return this.decode(reader, reader.uint32());
            };

            /**
             * Verifies a BeadInfo message.
             * @function verify
             * @memberof aslivecasino.Bac_BeadPlateRoad.BeadInfo
             * @static
             * @param {Object.<string,*>} message Plain object to verify
             * @returns {string|null} `null` if valid, otherwise the reason why it is not
             */
            BeadInfo.verify = function verify(message) {
                if (typeof message !== "object" || message === null)
                    return "object expected";
                if (message.bead_no != null && message.hasOwnProperty("bead_no"))
                    if (!$util.isInteger(message.bead_no))
                        return "bead_no: integer expected";
                if (message.winner_point != null && message.hasOwnProperty("winner_point"))
                    if (!$util.isInteger(message.winner_point))
                        return "winner_point: integer expected";
                if (message.div_type != null && message.hasOwnProperty("div_type")) {
                    if (!Array.isArray(message.div_type))
                        return "div_type: array expected";
                    for (var i = 0; i < message.div_type.length; ++i)
                        switch (message.div_type[i]) {
                        default:
                            return "div_type: enum value[] expected";
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                            break;
                        }
                }
                return null;
            };

            /**
             * Creates a BeadInfo message from a plain object. Also converts values to their respective internal types.
             * @function fromObject
             * @memberof aslivecasino.Bac_BeadPlateRoad.BeadInfo
             * @static
             * @param {Object.<string,*>} object Plain object
             * @returns {aslivecasino.Bac_BeadPlateRoad.BeadInfo} BeadInfo
             */
            BeadInfo.fromObject = function fromObject(object) {
                if (object instanceof $root.aslivecasino.Bac_BeadPlateRoad.BeadInfo)
                    return object;
                var message = new $root.aslivecasino.Bac_BeadPlateRoad.BeadInfo();
                if (object.bead_no != null)
                    message.bead_no = object.bead_no | 0;
                if (object.winner_point != null)
                    message.winner_point = object.winner_point | 0;
                if (object.div_type) {
                    if (!Array.isArray(object.div_type))
                        throw TypeError(".aslivecasino.Bac_BeadPlateRoad.BeadInfo.div_type: array expected");
                    message.div_type = [];
                    for (var i = 0; i < object.div_type.length; ++i)
                        switch (object.div_type[i]) {
                        default:
                        case "PLAYER":
                        case 0:
                            message.div_type[i] = 0;
                            break;
                        case "BANKER":
                        case 1:
                            message.div_type[i] = 1;
                            break;
                        case "TIE":
                        case 2:
                            message.div_type[i] = 2;
                            break;
                        case "PPAIR":
                        case 3:
                            message.div_type[i] = 3;
                            break;
                        case "BPAIR":
                        case 4:
                            message.div_type[i] = 4;
                            break;
                        }
                }
                return message;
            };

            /**
             * Creates a plain object from a BeadInfo message. Also converts values to other types if specified.
             * @function toObject
             * @memberof aslivecasino.Bac_BeadPlateRoad.BeadInfo
             * @static
             * @param {aslivecasino.Bac_BeadPlateRoad.BeadInfo} message BeadInfo
             * @param {$protobuf.IConversionOptions} [options] Conversion options
             * @returns {Object.<string,*>} Plain object
             */
            BeadInfo.toObject = function toObject(message, options) {
                if (!options)
                    options = {};
                var object = {};
                if (options.arrays || options.defaults)
                    object.div_type = [];
                if (options.defaults) {
                    object.bead_no = 0;
                    object.winner_point = 0;
                }
                if (message.bead_no != null && message.hasOwnProperty("bead_no"))
                    object.bead_no = message.bead_no;
                if (message.winner_point != null && message.hasOwnProperty("winner_point"))
                    object.winner_point = message.winner_point;
                if (message.div_type && message.div_type.length) {
                    object.div_type = [];
                    for (var j = 0; j < message.div_type.length; ++j)
                        object.div_type[j] = options.enums === String ? $root.aslivecasino.Bac_EnumDivType[message.div_type[j]] : message.div_type[j];
                }
                return object;
            };

            /**
             * Converts this BeadInfo to JSON.
             * @function toJSON
             * @memberof aslivecasino.Bac_BeadPlateRoad.BeadInfo
             * @instance
             * @returns {Object.<string,*>} JSON object
             */
            BeadInfo.prototype.toJSON = function toJSON() {
                return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
            };

            return BeadInfo;
        })();

        return Bac_BeadPlateRoad;
    })();

    aslivecasino.Bac_UserEnterTable = (function() {

        /**
         * Properties of a Bac_UserEnterTable.
         * @memberof aslivecasino
         * @interface IBac_UserEnterTable
         * @property {string|null} [room_no] Bac_UserEnterTable room_no
         * @property {number|null} [table_no] Bac_UserEnterTable table_no
         * @property {number|null} [seat_no] Bac_UserEnterTable seat_no
         * @property {number|Long|null} [user_id] Bac_UserEnterTable user_id
         * @property {string|null} [user_name] Bac_UserEnterTable user_name
         * @property {number|null} [balance] Bac_UserEnterTable balance
         */

        /**
         * Constructs a new Bac_UserEnterTable.
         * @memberof aslivecasino
         * @classdesc Represents a Bac_UserEnterTable.
         * @implements IBac_UserEnterTable
         * @constructor
         * @param {aslivecasino.IBac_UserEnterTable=} [properties] Properties to set
         */
        function Bac_UserEnterTable(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Bac_UserEnterTable room_no.
         * @member {string} room_no
         * @memberof aslivecasino.Bac_UserEnterTable
         * @instance
         */
        Bac_UserEnterTable.prototype.room_no = "";

        /**
         * Bac_UserEnterTable table_no.
         * @member {number} table_no
         * @memberof aslivecasino.Bac_UserEnterTable
         * @instance
         */
        Bac_UserEnterTable.prototype.table_no = 0;

        /**
         * Bac_UserEnterTable seat_no.
         * @member {number} seat_no
         * @memberof aslivecasino.Bac_UserEnterTable
         * @instance
         */
        Bac_UserEnterTable.prototype.seat_no = 0;

        /**
         * Bac_UserEnterTable user_id.
         * @member {number|Long} user_id
         * @memberof aslivecasino.Bac_UserEnterTable
         * @instance
         */
        Bac_UserEnterTable.prototype.user_id = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

        /**
         * Bac_UserEnterTable user_name.
         * @member {string} user_name
         * @memberof aslivecasino.Bac_UserEnterTable
         * @instance
         */
        Bac_UserEnterTable.prototype.user_name = "";

        /**
         * Bac_UserEnterTable balance.
         * @member {number} balance
         * @memberof aslivecasino.Bac_UserEnterTable
         * @instance
         */
        Bac_UserEnterTable.prototype.balance = 0;

        /**
         * Creates a new Bac_UserEnterTable instance using the specified properties.
         * @function create
         * @memberof aslivecasino.Bac_UserEnterTable
         * @static
         * @param {aslivecasino.IBac_UserEnterTable=} [properties] Properties to set
         * @returns {aslivecasino.Bac_UserEnterTable} Bac_UserEnterTable instance
         */
        Bac_UserEnterTable.create = function create(properties) {
            return new Bac_UserEnterTable(properties);
        };

        /**
         * Encodes the specified Bac_UserEnterTable message. Does not implicitly {@link aslivecasino.Bac_UserEnterTable.verify|verify} messages.
         * @function encode
         * @memberof aslivecasino.Bac_UserEnterTable
         * @static
         * @param {aslivecasino.IBac_UserEnterTable} message Bac_UserEnterTable message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Bac_UserEnterTable.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.room_no);
            if (message.table_no != null && message.hasOwnProperty("table_no"))
                writer.uint32(/* id 2, wireType 0 =*/16).sint32(message.table_no);
            if (message.seat_no != null && message.hasOwnProperty("seat_no"))
                writer.uint32(/* id 3, wireType 0 =*/24).sint32(message.seat_no);
            if (message.user_id != null && message.hasOwnProperty("user_id"))
                writer.uint32(/* id 4, wireType 0 =*/32).int64(message.user_id);
            if (message.user_name != null && message.hasOwnProperty("user_name"))
                writer.uint32(/* id 5, wireType 2 =*/42).string(message.user_name);
            if (message.balance != null && message.hasOwnProperty("balance"))
                writer.uint32(/* id 6, wireType 1 =*/49).double(message.balance);
            return writer;
        };

        /**
         * Encodes the specified Bac_UserEnterTable message, length delimited. Does not implicitly {@link aslivecasino.Bac_UserEnterTable.verify|verify} messages.
         * @function encodeDelimited
         * @memberof aslivecasino.Bac_UserEnterTable
         * @static
         * @param {aslivecasino.IBac_UserEnterTable} message Bac_UserEnterTable message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Bac_UserEnterTable.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Bac_UserEnterTable message from the specified reader or buffer.
         * @function decode
         * @memberof aslivecasino.Bac_UserEnterTable
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {aslivecasino.Bac_UserEnterTable} Bac_UserEnterTable
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Bac_UserEnterTable.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.aslivecasino.Bac_UserEnterTable();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.room_no = reader.string();
                    break;
                case 2:
                    message.table_no = reader.sint32();
                    break;
                case 3:
                    message.seat_no = reader.sint32();
                    break;
                case 4:
                    message.user_id = reader.int64();
                    break;
                case 5:
                    message.user_name = reader.string();
                    break;
                case 6:
                    message.balance = reader.double();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Bac_UserEnterTable message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof aslivecasino.Bac_UserEnterTable
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {aslivecasino.Bac_UserEnterTable} Bac_UserEnterTable
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Bac_UserEnterTable.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Bac_UserEnterTable message.
         * @function verify
         * @memberof aslivecasino.Bac_UserEnterTable
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Bac_UserEnterTable.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                if (!$util.isString(message.room_no))
                    return "room_no: string expected";
            if (message.table_no != null && message.hasOwnProperty("table_no"))
                if (!$util.isInteger(message.table_no))
                    return "table_no: integer expected";
            if (message.seat_no != null && message.hasOwnProperty("seat_no"))
                if (!$util.isInteger(message.seat_no))
                    return "seat_no: integer expected";
            if (message.user_id != null && message.hasOwnProperty("user_id"))
                if (!$util.isInteger(message.user_id) && !(message.user_id && $util.isInteger(message.user_id.low) && $util.isInteger(message.user_id.high)))
                    return "user_id: integer|Long expected";
            if (message.user_name != null && message.hasOwnProperty("user_name"))
                if (!$util.isString(message.user_name))
                    return "user_name: string expected";
            if (message.balance != null && message.hasOwnProperty("balance"))
                if (typeof message.balance !== "number")
                    return "balance: number expected";
            return null;
        };

        /**
         * Creates a Bac_UserEnterTable message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof aslivecasino.Bac_UserEnterTable
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {aslivecasino.Bac_UserEnterTable} Bac_UserEnterTable
         */
        Bac_UserEnterTable.fromObject = function fromObject(object) {
            if (object instanceof $root.aslivecasino.Bac_UserEnterTable)
                return object;
            var message = new $root.aslivecasino.Bac_UserEnterTable();
            if (object.room_no != null)
                message.room_no = String(object.room_no);
            if (object.table_no != null)
                message.table_no = object.table_no | 0;
            if (object.seat_no != null)
                message.seat_no = object.seat_no | 0;
            if (object.user_id != null)
                if ($util.Long)
                    (message.user_id = $util.Long.fromValue(object.user_id)).unsigned = false;
                else if (typeof object.user_id === "string")
                    message.user_id = parseInt(object.user_id, 10);
                else if (typeof object.user_id === "number")
                    message.user_id = object.user_id;
                else if (typeof object.user_id === "object")
                    message.user_id = new $util.LongBits(object.user_id.low >>> 0, object.user_id.high >>> 0).toNumber();
            if (object.user_name != null)
                message.user_name = String(object.user_name);
            if (object.balance != null)
                message.balance = Number(object.balance);
            return message;
        };

        /**
         * Creates a plain object from a Bac_UserEnterTable message. Also converts values to other types if specified.
         * @function toObject
         * @memberof aslivecasino.Bac_UserEnterTable
         * @static
         * @param {aslivecasino.Bac_UserEnterTable} message Bac_UserEnterTable
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Bac_UserEnterTable.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.room_no = "";
                object.table_no = 0;
                object.seat_no = 0;
                if ($util.Long) {
                    var long = new $util.Long(0, 0, false);
                    object.user_id = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.user_id = options.longs === String ? "0" : 0;
                object.user_name = "";
                object.balance = 0;
            }
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                object.room_no = message.room_no;
            if (message.table_no != null && message.hasOwnProperty("table_no"))
                object.table_no = message.table_no;
            if (message.seat_no != null && message.hasOwnProperty("seat_no"))
                object.seat_no = message.seat_no;
            if (message.user_id != null && message.hasOwnProperty("user_id"))
                if (typeof message.user_id === "number")
                    object.user_id = options.longs === String ? String(message.user_id) : message.user_id;
                else
                    object.user_id = options.longs === String ? $util.Long.prototype.toString.call(message.user_id) : options.longs === Number ? new $util.LongBits(message.user_id.low >>> 0, message.user_id.high >>> 0).toNumber() : message.user_id;
            if (message.user_name != null && message.hasOwnProperty("user_name"))
                object.user_name = message.user_name;
            if (message.balance != null && message.hasOwnProperty("balance"))
                object.balance = options.json && !isFinite(message.balance) ? String(message.balance) : message.balance;
            return object;
        };

        /**
         * Converts this Bac_UserEnterTable to JSON.
         * @function toJSON
         * @memberof aslivecasino.Bac_UserEnterTable
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Bac_UserEnterTable.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return Bac_UserEnterTable;
    })();

    aslivecasino.Bac_UserLeaveTable = (function() {

        /**
         * Properties of a Bac_UserLeaveTable.
         * @memberof aslivecasino
         * @interface IBac_UserLeaveTable
         * @property {string|null} [room_no] Bac_UserLeaveTable room_no
         * @property {number|null} [table_no] Bac_UserLeaveTable table_no
         * @property {number|null} [seat_no] Bac_UserLeaveTable seat_no
         * @property {number|Long|null} [user_id] Bac_UserLeaveTable user_id
         */

        /**
         * Constructs a new Bac_UserLeaveTable.
         * @memberof aslivecasino
         * @classdesc Represents a Bac_UserLeaveTable.
         * @implements IBac_UserLeaveTable
         * @constructor
         * @param {aslivecasino.IBac_UserLeaveTable=} [properties] Properties to set
         */
        function Bac_UserLeaveTable(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Bac_UserLeaveTable room_no.
         * @member {string} room_no
         * @memberof aslivecasino.Bac_UserLeaveTable
         * @instance
         */
        Bac_UserLeaveTable.prototype.room_no = "";

        /**
         * Bac_UserLeaveTable table_no.
         * @member {number} table_no
         * @memberof aslivecasino.Bac_UserLeaveTable
         * @instance
         */
        Bac_UserLeaveTable.prototype.table_no = 0;

        /**
         * Bac_UserLeaveTable seat_no.
         * @member {number} seat_no
         * @memberof aslivecasino.Bac_UserLeaveTable
         * @instance
         */
        Bac_UserLeaveTable.prototype.seat_no = 0;

        /**
         * Bac_UserLeaveTable user_id.
         * @member {number|Long} user_id
         * @memberof aslivecasino.Bac_UserLeaveTable
         * @instance
         */
        Bac_UserLeaveTable.prototype.user_id = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

        /**
         * Creates a new Bac_UserLeaveTable instance using the specified properties.
         * @function create
         * @memberof aslivecasino.Bac_UserLeaveTable
         * @static
         * @param {aslivecasino.IBac_UserLeaveTable=} [properties] Properties to set
         * @returns {aslivecasino.Bac_UserLeaveTable} Bac_UserLeaveTable instance
         */
        Bac_UserLeaveTable.create = function create(properties) {
            return new Bac_UserLeaveTable(properties);
        };

        /**
         * Encodes the specified Bac_UserLeaveTable message. Does not implicitly {@link aslivecasino.Bac_UserLeaveTable.verify|verify} messages.
         * @function encode
         * @memberof aslivecasino.Bac_UserLeaveTable
         * @static
         * @param {aslivecasino.IBac_UserLeaveTable} message Bac_UserLeaveTable message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Bac_UserLeaveTable.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.room_no);
            if (message.table_no != null && message.hasOwnProperty("table_no"))
                writer.uint32(/* id 2, wireType 0 =*/16).sint32(message.table_no);
            if (message.seat_no != null && message.hasOwnProperty("seat_no"))
                writer.uint32(/* id 3, wireType 0 =*/24).sint32(message.seat_no);
            if (message.user_id != null && message.hasOwnProperty("user_id"))
                writer.uint32(/* id 4, wireType 0 =*/32).int64(message.user_id);
            return writer;
        };

        /**
         * Encodes the specified Bac_UserLeaveTable message, length delimited. Does not implicitly {@link aslivecasino.Bac_UserLeaveTable.verify|verify} messages.
         * @function encodeDelimited
         * @memberof aslivecasino.Bac_UserLeaveTable
         * @static
         * @param {aslivecasino.IBac_UserLeaveTable} message Bac_UserLeaveTable message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Bac_UserLeaveTable.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Bac_UserLeaveTable message from the specified reader or buffer.
         * @function decode
         * @memberof aslivecasino.Bac_UserLeaveTable
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {aslivecasino.Bac_UserLeaveTable} Bac_UserLeaveTable
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Bac_UserLeaveTable.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.aslivecasino.Bac_UserLeaveTable();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.room_no = reader.string();
                    break;
                case 2:
                    message.table_no = reader.sint32();
                    break;
                case 3:
                    message.seat_no = reader.sint32();
                    break;
                case 4:
                    message.user_id = reader.int64();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Bac_UserLeaveTable message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof aslivecasino.Bac_UserLeaveTable
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {aslivecasino.Bac_UserLeaveTable} Bac_UserLeaveTable
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Bac_UserLeaveTable.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Bac_UserLeaveTable message.
         * @function verify
         * @memberof aslivecasino.Bac_UserLeaveTable
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Bac_UserLeaveTable.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                if (!$util.isString(message.room_no))
                    return "room_no: string expected";
            if (message.table_no != null && message.hasOwnProperty("table_no"))
                if (!$util.isInteger(message.table_no))
                    return "table_no: integer expected";
            if (message.seat_no != null && message.hasOwnProperty("seat_no"))
                if (!$util.isInteger(message.seat_no))
                    return "seat_no: integer expected";
            if (message.user_id != null && message.hasOwnProperty("user_id"))
                if (!$util.isInteger(message.user_id) && !(message.user_id && $util.isInteger(message.user_id.low) && $util.isInteger(message.user_id.high)))
                    return "user_id: integer|Long expected";
            return null;
        };

        /**
         * Creates a Bac_UserLeaveTable message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof aslivecasino.Bac_UserLeaveTable
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {aslivecasino.Bac_UserLeaveTable} Bac_UserLeaveTable
         */
        Bac_UserLeaveTable.fromObject = function fromObject(object) {
            if (object instanceof $root.aslivecasino.Bac_UserLeaveTable)
                return object;
            var message = new $root.aslivecasino.Bac_UserLeaveTable();
            if (object.room_no != null)
                message.room_no = String(object.room_no);
            if (object.table_no != null)
                message.table_no = object.table_no | 0;
            if (object.seat_no != null)
                message.seat_no = object.seat_no | 0;
            if (object.user_id != null)
                if ($util.Long)
                    (message.user_id = $util.Long.fromValue(object.user_id)).unsigned = false;
                else if (typeof object.user_id === "string")
                    message.user_id = parseInt(object.user_id, 10);
                else if (typeof object.user_id === "number")
                    message.user_id = object.user_id;
                else if (typeof object.user_id === "object")
                    message.user_id = new $util.LongBits(object.user_id.low >>> 0, object.user_id.high >>> 0).toNumber();
            return message;
        };

        /**
         * Creates a plain object from a Bac_UserLeaveTable message. Also converts values to other types if specified.
         * @function toObject
         * @memberof aslivecasino.Bac_UserLeaveTable
         * @static
         * @param {aslivecasino.Bac_UserLeaveTable} message Bac_UserLeaveTable
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Bac_UserLeaveTable.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.room_no = "";
                object.table_no = 0;
                object.seat_no = 0;
                if ($util.Long) {
                    var long = new $util.Long(0, 0, false);
                    object.user_id = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.user_id = options.longs === String ? "0" : 0;
            }
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                object.room_no = message.room_no;
            if (message.table_no != null && message.hasOwnProperty("table_no"))
                object.table_no = message.table_no;
            if (message.seat_no != null && message.hasOwnProperty("seat_no"))
                object.seat_no = message.seat_no;
            if (message.user_id != null && message.hasOwnProperty("user_id"))
                if (typeof message.user_id === "number")
                    object.user_id = options.longs === String ? String(message.user_id) : message.user_id;
                else
                    object.user_id = options.longs === String ? $util.Long.prototype.toString.call(message.user_id) : options.longs === Number ? new $util.LongBits(message.user_id.low >>> 0, message.user_id.high >>> 0).toNumber() : message.user_id;
            return object;
        };

        /**
         * Converts this Bac_UserLeaveTable to JSON.
         * @function toJSON
         * @memberof aslivecasino.Bac_UserLeaveTable
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Bac_UserLeaveTable.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return Bac_UserLeaveTable;
    })();

    aslivecasino.Bac_BettingInfo = (function() {

        /**
         * Properties of a Bac_BettingInfo.
         * @memberof aslivecasino
         * @interface IBac_BettingInfo
         * @property {string|null} [room_no] Bac_BettingInfo room_no
         * @property {number|null} [table_no] Bac_BettingInfo table_no
         * @property {Array.<aslivecasino.Bac_BettingInfo.IBetData>|null} [bet_data_list] Bac_BettingInfo bet_data_list
         */

        /**
         * Constructs a new Bac_BettingInfo.
         * @memberof aslivecasino
         * @classdesc Represents a Bac_BettingInfo.
         * @implements IBac_BettingInfo
         * @constructor
         * @param {aslivecasino.IBac_BettingInfo=} [properties] Properties to set
         */
        function Bac_BettingInfo(properties) {
            this.bet_data_list = [];
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Bac_BettingInfo room_no.
         * @member {string} room_no
         * @memberof aslivecasino.Bac_BettingInfo
         * @instance
         */
        Bac_BettingInfo.prototype.room_no = "";

        /**
         * Bac_BettingInfo table_no.
         * @member {number} table_no
         * @memberof aslivecasino.Bac_BettingInfo
         * @instance
         */
        Bac_BettingInfo.prototype.table_no = 0;

        /**
         * Bac_BettingInfo bet_data_list.
         * @member {Array.<aslivecasino.Bac_BettingInfo.IBetData>} bet_data_list
         * @memberof aslivecasino.Bac_BettingInfo
         * @instance
         */
        Bac_BettingInfo.prototype.bet_data_list = $util.emptyArray;

        /**
         * Creates a new Bac_BettingInfo instance using the specified properties.
         * @function create
         * @memberof aslivecasino.Bac_BettingInfo
         * @static
         * @param {aslivecasino.IBac_BettingInfo=} [properties] Properties to set
         * @returns {aslivecasino.Bac_BettingInfo} Bac_BettingInfo instance
         */
        Bac_BettingInfo.create = function create(properties) {
            return new Bac_BettingInfo(properties);
        };

        /**
         * Encodes the specified Bac_BettingInfo message. Does not implicitly {@link aslivecasino.Bac_BettingInfo.verify|verify} messages.
         * @function encode
         * @memberof aslivecasino.Bac_BettingInfo
         * @static
         * @param {aslivecasino.IBac_BettingInfo} message Bac_BettingInfo message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Bac_BettingInfo.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.room_no);
            if (message.table_no != null && message.hasOwnProperty("table_no"))
                writer.uint32(/* id 2, wireType 0 =*/16).sint32(message.table_no);
            if (message.bet_data_list != null && message.bet_data_list.length)
                for (var i = 0; i < message.bet_data_list.length; ++i)
                    $root.aslivecasino.Bac_BettingInfo.BetData.encode(message.bet_data_list[i], writer.uint32(/* id 3, wireType 2 =*/26).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified Bac_BettingInfo message, length delimited. Does not implicitly {@link aslivecasino.Bac_BettingInfo.verify|verify} messages.
         * @function encodeDelimited
         * @memberof aslivecasino.Bac_BettingInfo
         * @static
         * @param {aslivecasino.IBac_BettingInfo} message Bac_BettingInfo message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Bac_BettingInfo.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Bac_BettingInfo message from the specified reader or buffer.
         * @function decode
         * @memberof aslivecasino.Bac_BettingInfo
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {aslivecasino.Bac_BettingInfo} Bac_BettingInfo
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Bac_BettingInfo.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.aslivecasino.Bac_BettingInfo();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.room_no = reader.string();
                    break;
                case 2:
                    message.table_no = reader.sint32();
                    break;
                case 3:
                    if (!(message.bet_data_list && message.bet_data_list.length))
                        message.bet_data_list = [];
                    message.bet_data_list.push($root.aslivecasino.Bac_BettingInfo.BetData.decode(reader, reader.uint32()));
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Bac_BettingInfo message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof aslivecasino.Bac_BettingInfo
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {aslivecasino.Bac_BettingInfo} Bac_BettingInfo
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Bac_BettingInfo.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Bac_BettingInfo message.
         * @function verify
         * @memberof aslivecasino.Bac_BettingInfo
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Bac_BettingInfo.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                if (!$util.isString(message.room_no))
                    return "room_no: string expected";
            if (message.table_no != null && message.hasOwnProperty("table_no"))
                if (!$util.isInteger(message.table_no))
                    return "table_no: integer expected";
            if (message.bet_data_list != null && message.hasOwnProperty("bet_data_list")) {
                if (!Array.isArray(message.bet_data_list))
                    return "bet_data_list: array expected";
                for (var i = 0; i < message.bet_data_list.length; ++i) {
                    var error = $root.aslivecasino.Bac_BettingInfo.BetData.verify(message.bet_data_list[i]);
                    if (error)
                        return "bet_data_list." + error;
                }
            }
            return null;
        };

        /**
         * Creates a Bac_BettingInfo message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof aslivecasino.Bac_BettingInfo
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {aslivecasino.Bac_BettingInfo} Bac_BettingInfo
         */
        Bac_BettingInfo.fromObject = function fromObject(object) {
            if (object instanceof $root.aslivecasino.Bac_BettingInfo)
                return object;
            var message = new $root.aslivecasino.Bac_BettingInfo();
            if (object.room_no != null)
                message.room_no = String(object.room_no);
            if (object.table_no != null)
                message.table_no = object.table_no | 0;
            if (object.bet_data_list) {
                if (!Array.isArray(object.bet_data_list))
                    throw TypeError(".aslivecasino.Bac_BettingInfo.bet_data_list: array expected");
                message.bet_data_list = [];
                for (var i = 0; i < object.bet_data_list.length; ++i) {
                    if (typeof object.bet_data_list[i] !== "object")
                        throw TypeError(".aslivecasino.Bac_BettingInfo.bet_data_list: object expected");
                    message.bet_data_list[i] = $root.aslivecasino.Bac_BettingInfo.BetData.fromObject(object.bet_data_list[i]);
                }
            }
            return message;
        };

        /**
         * Creates a plain object from a Bac_BettingInfo message. Also converts values to other types if specified.
         * @function toObject
         * @memberof aslivecasino.Bac_BettingInfo
         * @static
         * @param {aslivecasino.Bac_BettingInfo} message Bac_BettingInfo
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Bac_BettingInfo.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.arrays || options.defaults)
                object.bet_data_list = [];
            if (options.defaults) {
                object.room_no = "";
                object.table_no = 0;
            }
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                object.room_no = message.room_no;
            if (message.table_no != null && message.hasOwnProperty("table_no"))
                object.table_no = message.table_no;
            if (message.bet_data_list && message.bet_data_list.length) {
                object.bet_data_list = [];
                for (var j = 0; j < message.bet_data_list.length; ++j)
                    object.bet_data_list[j] = $root.aslivecasino.Bac_BettingInfo.BetData.toObject(message.bet_data_list[j], options);
            }
            return object;
        };

        /**
         * Converts this Bac_BettingInfo to JSON.
         * @function toJSON
         * @memberof aslivecasino.Bac_BettingInfo
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Bac_BettingInfo.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        Bac_BettingInfo.BetData = (function() {

            /**
             * Properties of a BetData.
             * @memberof aslivecasino.Bac_BettingInfo
             * @interface IBetData
             * @property {number|null} [seat_no] BetData seat_no
             * @property {string|null} [user_name] BetData user_name
             * @property {number|null} [balance] BetData balance
             * @property {Array.<aslivecasino.Bac_BettingInfo.BetData.IEachBet>|null} [each_bet_list] BetData each_bet_list
             */

            /**
             * Constructs a new BetData.
             * @memberof aslivecasino.Bac_BettingInfo
             * @classdesc Represents a BetData.
             * @implements IBetData
             * @constructor
             * @param {aslivecasino.Bac_BettingInfo.IBetData=} [properties] Properties to set
             */
            function BetData(properties) {
                this.each_bet_list = [];
                if (properties)
                    for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                        if (properties[keys[i]] != null)
                            this[keys[i]] = properties[keys[i]];
            }

            /**
             * BetData seat_no.
             * @member {number} seat_no
             * @memberof aslivecasino.Bac_BettingInfo.BetData
             * @instance
             */
            BetData.prototype.seat_no = 0;

            /**
             * BetData user_name.
             * @member {string} user_name
             * @memberof aslivecasino.Bac_BettingInfo.BetData
             * @instance
             */
            BetData.prototype.user_name = "";

            /**
             * BetData balance.
             * @member {number} balance
             * @memberof aslivecasino.Bac_BettingInfo.BetData
             * @instance
             */
            BetData.prototype.balance = 0;

            /**
             * BetData each_bet_list.
             * @member {Array.<aslivecasino.Bac_BettingInfo.BetData.IEachBet>} each_bet_list
             * @memberof aslivecasino.Bac_BettingInfo.BetData
             * @instance
             */
            BetData.prototype.each_bet_list = $util.emptyArray;

            /**
             * Creates a new BetData instance using the specified properties.
             * @function create
             * @memberof aslivecasino.Bac_BettingInfo.BetData
             * @static
             * @param {aslivecasino.Bac_BettingInfo.IBetData=} [properties] Properties to set
             * @returns {aslivecasino.Bac_BettingInfo.BetData} BetData instance
             */
            BetData.create = function create(properties) {
                return new BetData(properties);
            };

            /**
             * Encodes the specified BetData message. Does not implicitly {@link aslivecasino.Bac_BettingInfo.BetData.verify|verify} messages.
             * @function encode
             * @memberof aslivecasino.Bac_BettingInfo.BetData
             * @static
             * @param {aslivecasino.Bac_BettingInfo.IBetData} message BetData message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            BetData.encode = function encode(message, writer) {
                if (!writer)
                    writer = $Writer.create();
                if (message.seat_no != null && message.hasOwnProperty("seat_no"))
                    writer.uint32(/* id 1, wireType 0 =*/8).sint32(message.seat_no);
                if (message.user_name != null && message.hasOwnProperty("user_name"))
                    writer.uint32(/* id 2, wireType 2 =*/18).string(message.user_name);
                if (message.balance != null && message.hasOwnProperty("balance"))
                    writer.uint32(/* id 3, wireType 1 =*/25).double(message.balance);
                if (message.each_bet_list != null && message.each_bet_list.length)
                    for (var i = 0; i < message.each_bet_list.length; ++i)
                        $root.aslivecasino.Bac_BettingInfo.BetData.EachBet.encode(message.each_bet_list[i], writer.uint32(/* id 4, wireType 2 =*/34).fork()).ldelim();
                return writer;
            };

            /**
             * Encodes the specified BetData message, length delimited. Does not implicitly {@link aslivecasino.Bac_BettingInfo.BetData.verify|verify} messages.
             * @function encodeDelimited
             * @memberof aslivecasino.Bac_BettingInfo.BetData
             * @static
             * @param {aslivecasino.Bac_BettingInfo.IBetData} message BetData message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            BetData.encodeDelimited = function encodeDelimited(message, writer) {
                return this.encode(message, writer).ldelim();
            };

            /**
             * Decodes a BetData message from the specified reader or buffer.
             * @function decode
             * @memberof aslivecasino.Bac_BettingInfo.BetData
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @param {number} [length] Message length if known beforehand
             * @returns {aslivecasino.Bac_BettingInfo.BetData} BetData
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            BetData.decode = function decode(reader, length) {
                if (!(reader instanceof $Reader))
                    reader = $Reader.create(reader);
                var end = length === undefined ? reader.len : reader.pos + length, message = new $root.aslivecasino.Bac_BettingInfo.BetData();
                while (reader.pos < end) {
                    var tag = reader.uint32();
                    switch (tag >>> 3) {
                    case 1:
                        message.seat_no = reader.sint32();
                        break;
                    case 2:
                        message.user_name = reader.string();
                        break;
                    case 3:
                        message.balance = reader.double();
                        break;
                    case 4:
                        if (!(message.each_bet_list && message.each_bet_list.length))
                            message.each_bet_list = [];
                        message.each_bet_list.push($root.aslivecasino.Bac_BettingInfo.BetData.EachBet.decode(reader, reader.uint32()));
                        break;
                    default:
                        reader.skipType(tag & 7);
                        break;
                    }
                }
                return message;
            };

            /**
             * Decodes a BetData message from the specified reader or buffer, length delimited.
             * @function decodeDelimited
             * @memberof aslivecasino.Bac_BettingInfo.BetData
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @returns {aslivecasino.Bac_BettingInfo.BetData} BetData
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            BetData.decodeDelimited = function decodeDelimited(reader) {
                if (!(reader instanceof $Reader))
                    reader = new $Reader(reader);
                return this.decode(reader, reader.uint32());
            };

            /**
             * Verifies a BetData message.
             * @function verify
             * @memberof aslivecasino.Bac_BettingInfo.BetData
             * @static
             * @param {Object.<string,*>} message Plain object to verify
             * @returns {string|null} `null` if valid, otherwise the reason why it is not
             */
            BetData.verify = function verify(message) {
                if (typeof message !== "object" || message === null)
                    return "object expected";
                if (message.seat_no != null && message.hasOwnProperty("seat_no"))
                    if (!$util.isInteger(message.seat_no))
                        return "seat_no: integer expected";
                if (message.user_name != null && message.hasOwnProperty("user_name"))
                    if (!$util.isString(message.user_name))
                        return "user_name: string expected";
                if (message.balance != null && message.hasOwnProperty("balance"))
                    if (typeof message.balance !== "number")
                        return "balance: number expected";
                if (message.each_bet_list != null && message.hasOwnProperty("each_bet_list")) {
                    if (!Array.isArray(message.each_bet_list))
                        return "each_bet_list: array expected";
                    for (var i = 0; i < message.each_bet_list.length; ++i) {
                        var error = $root.aslivecasino.Bac_BettingInfo.BetData.EachBet.verify(message.each_bet_list[i]);
                        if (error)
                            return "each_bet_list." + error;
                    }
                }
                return null;
            };

            /**
             * Creates a BetData message from a plain object. Also converts values to their respective internal types.
             * @function fromObject
             * @memberof aslivecasino.Bac_BettingInfo.BetData
             * @static
             * @param {Object.<string,*>} object Plain object
             * @returns {aslivecasino.Bac_BettingInfo.BetData} BetData
             */
            BetData.fromObject = function fromObject(object) {
                if (object instanceof $root.aslivecasino.Bac_BettingInfo.BetData)
                    return object;
                var message = new $root.aslivecasino.Bac_BettingInfo.BetData();
                if (object.seat_no != null)
                    message.seat_no = object.seat_no | 0;
                if (object.user_name != null)
                    message.user_name = String(object.user_name);
                if (object.balance != null)
                    message.balance = Number(object.balance);
                if (object.each_bet_list) {
                    if (!Array.isArray(object.each_bet_list))
                        throw TypeError(".aslivecasino.Bac_BettingInfo.BetData.each_bet_list: array expected");
                    message.each_bet_list = [];
                    for (var i = 0; i < object.each_bet_list.length; ++i) {
                        if (typeof object.each_bet_list[i] !== "object")
                            throw TypeError(".aslivecasino.Bac_BettingInfo.BetData.each_bet_list: object expected");
                        message.each_bet_list[i] = $root.aslivecasino.Bac_BettingInfo.BetData.EachBet.fromObject(object.each_bet_list[i]);
                    }
                }
                return message;
            };

            /**
             * Creates a plain object from a BetData message. Also converts values to other types if specified.
             * @function toObject
             * @memberof aslivecasino.Bac_BettingInfo.BetData
             * @static
             * @param {aslivecasino.Bac_BettingInfo.BetData} message BetData
             * @param {$protobuf.IConversionOptions} [options] Conversion options
             * @returns {Object.<string,*>} Plain object
             */
            BetData.toObject = function toObject(message, options) {
                if (!options)
                    options = {};
                var object = {};
                if (options.arrays || options.defaults)
                    object.each_bet_list = [];
                if (options.defaults) {
                    object.seat_no = 0;
                    object.user_name = "";
                    object.balance = 0;
                }
                if (message.seat_no != null && message.hasOwnProperty("seat_no"))
                    object.seat_no = message.seat_no;
                if (message.user_name != null && message.hasOwnProperty("user_name"))
                    object.user_name = message.user_name;
                if (message.balance != null && message.hasOwnProperty("balance"))
                    object.balance = options.json && !isFinite(message.balance) ? String(message.balance) : message.balance;
                if (message.each_bet_list && message.each_bet_list.length) {
                    object.each_bet_list = [];
                    for (var j = 0; j < message.each_bet_list.length; ++j)
                        object.each_bet_list[j] = $root.aslivecasino.Bac_BettingInfo.BetData.EachBet.toObject(message.each_bet_list[j], options);
                }
                return object;
            };

            /**
             * Converts this BetData to JSON.
             * @function toJSON
             * @memberof aslivecasino.Bac_BettingInfo.BetData
             * @instance
             * @returns {Object.<string,*>} JSON object
             */
            BetData.prototype.toJSON = function toJSON() {
                return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
            };

            BetData.EachBet = (function() {

                /**
                 * Properties of an EachBet.
                 * @memberof aslivecasino.Bac_BettingInfo.BetData
                 * @interface IEachBet
                 * @property {aslivecasino.Bac_EnumDivType|null} [div_type] EachBet div_type
                 * @property {number|Long|null} [bet_time] EachBet bet_time
                 * @property {number|null} [chips] EachBet chips
                 */

                /**
                 * Constructs a new EachBet.
                 * @memberof aslivecasino.Bac_BettingInfo.BetData
                 * @classdesc Represents an EachBet.
                 * @implements IEachBet
                 * @constructor
                 * @param {aslivecasino.Bac_BettingInfo.BetData.IEachBet=} [properties] Properties to set
                 */
                function EachBet(properties) {
                    if (properties)
                        for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                            if (properties[keys[i]] != null)
                                this[keys[i]] = properties[keys[i]];
                }

                /**
                 * EachBet div_type.
                 * @member {aslivecasino.Bac_EnumDivType} div_type
                 * @memberof aslivecasino.Bac_BettingInfo.BetData.EachBet
                 * @instance
                 */
                EachBet.prototype.div_type = 0;

                /**
                 * EachBet bet_time.
                 * @member {number|Long} bet_time
                 * @memberof aslivecasino.Bac_BettingInfo.BetData.EachBet
                 * @instance
                 */
                EachBet.prototype.bet_time = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

                /**
                 * EachBet chips.
                 * @member {number} chips
                 * @memberof aslivecasino.Bac_BettingInfo.BetData.EachBet
                 * @instance
                 */
                EachBet.prototype.chips = 0;

                /**
                 * Creates a new EachBet instance using the specified properties.
                 * @function create
                 * @memberof aslivecasino.Bac_BettingInfo.BetData.EachBet
                 * @static
                 * @param {aslivecasino.Bac_BettingInfo.BetData.IEachBet=} [properties] Properties to set
                 * @returns {aslivecasino.Bac_BettingInfo.BetData.EachBet} EachBet instance
                 */
                EachBet.create = function create(properties) {
                    return new EachBet(properties);
                };

                /**
                 * Encodes the specified EachBet message. Does not implicitly {@link aslivecasino.Bac_BettingInfo.BetData.EachBet.verify|verify} messages.
                 * @function encode
                 * @memberof aslivecasino.Bac_BettingInfo.BetData.EachBet
                 * @static
                 * @param {aslivecasino.Bac_BettingInfo.BetData.IEachBet} message EachBet message or plain object to encode
                 * @param {$protobuf.Writer} [writer] Writer to encode to
                 * @returns {$protobuf.Writer} Writer
                 */
                EachBet.encode = function encode(message, writer) {
                    if (!writer)
                        writer = $Writer.create();
                    if (message.div_type != null && message.hasOwnProperty("div_type"))
                        writer.uint32(/* id 1, wireType 0 =*/8).int32(message.div_type);
                    if (message.bet_time != null && message.hasOwnProperty("bet_time"))
                        writer.uint32(/* id 2, wireType 0 =*/16).int64(message.bet_time);
                    if (message.chips != null && message.hasOwnProperty("chips"))
                        writer.uint32(/* id 3, wireType 0 =*/24).int32(message.chips);
                    return writer;
                };

                /**
                 * Encodes the specified EachBet message, length delimited. Does not implicitly {@link aslivecasino.Bac_BettingInfo.BetData.EachBet.verify|verify} messages.
                 * @function encodeDelimited
                 * @memberof aslivecasino.Bac_BettingInfo.BetData.EachBet
                 * @static
                 * @param {aslivecasino.Bac_BettingInfo.BetData.IEachBet} message EachBet message or plain object to encode
                 * @param {$protobuf.Writer} [writer] Writer to encode to
                 * @returns {$protobuf.Writer} Writer
                 */
                EachBet.encodeDelimited = function encodeDelimited(message, writer) {
                    return this.encode(message, writer).ldelim();
                };

                /**
                 * Decodes an EachBet message from the specified reader or buffer.
                 * @function decode
                 * @memberof aslivecasino.Bac_BettingInfo.BetData.EachBet
                 * @static
                 * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                 * @param {number} [length] Message length if known beforehand
                 * @returns {aslivecasino.Bac_BettingInfo.BetData.EachBet} EachBet
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                EachBet.decode = function decode(reader, length) {
                    if (!(reader instanceof $Reader))
                        reader = $Reader.create(reader);
                    var end = length === undefined ? reader.len : reader.pos + length, message = new $root.aslivecasino.Bac_BettingInfo.BetData.EachBet();
                    while (reader.pos < end) {
                        var tag = reader.uint32();
                        switch (tag >>> 3) {
                        case 1:
                            message.div_type = reader.int32();
                            break;
                        case 2:
                            message.bet_time = reader.int64();
                            break;
                        case 3:
                            message.chips = reader.int32();
                            break;
                        default:
                            reader.skipType(tag & 7);
                            break;
                        }
                    }
                    return message;
                };

                /**
                 * Decodes an EachBet message from the specified reader or buffer, length delimited.
                 * @function decodeDelimited
                 * @memberof aslivecasino.Bac_BettingInfo.BetData.EachBet
                 * @static
                 * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                 * @returns {aslivecasino.Bac_BettingInfo.BetData.EachBet} EachBet
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                EachBet.decodeDelimited = function decodeDelimited(reader) {
                    if (!(reader instanceof $Reader))
                        reader = new $Reader(reader);
                    return this.decode(reader, reader.uint32());
                };

                /**
                 * Verifies an EachBet message.
                 * @function verify
                 * @memberof aslivecasino.Bac_BettingInfo.BetData.EachBet
                 * @static
                 * @param {Object.<string,*>} message Plain object to verify
                 * @returns {string|null} `null` if valid, otherwise the reason why it is not
                 */
                EachBet.verify = function verify(message) {
                    if (typeof message !== "object" || message === null)
                        return "object expected";
                    if (message.div_type != null && message.hasOwnProperty("div_type"))
                        switch (message.div_type) {
                        default:
                            return "div_type: enum value expected";
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                            break;
                        }
                    if (message.bet_time != null && message.hasOwnProperty("bet_time"))
                        if (!$util.isInteger(message.bet_time) && !(message.bet_time && $util.isInteger(message.bet_time.low) && $util.isInteger(message.bet_time.high)))
                            return "bet_time: integer|Long expected";
                    if (message.chips != null && message.hasOwnProperty("chips"))
                        if (!$util.isInteger(message.chips))
                            return "chips: integer expected";
                    return null;
                };

                /**
                 * Creates an EachBet message from a plain object. Also converts values to their respective internal types.
                 * @function fromObject
                 * @memberof aslivecasino.Bac_BettingInfo.BetData.EachBet
                 * @static
                 * @param {Object.<string,*>} object Plain object
                 * @returns {aslivecasino.Bac_BettingInfo.BetData.EachBet} EachBet
                 */
                EachBet.fromObject = function fromObject(object) {
                    if (object instanceof $root.aslivecasino.Bac_BettingInfo.BetData.EachBet)
                        return object;
                    var message = new $root.aslivecasino.Bac_BettingInfo.BetData.EachBet();
                    switch (object.div_type) {
                    case "PLAYER":
                    case 0:
                        message.div_type = 0;
                        break;
                    case "BANKER":
                    case 1:
                        message.div_type = 1;
                        break;
                    case "TIE":
                    case 2:
                        message.div_type = 2;
                        break;
                    case "PPAIR":
                    case 3:
                        message.div_type = 3;
                        break;
                    case "BPAIR":
                    case 4:
                        message.div_type = 4;
                        break;
                    }
                    if (object.bet_time != null)
                        if ($util.Long)
                            (message.bet_time = $util.Long.fromValue(object.bet_time)).unsigned = false;
                        else if (typeof object.bet_time === "string")
                            message.bet_time = parseInt(object.bet_time, 10);
                        else if (typeof object.bet_time === "number")
                            message.bet_time = object.bet_time;
                        else if (typeof object.bet_time === "object")
                            message.bet_time = new $util.LongBits(object.bet_time.low >>> 0, object.bet_time.high >>> 0).toNumber();
                    if (object.chips != null)
                        message.chips = object.chips | 0;
                    return message;
                };

                /**
                 * Creates a plain object from an EachBet message. Also converts values to other types if specified.
                 * @function toObject
                 * @memberof aslivecasino.Bac_BettingInfo.BetData.EachBet
                 * @static
                 * @param {aslivecasino.Bac_BettingInfo.BetData.EachBet} message EachBet
                 * @param {$protobuf.IConversionOptions} [options] Conversion options
                 * @returns {Object.<string,*>} Plain object
                 */
                EachBet.toObject = function toObject(message, options) {
                    if (!options)
                        options = {};
                    var object = {};
                    if (options.defaults) {
                        object.div_type = options.enums === String ? "PLAYER" : 0;
                        if ($util.Long) {
                            var long = new $util.Long(0, 0, false);
                            object.bet_time = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                        } else
                            object.bet_time = options.longs === String ? "0" : 0;
                        object.chips = 0;
                    }
                    if (message.div_type != null && message.hasOwnProperty("div_type"))
                        object.div_type = options.enums === String ? $root.aslivecasino.Bac_EnumDivType[message.div_type] : message.div_type;
                    if (message.bet_time != null && message.hasOwnProperty("bet_time"))
                        if (typeof message.bet_time === "number")
                            object.bet_time = options.longs === String ? String(message.bet_time) : message.bet_time;
                        else
                            object.bet_time = options.longs === String ? $util.Long.prototype.toString.call(message.bet_time) : options.longs === Number ? new $util.LongBits(message.bet_time.low >>> 0, message.bet_time.high >>> 0).toNumber() : message.bet_time;
                    if (message.chips != null && message.hasOwnProperty("chips"))
                        object.chips = message.chips;
                    return object;
                };

                /**
                 * Converts this EachBet to JSON.
                 * @function toJSON
                 * @memberof aslivecasino.Bac_BettingInfo.BetData.EachBet
                 * @instance
                 * @returns {Object.<string,*>} JSON object
                 */
                EachBet.prototype.toJSON = function toJSON() {
                    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
                };

                return EachBet;
            })();

            return BetData;
        })();

        return Bac_BettingInfo;
    })();

    /**
     * Bac_EnumRoomState enum.
     * @name aslivecasino.Bac_EnumRoomState
     * @enum {string}
     * @property {number} START=0 START value
     * @property {number} ONSHUFFLE=1 ONSHUFFLE value
     * @property {number} ROUNDSTART=2 ROUNDSTART value
     * @property {number} ONBETTING=3 ONBETTING value
     * @property {number} EXTBETTINGTIME=4 EXTBETTINGTIME value
     * @property {number} SHOWDOWN=5 SHOWDOWN value
     * @property {number} SETTLEMENT=6 SETTLEMENT value
     * @property {number} ROUNDOVER=7 ROUNDOVER value
     * @property {number} ONABNORMAL=8 ONABNORMAL value
     */
    aslivecasino.Bac_EnumRoomState = (function() {
        var valuesById = {}, values = Object.create(valuesById);
        values[valuesById[0] = "START"] = 0;
        values[valuesById[1] = "ONSHUFFLE"] = 1;
        values[valuesById[2] = "ROUNDSTART"] = 2;
        values[valuesById[3] = "ONBETTING"] = 3;
        values[valuesById[4] = "EXTBETTINGTIME"] = 4;
        values[valuesById[5] = "SHOWDOWN"] = 5;
        values[valuesById[6] = "SETTLEMENT"] = 6;
        values[valuesById[7] = "ROUNDOVER"] = 7;
        values[valuesById[8] = "ONABNORMAL"] = 8;
        return values;
    })();

    aslivecasino.Bac_BriefRoomState = (function() {

        /**
         * Properties of a Bac_BriefRoomState.
         * @memberof aslivecasino
         * @interface IBac_BriefRoomState
         * @property {string|null} [room_no] Bac_BriefRoomState room_no
         * @property {aslivecasino.Bac_EnumRoomState|null} [room_state] Bac_BriefRoomState room_state
         * @property {number|null} [count_down] Bac_BriefRoomState count_down
         */

        /**
         * Constructs a new Bac_BriefRoomState.
         * @memberof aslivecasino
         * @classdesc Represents a Bac_BriefRoomState.
         * @implements IBac_BriefRoomState
         * @constructor
         * @param {aslivecasino.IBac_BriefRoomState=} [properties] Properties to set
         */
        function Bac_BriefRoomState(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Bac_BriefRoomState room_no.
         * @member {string} room_no
         * @memberof aslivecasino.Bac_BriefRoomState
         * @instance
         */
        Bac_BriefRoomState.prototype.room_no = "";

        /**
         * Bac_BriefRoomState room_state.
         * @member {aslivecasino.Bac_EnumRoomState} room_state
         * @memberof aslivecasino.Bac_BriefRoomState
         * @instance
         */
        Bac_BriefRoomState.prototype.room_state = 0;

        /**
         * Bac_BriefRoomState count_down.
         * @member {number} count_down
         * @memberof aslivecasino.Bac_BriefRoomState
         * @instance
         */
        Bac_BriefRoomState.prototype.count_down = 0;

        /**
         * Creates a new Bac_BriefRoomState instance using the specified properties.
         * @function create
         * @memberof aslivecasino.Bac_BriefRoomState
         * @static
         * @param {aslivecasino.IBac_BriefRoomState=} [properties] Properties to set
         * @returns {aslivecasino.Bac_BriefRoomState} Bac_BriefRoomState instance
         */
        Bac_BriefRoomState.create = function create(properties) {
            return new Bac_BriefRoomState(properties);
        };

        /**
         * Encodes the specified Bac_BriefRoomState message. Does not implicitly {@link aslivecasino.Bac_BriefRoomState.verify|verify} messages.
         * @function encode
         * @memberof aslivecasino.Bac_BriefRoomState
         * @static
         * @param {aslivecasino.IBac_BriefRoomState} message Bac_BriefRoomState message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Bac_BriefRoomState.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.room_no);
            if (message.room_state != null && message.hasOwnProperty("room_state"))
                writer.uint32(/* id 2, wireType 0 =*/16).int32(message.room_state);
            if (message.count_down != null && message.hasOwnProperty("count_down"))
                writer.uint32(/* id 3, wireType 0 =*/24).int32(message.count_down);
            return writer;
        };

        /**
         * Encodes the specified Bac_BriefRoomState message, length delimited. Does not implicitly {@link aslivecasino.Bac_BriefRoomState.verify|verify} messages.
         * @function encodeDelimited
         * @memberof aslivecasino.Bac_BriefRoomState
         * @static
         * @param {aslivecasino.IBac_BriefRoomState} message Bac_BriefRoomState message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Bac_BriefRoomState.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Bac_BriefRoomState message from the specified reader or buffer.
         * @function decode
         * @memberof aslivecasino.Bac_BriefRoomState
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {aslivecasino.Bac_BriefRoomState} Bac_BriefRoomState
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Bac_BriefRoomState.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.aslivecasino.Bac_BriefRoomState();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.room_no = reader.string();
                    break;
                case 2:
                    message.room_state = reader.int32();
                    break;
                case 3:
                    message.count_down = reader.int32();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Bac_BriefRoomState message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof aslivecasino.Bac_BriefRoomState
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {aslivecasino.Bac_BriefRoomState} Bac_BriefRoomState
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Bac_BriefRoomState.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Bac_BriefRoomState message.
         * @function verify
         * @memberof aslivecasino.Bac_BriefRoomState
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Bac_BriefRoomState.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                if (!$util.isString(message.room_no))
                    return "room_no: string expected";
            if (message.room_state != null && message.hasOwnProperty("room_state"))
                switch (message.room_state) {
                default:
                    return "room_state: enum value expected";
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                    break;
                }
            if (message.count_down != null && message.hasOwnProperty("count_down"))
                if (!$util.isInteger(message.count_down))
                    return "count_down: integer expected";
            return null;
        };

        /**
         * Creates a Bac_BriefRoomState message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof aslivecasino.Bac_BriefRoomState
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {aslivecasino.Bac_BriefRoomState} Bac_BriefRoomState
         */
        Bac_BriefRoomState.fromObject = function fromObject(object) {
            if (object instanceof $root.aslivecasino.Bac_BriefRoomState)
                return object;
            var message = new $root.aslivecasino.Bac_BriefRoomState();
            if (object.room_no != null)
                message.room_no = String(object.room_no);
            switch (object.room_state) {
            case "START":
            case 0:
                message.room_state = 0;
                break;
            case "ONSHUFFLE":
            case 1:
                message.room_state = 1;
                break;
            case "ROUNDSTART":
            case 2:
                message.room_state = 2;
                break;
            case "ONBETTING":
            case 3:
                message.room_state = 3;
                break;
            case "EXTBETTINGTIME":
            case 4:
                message.room_state = 4;
                break;
            case "SHOWDOWN":
            case 5:
                message.room_state = 5;
                break;
            case "SETTLEMENT":
            case 6:
                message.room_state = 6;
                break;
            case "ROUNDOVER":
            case 7:
                message.room_state = 7;
                break;
            case "ONABNORMAL":
            case 8:
                message.room_state = 8;
                break;
            }
            if (object.count_down != null)
                message.count_down = object.count_down | 0;
            return message;
        };

        /**
         * Creates a plain object from a Bac_BriefRoomState message. Also converts values to other types if specified.
         * @function toObject
         * @memberof aslivecasino.Bac_BriefRoomState
         * @static
         * @param {aslivecasino.Bac_BriefRoomState} message Bac_BriefRoomState
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Bac_BriefRoomState.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.room_no = "";
                object.room_state = options.enums === String ? "START" : 0;
                object.count_down = 0;
            }
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                object.room_no = message.room_no;
            if (message.room_state != null && message.hasOwnProperty("room_state"))
                object.room_state = options.enums === String ? $root.aslivecasino.Bac_EnumRoomState[message.room_state] : message.room_state;
            if (message.count_down != null && message.hasOwnProperty("count_down"))
                object.count_down = message.count_down;
            return object;
        };

        /**
         * Converts this Bac_BriefRoomState to JSON.
         * @function toJSON
         * @memberof aslivecasino.Bac_BriefRoomState
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Bac_BriefRoomState.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return Bac_BriefRoomState;
    })();

    aslivecasino.Bac_DetailRoomState = (function() {

        /**
         * Properties of a Bac_DetailRoomState.
         * @memberof aslivecasino
         * @interface IBac_DetailRoomState
         * @property {string|null} [room_no] Bac_DetailRoomState room_no
         * @property {aslivecasino.Bac_EnumRoomState|null} [room_state] Bac_DetailRoomState room_state
         * @property {number|null} [count_down] Bac_DetailRoomState count_down
         * @property {number|null} [remain_extbet_count] Bac_DetailRoomState remain_extbet_count
         * @property {aslivecasino.Bac_DetailRoomState.IShowdownStateInfo_Classic|null} [classic_showdown_info] Bac_DetailRoomState classic_showdown_info
         * @property {aslivecasino.Bac_DetailRoomState.IShowdownStateInfo_MeCard|null} [mecard_showdown_info] Bac_DetailRoomState mecard_showdown_info
         * @property {aslivecasino.Bac_DetailRoomState.ISettlementStateInfo|null} [settlement_info] Bac_DetailRoomState settlement_info
         */

        /**
         * Constructs a new Bac_DetailRoomState.
         * @memberof aslivecasino
         * @classdesc Represents a Bac_DetailRoomState.
         * @implements IBac_DetailRoomState
         * @constructor
         * @param {aslivecasino.IBac_DetailRoomState=} [properties] Properties to set
         */
        function Bac_DetailRoomState(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Bac_DetailRoomState room_no.
         * @member {string} room_no
         * @memberof aslivecasino.Bac_DetailRoomState
         * @instance
         */
        Bac_DetailRoomState.prototype.room_no = "";

        /**
         * Bac_DetailRoomState room_state.
         * @member {aslivecasino.Bac_EnumRoomState} room_state
         * @memberof aslivecasino.Bac_DetailRoomState
         * @instance
         */
        Bac_DetailRoomState.prototype.room_state = 0;

        /**
         * Bac_DetailRoomState count_down.
         * @member {number} count_down
         * @memberof aslivecasino.Bac_DetailRoomState
         * @instance
         */
        Bac_DetailRoomState.prototype.count_down = 0;

        /**
         * Bac_DetailRoomState remain_extbet_count.
         * @member {number} remain_extbet_count
         * @memberof aslivecasino.Bac_DetailRoomState
         * @instance
         */
        Bac_DetailRoomState.prototype.remain_extbet_count = 0;

        /**
         * Bac_DetailRoomState classic_showdown_info.
         * @member {aslivecasino.Bac_DetailRoomState.IShowdownStateInfo_Classic|null|undefined} classic_showdown_info
         * @memberof aslivecasino.Bac_DetailRoomState
         * @instance
         */
        Bac_DetailRoomState.prototype.classic_showdown_info = null;

        /**
         * Bac_DetailRoomState mecard_showdown_info.
         * @member {aslivecasino.Bac_DetailRoomState.IShowdownStateInfo_MeCard|null|undefined} mecard_showdown_info
         * @memberof aslivecasino.Bac_DetailRoomState
         * @instance
         */
        Bac_DetailRoomState.prototype.mecard_showdown_info = null;

        /**
         * Bac_DetailRoomState settlement_info.
         * @member {aslivecasino.Bac_DetailRoomState.ISettlementStateInfo|null|undefined} settlement_info
         * @memberof aslivecasino.Bac_DetailRoomState
         * @instance
         */
        Bac_DetailRoomState.prototype.settlement_info = null;

        // OneOf field names bound to virtual getters and setters
        var $oneOfFields;

        /**
         * Bac_DetailRoomState state_oneof.
         * @member {"classic_showdown_info"|"mecard_showdown_info"|"settlement_info"|undefined} state_oneof
         * @memberof aslivecasino.Bac_DetailRoomState
         * @instance
         */
        Object.defineProperty(Bac_DetailRoomState.prototype, "state_oneof", {
            get: $util.oneOfGetter($oneOfFields = ["classic_showdown_info", "mecard_showdown_info", "settlement_info"]),
            set: $util.oneOfSetter($oneOfFields)
        });

        /**
         * Creates a new Bac_DetailRoomState instance using the specified properties.
         * @function create
         * @memberof aslivecasino.Bac_DetailRoomState
         * @static
         * @param {aslivecasino.IBac_DetailRoomState=} [properties] Properties to set
         * @returns {aslivecasino.Bac_DetailRoomState} Bac_DetailRoomState instance
         */
        Bac_DetailRoomState.create = function create(properties) {
            return new Bac_DetailRoomState(properties);
        };

        /**
         * Encodes the specified Bac_DetailRoomState message. Does not implicitly {@link aslivecasino.Bac_DetailRoomState.verify|verify} messages.
         * @function encode
         * @memberof aslivecasino.Bac_DetailRoomState
         * @static
         * @param {aslivecasino.IBac_DetailRoomState} message Bac_DetailRoomState message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Bac_DetailRoomState.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.room_no);
            if (message.room_state != null && message.hasOwnProperty("room_state"))
                writer.uint32(/* id 2, wireType 0 =*/16).int32(message.room_state);
            if (message.count_down != null && message.hasOwnProperty("count_down"))
                writer.uint32(/* id 3, wireType 0 =*/24).int32(message.count_down);
            if (message.remain_extbet_count != null && message.hasOwnProperty("remain_extbet_count"))
                writer.uint32(/* id 4, wireType 0 =*/32).int32(message.remain_extbet_count);
            if (message.classic_showdown_info != null && message.hasOwnProperty("classic_showdown_info"))
                $root.aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_Classic.encode(message.classic_showdown_info, writer.uint32(/* id 7, wireType 2 =*/58).fork()).ldelim();
            if (message.mecard_showdown_info != null && message.hasOwnProperty("mecard_showdown_info"))
                $root.aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.encode(message.mecard_showdown_info, writer.uint32(/* id 8, wireType 2 =*/66).fork()).ldelim();
            if (message.settlement_info != null && message.hasOwnProperty("settlement_info"))
                $root.aslivecasino.Bac_DetailRoomState.SettlementStateInfo.encode(message.settlement_info, writer.uint32(/* id 10, wireType 2 =*/82).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified Bac_DetailRoomState message, length delimited. Does not implicitly {@link aslivecasino.Bac_DetailRoomState.verify|verify} messages.
         * @function encodeDelimited
         * @memberof aslivecasino.Bac_DetailRoomState
         * @static
         * @param {aslivecasino.IBac_DetailRoomState} message Bac_DetailRoomState message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Bac_DetailRoomState.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Bac_DetailRoomState message from the specified reader or buffer.
         * @function decode
         * @memberof aslivecasino.Bac_DetailRoomState
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {aslivecasino.Bac_DetailRoomState} Bac_DetailRoomState
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Bac_DetailRoomState.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.aslivecasino.Bac_DetailRoomState();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.room_no = reader.string();
                    break;
                case 2:
                    message.room_state = reader.int32();
                    break;
                case 3:
                    message.count_down = reader.int32();
                    break;
                case 4:
                    message.remain_extbet_count = reader.int32();
                    break;
                case 7:
                    message.classic_showdown_info = $root.aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_Classic.decode(reader, reader.uint32());
                    break;
                case 8:
                    message.mecard_showdown_info = $root.aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.decode(reader, reader.uint32());
                    break;
                case 10:
                    message.settlement_info = $root.aslivecasino.Bac_DetailRoomState.SettlementStateInfo.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Bac_DetailRoomState message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof aslivecasino.Bac_DetailRoomState
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {aslivecasino.Bac_DetailRoomState} Bac_DetailRoomState
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Bac_DetailRoomState.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Bac_DetailRoomState message.
         * @function verify
         * @memberof aslivecasino.Bac_DetailRoomState
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Bac_DetailRoomState.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            var properties = {};
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                if (!$util.isString(message.room_no))
                    return "room_no: string expected";
            if (message.room_state != null && message.hasOwnProperty("room_state"))
                switch (message.room_state) {
                default:
                    return "room_state: enum value expected";
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                    break;
                }
            if (message.count_down != null && message.hasOwnProperty("count_down"))
                if (!$util.isInteger(message.count_down))
                    return "count_down: integer expected";
            if (message.remain_extbet_count != null && message.hasOwnProperty("remain_extbet_count"))
                if (!$util.isInteger(message.remain_extbet_count))
                    return "remain_extbet_count: integer expected";
            if (message.classic_showdown_info != null && message.hasOwnProperty("classic_showdown_info")) {
                properties.state_oneof = 1;
                {
                    var error = $root.aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_Classic.verify(message.classic_showdown_info);
                    if (error)
                        return "classic_showdown_info." + error;
                }
            }
            if (message.mecard_showdown_info != null && message.hasOwnProperty("mecard_showdown_info")) {
                if (properties.state_oneof === 1)
                    return "state_oneof: multiple values";
                properties.state_oneof = 1;
                {
                    var error = $root.aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.verify(message.mecard_showdown_info);
                    if (error)
                        return "mecard_showdown_info." + error;
                }
            }
            if (message.settlement_info != null && message.hasOwnProperty("settlement_info")) {
                if (properties.state_oneof === 1)
                    return "state_oneof: multiple values";
                properties.state_oneof = 1;
                {
                    var error = $root.aslivecasino.Bac_DetailRoomState.SettlementStateInfo.verify(message.settlement_info);
                    if (error)
                        return "settlement_info." + error;
                }
            }
            return null;
        };

        /**
         * Creates a Bac_DetailRoomState message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof aslivecasino.Bac_DetailRoomState
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {aslivecasino.Bac_DetailRoomState} Bac_DetailRoomState
         */
        Bac_DetailRoomState.fromObject = function fromObject(object) {
            if (object instanceof $root.aslivecasino.Bac_DetailRoomState)
                return object;
            var message = new $root.aslivecasino.Bac_DetailRoomState();
            if (object.room_no != null)
                message.room_no = String(object.room_no);
            switch (object.room_state) {
            case "START":
            case 0:
                message.room_state = 0;
                break;
            case "ONSHUFFLE":
            case 1:
                message.room_state = 1;
                break;
            case "ROUNDSTART":
            case 2:
                message.room_state = 2;
                break;
            case "ONBETTING":
            case 3:
                message.room_state = 3;
                break;
            case "EXTBETTINGTIME":
            case 4:
                message.room_state = 4;
                break;
            case "SHOWDOWN":
            case 5:
                message.room_state = 5;
                break;
            case "SETTLEMENT":
            case 6:
                message.room_state = 6;
                break;
            case "ROUNDOVER":
            case 7:
                message.room_state = 7;
                break;
            case "ONABNORMAL":
            case 8:
                message.room_state = 8;
                break;
            }
            if (object.count_down != null)
                message.count_down = object.count_down | 0;
            if (object.remain_extbet_count != null)
                message.remain_extbet_count = object.remain_extbet_count | 0;
            if (object.classic_showdown_info != null) {
                if (typeof object.classic_showdown_info !== "object")
                    throw TypeError(".aslivecasino.Bac_DetailRoomState.classic_showdown_info: object expected");
                message.classic_showdown_info = $root.aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_Classic.fromObject(object.classic_showdown_info);
            }
            if (object.mecard_showdown_info != null) {
                if (typeof object.mecard_showdown_info !== "object")
                    throw TypeError(".aslivecasino.Bac_DetailRoomState.mecard_showdown_info: object expected");
                message.mecard_showdown_info = $root.aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.fromObject(object.mecard_showdown_info);
            }
            if (object.settlement_info != null) {
                if (typeof object.settlement_info !== "object")
                    throw TypeError(".aslivecasino.Bac_DetailRoomState.settlement_info: object expected");
                message.settlement_info = $root.aslivecasino.Bac_DetailRoomState.SettlementStateInfo.fromObject(object.settlement_info);
            }
            return message;
        };

        /**
         * Creates a plain object from a Bac_DetailRoomState message. Also converts values to other types if specified.
         * @function toObject
         * @memberof aslivecasino.Bac_DetailRoomState
         * @static
         * @param {aslivecasino.Bac_DetailRoomState} message Bac_DetailRoomState
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Bac_DetailRoomState.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.room_no = "";
                object.room_state = options.enums === String ? "START" : 0;
                object.count_down = 0;
                object.remain_extbet_count = 0;
            }
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                object.room_no = message.room_no;
            if (message.room_state != null && message.hasOwnProperty("room_state"))
                object.room_state = options.enums === String ? $root.aslivecasino.Bac_EnumRoomState[message.room_state] : message.room_state;
            if (message.count_down != null && message.hasOwnProperty("count_down"))
                object.count_down = message.count_down;
            if (message.remain_extbet_count != null && message.hasOwnProperty("remain_extbet_count"))
                object.remain_extbet_count = message.remain_extbet_count;
            if (message.classic_showdown_info != null && message.hasOwnProperty("classic_showdown_info")) {
                object.classic_showdown_info = $root.aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_Classic.toObject(message.classic_showdown_info, options);
                if (options.oneofs)
                    object.state_oneof = "classic_showdown_info";
            }
            if (message.mecard_showdown_info != null && message.hasOwnProperty("mecard_showdown_info")) {
                object.mecard_showdown_info = $root.aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.toObject(message.mecard_showdown_info, options);
                if (options.oneofs)
                    object.state_oneof = "mecard_showdown_info";
            }
            if (message.settlement_info != null && message.hasOwnProperty("settlement_info")) {
                object.settlement_info = $root.aslivecasino.Bac_DetailRoomState.SettlementStateInfo.toObject(message.settlement_info, options);
                if (options.oneofs)
                    object.state_oneof = "settlement_info";
            }
            return object;
        };

        /**
         * Converts this Bac_DetailRoomState to JSON.
         * @function toJSON
         * @memberof aslivecasino.Bac_DetailRoomState
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Bac_DetailRoomState.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        Bac_DetailRoomState.ShowdownStateInfo_Classic = (function() {

            /**
             * Properties of a ShowdownStateInfo_Classic.
             * @memberof aslivecasino.Bac_DetailRoomState
             * @interface IShowdownStateInfo_Classic
             * @property {Array.<aslivecasino.ICardData>|null} [player_card_list] ShowdownStateInfo_Classic player_card_list
             * @property {Array.<aslivecasino.ICardData>|null} [banker_card_list] ShowdownStateInfo_Classic banker_card_list
             * @property {number|null} [showdown_count_down] ShowdownStateInfo_Classic showdown_count_down
             * @property {aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_Classic.EnumOpenStage|null} [open_stage] ShowdownStateInfo_Classic open_stage
             * @property {Array.<aslivecasino.Bac_EnumDivType>|null} [win_div_list] ShowdownStateInfo_Classic win_div_list
             * @property {number|null} [player_point] ShowdownStateInfo_Classic player_point
             * @property {number|null} [banker_point] ShowdownStateInfo_Classic banker_point
             */

            /**
             * Constructs a new ShowdownStateInfo_Classic.
             * @memberof aslivecasino.Bac_DetailRoomState
             * @classdesc Represents a ShowdownStateInfo_Classic.
             * @implements IShowdownStateInfo_Classic
             * @constructor
             * @param {aslivecasino.Bac_DetailRoomState.IShowdownStateInfo_Classic=} [properties] Properties to set
             */
            function ShowdownStateInfo_Classic(properties) {
                this.player_card_list = [];
                this.banker_card_list = [];
                this.win_div_list = [];
                if (properties)
                    for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                        if (properties[keys[i]] != null)
                            this[keys[i]] = properties[keys[i]];
            }

            /**
             * ShowdownStateInfo_Classic player_card_list.
             * @member {Array.<aslivecasino.ICardData>} player_card_list
             * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_Classic
             * @instance
             */
            ShowdownStateInfo_Classic.prototype.player_card_list = $util.emptyArray;

            /**
             * ShowdownStateInfo_Classic banker_card_list.
             * @member {Array.<aslivecasino.ICardData>} banker_card_list
             * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_Classic
             * @instance
             */
            ShowdownStateInfo_Classic.prototype.banker_card_list = $util.emptyArray;

            /**
             * ShowdownStateInfo_Classic showdown_count_down.
             * @member {number} showdown_count_down
             * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_Classic
             * @instance
             */
            ShowdownStateInfo_Classic.prototype.showdown_count_down = 0;

            /**
             * ShowdownStateInfo_Classic open_stage.
             * @member {aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_Classic.EnumOpenStage} open_stage
             * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_Classic
             * @instance
             */
            ShowdownStateInfo_Classic.prototype.open_stage = 0;

            /**
             * ShowdownStateInfo_Classic win_div_list.
             * @member {Array.<aslivecasino.Bac_EnumDivType>} win_div_list
             * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_Classic
             * @instance
             */
            ShowdownStateInfo_Classic.prototype.win_div_list = $util.emptyArray;

            /**
             * ShowdownStateInfo_Classic player_point.
             * @member {number} player_point
             * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_Classic
             * @instance
             */
            ShowdownStateInfo_Classic.prototype.player_point = 0;

            /**
             * ShowdownStateInfo_Classic banker_point.
             * @member {number} banker_point
             * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_Classic
             * @instance
             */
            ShowdownStateInfo_Classic.prototype.banker_point = 0;

            /**
             * Creates a new ShowdownStateInfo_Classic instance using the specified properties.
             * @function create
             * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_Classic
             * @static
             * @param {aslivecasino.Bac_DetailRoomState.IShowdownStateInfo_Classic=} [properties] Properties to set
             * @returns {aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_Classic} ShowdownStateInfo_Classic instance
             */
            ShowdownStateInfo_Classic.create = function create(properties) {
                return new ShowdownStateInfo_Classic(properties);
            };

            /**
             * Encodes the specified ShowdownStateInfo_Classic message. Does not implicitly {@link aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_Classic.verify|verify} messages.
             * @function encode
             * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_Classic
             * @static
             * @param {aslivecasino.Bac_DetailRoomState.IShowdownStateInfo_Classic} message ShowdownStateInfo_Classic message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            ShowdownStateInfo_Classic.encode = function encode(message, writer) {
                if (!writer)
                    writer = $Writer.create();
                if (message.player_card_list != null && message.player_card_list.length)
                    for (var i = 0; i < message.player_card_list.length; ++i)
                        $root.aslivecasino.CardData.encode(message.player_card_list[i], writer.uint32(/* id 1, wireType 2 =*/10).fork()).ldelim();
                if (message.banker_card_list != null && message.banker_card_list.length)
                    for (var i = 0; i < message.banker_card_list.length; ++i)
                        $root.aslivecasino.CardData.encode(message.banker_card_list[i], writer.uint32(/* id 2, wireType 2 =*/18).fork()).ldelim();
                if (message.showdown_count_down != null && message.hasOwnProperty("showdown_count_down"))
                    writer.uint32(/* id 3, wireType 0 =*/24).int32(message.showdown_count_down);
                if (message.open_stage != null && message.hasOwnProperty("open_stage"))
                    writer.uint32(/* id 4, wireType 0 =*/32).int32(message.open_stage);
                if (message.win_div_list != null && message.win_div_list.length) {
                    writer.uint32(/* id 5, wireType 2 =*/42).fork();
                    for (var i = 0; i < message.win_div_list.length; ++i)
                        writer.int32(message.win_div_list[i]);
                    writer.ldelim();
                }
                if (message.player_point != null && message.hasOwnProperty("player_point"))
                    writer.uint32(/* id 6, wireType 0 =*/48).int32(message.player_point);
                if (message.banker_point != null && message.hasOwnProperty("banker_point"))
                    writer.uint32(/* id 7, wireType 0 =*/56).int32(message.banker_point);
                return writer;
            };

            /**
             * Encodes the specified ShowdownStateInfo_Classic message, length delimited. Does not implicitly {@link aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_Classic.verify|verify} messages.
             * @function encodeDelimited
             * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_Classic
             * @static
             * @param {aslivecasino.Bac_DetailRoomState.IShowdownStateInfo_Classic} message ShowdownStateInfo_Classic message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            ShowdownStateInfo_Classic.encodeDelimited = function encodeDelimited(message, writer) {
                return this.encode(message, writer).ldelim();
            };

            /**
             * Decodes a ShowdownStateInfo_Classic message from the specified reader or buffer.
             * @function decode
             * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_Classic
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @param {number} [length] Message length if known beforehand
             * @returns {aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_Classic} ShowdownStateInfo_Classic
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            ShowdownStateInfo_Classic.decode = function decode(reader, length) {
                if (!(reader instanceof $Reader))
                    reader = $Reader.create(reader);
                var end = length === undefined ? reader.len : reader.pos + length, message = new $root.aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_Classic();
                while (reader.pos < end) {
                    var tag = reader.uint32();
                    switch (tag >>> 3) {
                    case 1:
                        if (!(message.player_card_list && message.player_card_list.length))
                            message.player_card_list = [];
                        message.player_card_list.push($root.aslivecasino.CardData.decode(reader, reader.uint32()));
                        break;
                    case 2:
                        if (!(message.banker_card_list && message.banker_card_list.length))
                            message.banker_card_list = [];
                        message.banker_card_list.push($root.aslivecasino.CardData.decode(reader, reader.uint32()));
                        break;
                    case 3:
                        message.showdown_count_down = reader.int32();
                        break;
                    case 4:
                        message.open_stage = reader.int32();
                        break;
                    case 5:
                        if (!(message.win_div_list && message.win_div_list.length))
                            message.win_div_list = [];
                        if ((tag & 7) === 2) {
                            var end2 = reader.uint32() + reader.pos;
                            while (reader.pos < end2)
                                message.win_div_list.push(reader.int32());
                        } else
                            message.win_div_list.push(reader.int32());
                        break;
                    case 6:
                        message.player_point = reader.int32();
                        break;
                    case 7:
                        message.banker_point = reader.int32();
                        break;
                    default:
                        reader.skipType(tag & 7);
                        break;
                    }
                }
                return message;
            };

            /**
             * Decodes a ShowdownStateInfo_Classic message from the specified reader or buffer, length delimited.
             * @function decodeDelimited
             * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_Classic
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @returns {aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_Classic} ShowdownStateInfo_Classic
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            ShowdownStateInfo_Classic.decodeDelimited = function decodeDelimited(reader) {
                if (!(reader instanceof $Reader))
                    reader = new $Reader(reader);
                return this.decode(reader, reader.uint32());
            };

            /**
             * Verifies a ShowdownStateInfo_Classic message.
             * @function verify
             * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_Classic
             * @static
             * @param {Object.<string,*>} message Plain object to verify
             * @returns {string|null} `null` if valid, otherwise the reason why it is not
             */
            ShowdownStateInfo_Classic.verify = function verify(message) {
                if (typeof message !== "object" || message === null)
                    return "object expected";
                if (message.player_card_list != null && message.hasOwnProperty("player_card_list")) {
                    if (!Array.isArray(message.player_card_list))
                        return "player_card_list: array expected";
                    for (var i = 0; i < message.player_card_list.length; ++i) {
                        var error = $root.aslivecasino.CardData.verify(message.player_card_list[i]);
                        if (error)
                            return "player_card_list." + error;
                    }
                }
                if (message.banker_card_list != null && message.hasOwnProperty("banker_card_list")) {
                    if (!Array.isArray(message.banker_card_list))
                        return "banker_card_list: array expected";
                    for (var i = 0; i < message.banker_card_list.length; ++i) {
                        var error = $root.aslivecasino.CardData.verify(message.banker_card_list[i]);
                        if (error)
                            return "banker_card_list." + error;
                    }
                }
                if (message.showdown_count_down != null && message.hasOwnProperty("showdown_count_down"))
                    if (!$util.isInteger(message.showdown_count_down))
                        return "showdown_count_down: integer expected";
                if (message.open_stage != null && message.hasOwnProperty("open_stage"))
                    switch (message.open_stage) {
                    default:
                        return "open_stage: enum value expected";
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                    case 10:
                    case 11:
                        break;
                    }
                if (message.win_div_list != null && message.hasOwnProperty("win_div_list")) {
                    if (!Array.isArray(message.win_div_list))
                        return "win_div_list: array expected";
                    for (var i = 0; i < message.win_div_list.length; ++i)
                        switch (message.win_div_list[i]) {
                        default:
                            return "win_div_list: enum value[] expected";
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                            break;
                        }
                }
                if (message.player_point != null && message.hasOwnProperty("player_point"))
                    if (!$util.isInteger(message.player_point))
                        return "player_point: integer expected";
                if (message.banker_point != null && message.hasOwnProperty("banker_point"))
                    if (!$util.isInteger(message.banker_point))
                        return "banker_point: integer expected";
                return null;
            };

            /**
             * Creates a ShowdownStateInfo_Classic message from a plain object. Also converts values to their respective internal types.
             * @function fromObject
             * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_Classic
             * @static
             * @param {Object.<string,*>} object Plain object
             * @returns {aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_Classic} ShowdownStateInfo_Classic
             */
            ShowdownStateInfo_Classic.fromObject = function fromObject(object) {
                if (object instanceof $root.aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_Classic)
                    return object;
                var message = new $root.aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_Classic();
                if (object.player_card_list) {
                    if (!Array.isArray(object.player_card_list))
                        throw TypeError(".aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_Classic.player_card_list: array expected");
                    message.player_card_list = [];
                    for (var i = 0; i < object.player_card_list.length; ++i) {
                        if (typeof object.player_card_list[i] !== "object")
                            throw TypeError(".aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_Classic.player_card_list: object expected");
                        message.player_card_list[i] = $root.aslivecasino.CardData.fromObject(object.player_card_list[i]);
                    }
                }
                if (object.banker_card_list) {
                    if (!Array.isArray(object.banker_card_list))
                        throw TypeError(".aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_Classic.banker_card_list: array expected");
                    message.banker_card_list = [];
                    for (var i = 0; i < object.banker_card_list.length; ++i) {
                        if (typeof object.banker_card_list[i] !== "object")
                            throw TypeError(".aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_Classic.banker_card_list: object expected");
                        message.banker_card_list[i] = $root.aslivecasino.CardData.fromObject(object.banker_card_list[i]);
                    }
                }
                if (object.showdown_count_down != null)
                    message.showdown_count_down = object.showdown_count_down | 0;
                switch (object.open_stage) {
                case "START":
                case 0:
                    message.open_stage = 0;
                    break;
                case "WAIT_PLAYEROPEN":
                case 1:
                    message.open_stage = 1;
                    break;
                case "PLAYEROPEN":
                case 2:
                    message.open_stage = 2;
                    break;
                case "WAIT_BANKEROPEN":
                case 3:
                    message.open_stage = 3;
                    break;
                case "BANKEROPEN":
                case 4:
                    message.open_stage = 4;
                    break;
                case "WAIT_PLAYERDRAW":
                case 5:
                    message.open_stage = 5;
                    break;
                case "PLAYERDRAW":
                case 6:
                    message.open_stage = 6;
                    break;
                case "WAIT_BANKERDRAW":
                case 7:
                    message.open_stage = 7;
                    break;
                case "BANKERDRAW":
                case 8:
                    message.open_stage = 8;
                    break;
                case "COMPUTING":
                case 9:
                    message.open_stage = 9;
                    break;
                case "WAIT_BTN_NEXTSTATE":
                case 10:
                    message.open_stage = 10;
                    break;
                case "NEXTMAINSTATE":
                case 11:
                    message.open_stage = 11;
                    break;
                }
                if (object.win_div_list) {
                    if (!Array.isArray(object.win_div_list))
                        throw TypeError(".aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_Classic.win_div_list: array expected");
                    message.win_div_list = [];
                    for (var i = 0; i < object.win_div_list.length; ++i)
                        switch (object.win_div_list[i]) {
                        default:
                        case "PLAYER":
                        case 0:
                            message.win_div_list[i] = 0;
                            break;
                        case "BANKER":
                        case 1:
                            message.win_div_list[i] = 1;
                            break;
                        case "TIE":
                        case 2:
                            message.win_div_list[i] = 2;
                            break;
                        case "PPAIR":
                        case 3:
                            message.win_div_list[i] = 3;
                            break;
                        case "BPAIR":
                        case 4:
                            message.win_div_list[i] = 4;
                            break;
                        }
                }
                if (object.player_point != null)
                    message.player_point = object.player_point | 0;
                if (object.banker_point != null)
                    message.banker_point = object.banker_point | 0;
                return message;
            };

            /**
             * Creates a plain object from a ShowdownStateInfo_Classic message. Also converts values to other types if specified.
             * @function toObject
             * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_Classic
             * @static
             * @param {aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_Classic} message ShowdownStateInfo_Classic
             * @param {$protobuf.IConversionOptions} [options] Conversion options
             * @returns {Object.<string,*>} Plain object
             */
            ShowdownStateInfo_Classic.toObject = function toObject(message, options) {
                if (!options)
                    options = {};
                var object = {};
                if (options.arrays || options.defaults) {
                    object.player_card_list = [];
                    object.banker_card_list = [];
                    object.win_div_list = [];
                }
                if (options.defaults) {
                    object.showdown_count_down = 0;
                    object.open_stage = options.enums === String ? "START" : 0;
                    object.player_point = 0;
                    object.banker_point = 0;
                }
                if (message.player_card_list && message.player_card_list.length) {
                    object.player_card_list = [];
                    for (var j = 0; j < message.player_card_list.length; ++j)
                        object.player_card_list[j] = $root.aslivecasino.CardData.toObject(message.player_card_list[j], options);
                }
                if (message.banker_card_list && message.banker_card_list.length) {
                    object.banker_card_list = [];
                    for (var j = 0; j < message.banker_card_list.length; ++j)
                        object.banker_card_list[j] = $root.aslivecasino.CardData.toObject(message.banker_card_list[j], options);
                }
                if (message.showdown_count_down != null && message.hasOwnProperty("showdown_count_down"))
                    object.showdown_count_down = message.showdown_count_down;
                if (message.open_stage != null && message.hasOwnProperty("open_stage"))
                    object.open_stage = options.enums === String ? $root.aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_Classic.EnumOpenStage[message.open_stage] : message.open_stage;
                if (message.win_div_list && message.win_div_list.length) {
                    object.win_div_list = [];
                    for (var j = 0; j < message.win_div_list.length; ++j)
                        object.win_div_list[j] = options.enums === String ? $root.aslivecasino.Bac_EnumDivType[message.win_div_list[j]] : message.win_div_list[j];
                }
                if (message.player_point != null && message.hasOwnProperty("player_point"))
                    object.player_point = message.player_point;
                if (message.banker_point != null && message.hasOwnProperty("banker_point"))
                    object.banker_point = message.banker_point;
                return object;
            };

            /**
             * Converts this ShowdownStateInfo_Classic to JSON.
             * @function toJSON
             * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_Classic
             * @instance
             * @returns {Object.<string,*>} JSON object
             */
            ShowdownStateInfo_Classic.prototype.toJSON = function toJSON() {
                return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
            };

            /**
             * EnumOpenStage enum.
             * @name aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_Classic.EnumOpenStage
             * @enum {string}
             * @property {number} START=0 START value
             * @property {number} WAIT_PLAYEROPEN=1 WAIT_PLAYEROPEN value
             * @property {number} PLAYEROPEN=2 PLAYEROPEN value
             * @property {number} WAIT_BANKEROPEN=3 WAIT_BANKEROPEN value
             * @property {number} BANKEROPEN=4 BANKEROPEN value
             * @property {number} WAIT_PLAYERDRAW=5 WAIT_PLAYERDRAW value
             * @property {number} PLAYERDRAW=6 PLAYERDRAW value
             * @property {number} WAIT_BANKERDRAW=7 WAIT_BANKERDRAW value
             * @property {number} BANKERDRAW=8 BANKERDRAW value
             * @property {number} COMPUTING=9 COMPUTING value
             * @property {number} WAIT_BTN_NEXTSTATE=10 WAIT_BTN_NEXTSTATE value
             * @property {number} NEXTMAINSTATE=11 NEXTMAINSTATE value
             */
            ShowdownStateInfo_Classic.EnumOpenStage = (function() {
                var valuesById = {}, values = Object.create(valuesById);
                values[valuesById[0] = "START"] = 0;
                values[valuesById[1] = "WAIT_PLAYEROPEN"] = 1;
                values[valuesById[2] = "PLAYEROPEN"] = 2;
                values[valuesById[3] = "WAIT_BANKEROPEN"] = 3;
                values[valuesById[4] = "BANKEROPEN"] = 4;
                values[valuesById[5] = "WAIT_PLAYERDRAW"] = 5;
                values[valuesById[6] = "PLAYERDRAW"] = 6;
                values[valuesById[7] = "WAIT_BANKERDRAW"] = 7;
                values[valuesById[8] = "BANKERDRAW"] = 8;
                values[valuesById[9] = "COMPUTING"] = 9;
                values[valuesById[10] = "WAIT_BTN_NEXTSTATE"] = 10;
                values[valuesById[11] = "NEXTMAINSTATE"] = 11;
                return values;
            })();

            return ShowdownStateInfo_Classic;
        })();

        Bac_DetailRoomState.ShowdownStateInfo_MeCard = (function() {

            /**
             * Properties of a ShowdownStateInfo_MeCard.
             * @memberof aslivecasino.Bac_DetailRoomState
             * @interface IShowdownStateInfo_MeCard
             * @property {Array.<aslivecasino.ICardData>|null} [player_card_list] ShowdownStateInfo_MeCard player_card_list
             * @property {Array.<aslivecasino.ICardData>|null} [banker_card_list] ShowdownStateInfo_MeCard banker_card_list
             * @property {number|null} [showdown_count_down] ShowdownStateInfo_MeCard showdown_count_down
             * @property {aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.EnumMeCardOpenStage|null} [open_stage] ShowdownStateInfo_MeCard open_stage
             * @property {Array.<aslivecasino.Bac_EnumDivType>|null} [win_div_list] ShowdownStateInfo_MeCard win_div_list
             * @property {number|null} [player_point] ShowdownStateInfo_MeCard player_point
             * @property {number|null} [banker_point] ShowdownStateInfo_MeCard banker_point
             * @property {boolean|null} [player_draw] ShowdownStateInfo_MeCard player_draw
             * @property {boolean|null} [banker_draw] ShowdownStateInfo_MeCard banker_draw
             * @property {Array.<aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.IFlipCard>|null} [player_flip_list] ShowdownStateInfo_MeCard player_flip_list
             * @property {Array.<aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.IFlipCard>|null} [banker_flip_list] ShowdownStateInfo_MeCard banker_flip_list
             * @property {number|null} [me_player_seat_no] ShowdownStateInfo_MeCard me_player_seat_no
             * @property {number|null} [me_banker_seat_no] ShowdownStateInfo_MeCard me_banker_seat_no
             * @property {string|null} [me_player_user_name] ShowdownStateInfo_MeCard me_player_user_name
             * @property {string|null} [me_banker_user_name] ShowdownStateInfo_MeCard me_banker_user_name
             */

            /**
             * Constructs a new ShowdownStateInfo_MeCard.
             * @memberof aslivecasino.Bac_DetailRoomState
             * @classdesc Represents a ShowdownStateInfo_MeCard.
             * @implements IShowdownStateInfo_MeCard
             * @constructor
             * @param {aslivecasino.Bac_DetailRoomState.IShowdownStateInfo_MeCard=} [properties] Properties to set
             */
            function ShowdownStateInfo_MeCard(properties) {
                this.player_card_list = [];
                this.banker_card_list = [];
                this.win_div_list = [];
                this.player_flip_list = [];
                this.banker_flip_list = [];
                if (properties)
                    for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                        if (properties[keys[i]] != null)
                            this[keys[i]] = properties[keys[i]];
            }

            /**
             * ShowdownStateInfo_MeCard player_card_list.
             * @member {Array.<aslivecasino.ICardData>} player_card_list
             * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard
             * @instance
             */
            ShowdownStateInfo_MeCard.prototype.player_card_list = $util.emptyArray;

            /**
             * ShowdownStateInfo_MeCard banker_card_list.
             * @member {Array.<aslivecasino.ICardData>} banker_card_list
             * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard
             * @instance
             */
            ShowdownStateInfo_MeCard.prototype.banker_card_list = $util.emptyArray;

            /**
             * ShowdownStateInfo_MeCard showdown_count_down.
             * @member {number} showdown_count_down
             * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard
             * @instance
             */
            ShowdownStateInfo_MeCard.prototype.showdown_count_down = 0;

            /**
             * ShowdownStateInfo_MeCard open_stage.
             * @member {aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.EnumMeCardOpenStage} open_stage
             * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard
             * @instance
             */
            ShowdownStateInfo_MeCard.prototype.open_stage = 0;

            /**
             * ShowdownStateInfo_MeCard win_div_list.
             * @member {Array.<aslivecasino.Bac_EnumDivType>} win_div_list
             * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard
             * @instance
             */
            ShowdownStateInfo_MeCard.prototype.win_div_list = $util.emptyArray;

            /**
             * ShowdownStateInfo_MeCard player_point.
             * @member {number} player_point
             * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard
             * @instance
             */
            ShowdownStateInfo_MeCard.prototype.player_point = 0;

            /**
             * ShowdownStateInfo_MeCard banker_point.
             * @member {number} banker_point
             * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard
             * @instance
             */
            ShowdownStateInfo_MeCard.prototype.banker_point = 0;

            /**
             * ShowdownStateInfo_MeCard player_draw.
             * @member {boolean} player_draw
             * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard
             * @instance
             */
            ShowdownStateInfo_MeCard.prototype.player_draw = false;

            /**
             * ShowdownStateInfo_MeCard banker_draw.
             * @member {boolean} banker_draw
             * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard
             * @instance
             */
            ShowdownStateInfo_MeCard.prototype.banker_draw = false;

            /**
             * ShowdownStateInfo_MeCard player_flip_list.
             * @member {Array.<aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.IFlipCard>} player_flip_list
             * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard
             * @instance
             */
            ShowdownStateInfo_MeCard.prototype.player_flip_list = $util.emptyArray;

            /**
             * ShowdownStateInfo_MeCard banker_flip_list.
             * @member {Array.<aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.IFlipCard>} banker_flip_list
             * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard
             * @instance
             */
            ShowdownStateInfo_MeCard.prototype.banker_flip_list = $util.emptyArray;

            /**
             * ShowdownStateInfo_MeCard me_player_seat_no.
             * @member {number} me_player_seat_no
             * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard
             * @instance
             */
            ShowdownStateInfo_MeCard.prototype.me_player_seat_no = 0;

            /**
             * ShowdownStateInfo_MeCard me_banker_seat_no.
             * @member {number} me_banker_seat_no
             * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard
             * @instance
             */
            ShowdownStateInfo_MeCard.prototype.me_banker_seat_no = 0;

            /**
             * ShowdownStateInfo_MeCard me_player_user_name.
             * @member {string} me_player_user_name
             * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard
             * @instance
             */
            ShowdownStateInfo_MeCard.prototype.me_player_user_name = "";

            /**
             * ShowdownStateInfo_MeCard me_banker_user_name.
             * @member {string} me_banker_user_name
             * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard
             * @instance
             */
            ShowdownStateInfo_MeCard.prototype.me_banker_user_name = "";

            /**
             * Creates a new ShowdownStateInfo_MeCard instance using the specified properties.
             * @function create
             * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard
             * @static
             * @param {aslivecasino.Bac_DetailRoomState.IShowdownStateInfo_MeCard=} [properties] Properties to set
             * @returns {aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard} ShowdownStateInfo_MeCard instance
             */
            ShowdownStateInfo_MeCard.create = function create(properties) {
                return new ShowdownStateInfo_MeCard(properties);
            };

            /**
             * Encodes the specified ShowdownStateInfo_MeCard message. Does not implicitly {@link aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.verify|verify} messages.
             * @function encode
             * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard
             * @static
             * @param {aslivecasino.Bac_DetailRoomState.IShowdownStateInfo_MeCard} message ShowdownStateInfo_MeCard message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            ShowdownStateInfo_MeCard.encode = function encode(message, writer) {
                if (!writer)
                    writer = $Writer.create();
                if (message.player_card_list != null && message.player_card_list.length)
                    for (var i = 0; i < message.player_card_list.length; ++i)
                        $root.aslivecasino.CardData.encode(message.player_card_list[i], writer.uint32(/* id 1, wireType 2 =*/10).fork()).ldelim();
                if (message.banker_card_list != null && message.banker_card_list.length)
                    for (var i = 0; i < message.banker_card_list.length; ++i)
                        $root.aslivecasino.CardData.encode(message.banker_card_list[i], writer.uint32(/* id 2, wireType 2 =*/18).fork()).ldelim();
                if (message.showdown_count_down != null && message.hasOwnProperty("showdown_count_down"))
                    writer.uint32(/* id 3, wireType 0 =*/24).int32(message.showdown_count_down);
                if (message.open_stage != null && message.hasOwnProperty("open_stage"))
                    writer.uint32(/* id 4, wireType 0 =*/32).int32(message.open_stage);
                if (message.win_div_list != null && message.win_div_list.length) {
                    writer.uint32(/* id 5, wireType 2 =*/42).fork();
                    for (var i = 0; i < message.win_div_list.length; ++i)
                        writer.int32(message.win_div_list[i]);
                    writer.ldelim();
                }
                if (message.player_point != null && message.hasOwnProperty("player_point"))
                    writer.uint32(/* id 6, wireType 0 =*/48).int32(message.player_point);
                if (message.banker_point != null && message.hasOwnProperty("banker_point"))
                    writer.uint32(/* id 7, wireType 0 =*/56).int32(message.banker_point);
                if (message.player_draw != null && message.hasOwnProperty("player_draw"))
                    writer.uint32(/* id 8, wireType 0 =*/64).bool(message.player_draw);
                if (message.banker_draw != null && message.hasOwnProperty("banker_draw"))
                    writer.uint32(/* id 9, wireType 0 =*/72).bool(message.banker_draw);
                if (message.player_flip_list != null && message.player_flip_list.length)
                    for (var i = 0; i < message.player_flip_list.length; ++i)
                        $root.aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.FlipCard.encode(message.player_flip_list[i], writer.uint32(/* id 10, wireType 2 =*/82).fork()).ldelim();
                if (message.banker_flip_list != null && message.banker_flip_list.length)
                    for (var i = 0; i < message.banker_flip_list.length; ++i)
                        $root.aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.FlipCard.encode(message.banker_flip_list[i], writer.uint32(/* id 11, wireType 2 =*/90).fork()).ldelim();
                if (message.me_player_seat_no != null && message.hasOwnProperty("me_player_seat_no"))
                    writer.uint32(/* id 12, wireType 0 =*/96).sint32(message.me_player_seat_no);
                if (message.me_banker_seat_no != null && message.hasOwnProperty("me_banker_seat_no"))
                    writer.uint32(/* id 13, wireType 0 =*/104).sint32(message.me_banker_seat_no);
                if (message.me_player_user_name != null && message.hasOwnProperty("me_player_user_name"))
                    writer.uint32(/* id 14, wireType 2 =*/114).string(message.me_player_user_name);
                if (message.me_banker_user_name != null && message.hasOwnProperty("me_banker_user_name"))
                    writer.uint32(/* id 15, wireType 2 =*/122).string(message.me_banker_user_name);
                return writer;
            };

            /**
             * Encodes the specified ShowdownStateInfo_MeCard message, length delimited. Does not implicitly {@link aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.verify|verify} messages.
             * @function encodeDelimited
             * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard
             * @static
             * @param {aslivecasino.Bac_DetailRoomState.IShowdownStateInfo_MeCard} message ShowdownStateInfo_MeCard message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            ShowdownStateInfo_MeCard.encodeDelimited = function encodeDelimited(message, writer) {
                return this.encode(message, writer).ldelim();
            };

            /**
             * Decodes a ShowdownStateInfo_MeCard message from the specified reader or buffer.
             * @function decode
             * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @param {number} [length] Message length if known beforehand
             * @returns {aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard} ShowdownStateInfo_MeCard
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            ShowdownStateInfo_MeCard.decode = function decode(reader, length) {
                if (!(reader instanceof $Reader))
                    reader = $Reader.create(reader);
                var end = length === undefined ? reader.len : reader.pos + length, message = new $root.aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard();
                while (reader.pos < end) {
                    var tag = reader.uint32();
                    switch (tag >>> 3) {
                    case 1:
                        if (!(message.player_card_list && message.player_card_list.length))
                            message.player_card_list = [];
                        message.player_card_list.push($root.aslivecasino.CardData.decode(reader, reader.uint32()));
                        break;
                    case 2:
                        if (!(message.banker_card_list && message.banker_card_list.length))
                            message.banker_card_list = [];
                        message.banker_card_list.push($root.aslivecasino.CardData.decode(reader, reader.uint32()));
                        break;
                    case 3:
                        message.showdown_count_down = reader.int32();
                        break;
                    case 4:
                        message.open_stage = reader.int32();
                        break;
                    case 5:
                        if (!(message.win_div_list && message.win_div_list.length))
                            message.win_div_list = [];
                        if ((tag & 7) === 2) {
                            var end2 = reader.uint32() + reader.pos;
                            while (reader.pos < end2)
                                message.win_div_list.push(reader.int32());
                        } else
                            message.win_div_list.push(reader.int32());
                        break;
                    case 6:
                        message.player_point = reader.int32();
                        break;
                    case 7:
                        message.banker_point = reader.int32();
                        break;
                    case 8:
                        message.player_draw = reader.bool();
                        break;
                    case 9:
                        message.banker_draw = reader.bool();
                        break;
                    case 10:
                        if (!(message.player_flip_list && message.player_flip_list.length))
                            message.player_flip_list = [];
                        message.player_flip_list.push($root.aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.FlipCard.decode(reader, reader.uint32()));
                        break;
                    case 11:
                        if (!(message.banker_flip_list && message.banker_flip_list.length))
                            message.banker_flip_list = [];
                        message.banker_flip_list.push($root.aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.FlipCard.decode(reader, reader.uint32()));
                        break;
                    case 12:
                        message.me_player_seat_no = reader.sint32();
                        break;
                    case 13:
                        message.me_banker_seat_no = reader.sint32();
                        break;
                    case 14:
                        message.me_player_user_name = reader.string();
                        break;
                    case 15:
                        message.me_banker_user_name = reader.string();
                        break;
                    default:
                        reader.skipType(tag & 7);
                        break;
                    }
                }
                return message;
            };

            /**
             * Decodes a ShowdownStateInfo_MeCard message from the specified reader or buffer, length delimited.
             * @function decodeDelimited
             * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @returns {aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard} ShowdownStateInfo_MeCard
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            ShowdownStateInfo_MeCard.decodeDelimited = function decodeDelimited(reader) {
                if (!(reader instanceof $Reader))
                    reader = new $Reader(reader);
                return this.decode(reader, reader.uint32());
            };

            /**
             * Verifies a ShowdownStateInfo_MeCard message.
             * @function verify
             * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard
             * @static
             * @param {Object.<string,*>} message Plain object to verify
             * @returns {string|null} `null` if valid, otherwise the reason why it is not
             */
            ShowdownStateInfo_MeCard.verify = function verify(message) {
                if (typeof message !== "object" || message === null)
                    return "object expected";
                if (message.player_card_list != null && message.hasOwnProperty("player_card_list")) {
                    if (!Array.isArray(message.player_card_list))
                        return "player_card_list: array expected";
                    for (var i = 0; i < message.player_card_list.length; ++i) {
                        var error = $root.aslivecasino.CardData.verify(message.player_card_list[i]);
                        if (error)
                            return "player_card_list." + error;
                    }
                }
                if (message.banker_card_list != null && message.hasOwnProperty("banker_card_list")) {
                    if (!Array.isArray(message.banker_card_list))
                        return "banker_card_list: array expected";
                    for (var i = 0; i < message.banker_card_list.length; ++i) {
                        var error = $root.aslivecasino.CardData.verify(message.banker_card_list[i]);
                        if (error)
                            return "banker_card_list." + error;
                    }
                }
                if (message.showdown_count_down != null && message.hasOwnProperty("showdown_count_down"))
                    if (!$util.isInteger(message.showdown_count_down))
                        return "showdown_count_down: integer expected";
                if (message.open_stage != null && message.hasOwnProperty("open_stage"))
                    switch (message.open_stage) {
                    default:
                        return "open_stage: enum value expected";
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                    case 10:
                    case 11:
                    case 12:
                    case 13:
                        break;
                    }
                if (message.win_div_list != null && message.hasOwnProperty("win_div_list")) {
                    if (!Array.isArray(message.win_div_list))
                        return "win_div_list: array expected";
                    for (var i = 0; i < message.win_div_list.length; ++i)
                        switch (message.win_div_list[i]) {
                        default:
                            return "win_div_list: enum value[] expected";
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                            break;
                        }
                }
                if (message.player_point != null && message.hasOwnProperty("player_point"))
                    if (!$util.isInteger(message.player_point))
                        return "player_point: integer expected";
                if (message.banker_point != null && message.hasOwnProperty("banker_point"))
                    if (!$util.isInteger(message.banker_point))
                        return "banker_point: integer expected";
                if (message.player_draw != null && message.hasOwnProperty("player_draw"))
                    if (typeof message.player_draw !== "boolean")
                        return "player_draw: boolean expected";
                if (message.banker_draw != null && message.hasOwnProperty("banker_draw"))
                    if (typeof message.banker_draw !== "boolean")
                        return "banker_draw: boolean expected";
                if (message.player_flip_list != null && message.hasOwnProperty("player_flip_list")) {
                    if (!Array.isArray(message.player_flip_list))
                        return "player_flip_list: array expected";
                    for (var i = 0; i < message.player_flip_list.length; ++i) {
                        var error = $root.aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.FlipCard.verify(message.player_flip_list[i]);
                        if (error)
                            return "player_flip_list." + error;
                    }
                }
                if (message.banker_flip_list != null && message.hasOwnProperty("banker_flip_list")) {
                    if (!Array.isArray(message.banker_flip_list))
                        return "banker_flip_list: array expected";
                    for (var i = 0; i < message.banker_flip_list.length; ++i) {
                        var error = $root.aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.FlipCard.verify(message.banker_flip_list[i]);
                        if (error)
                            return "banker_flip_list." + error;
                    }
                }
                if (message.me_player_seat_no != null && message.hasOwnProperty("me_player_seat_no"))
                    if (!$util.isInteger(message.me_player_seat_no))
                        return "me_player_seat_no: integer expected";
                if (message.me_banker_seat_no != null && message.hasOwnProperty("me_banker_seat_no"))
                    if (!$util.isInteger(message.me_banker_seat_no))
                        return "me_banker_seat_no: integer expected";
                if (message.me_player_user_name != null && message.hasOwnProperty("me_player_user_name"))
                    if (!$util.isString(message.me_player_user_name))
                        return "me_player_user_name: string expected";
                if (message.me_banker_user_name != null && message.hasOwnProperty("me_banker_user_name"))
                    if (!$util.isString(message.me_banker_user_name))
                        return "me_banker_user_name: string expected";
                return null;
            };

            /**
             * Creates a ShowdownStateInfo_MeCard message from a plain object. Also converts values to their respective internal types.
             * @function fromObject
             * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard
             * @static
             * @param {Object.<string,*>} object Plain object
             * @returns {aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard} ShowdownStateInfo_MeCard
             */
            ShowdownStateInfo_MeCard.fromObject = function fromObject(object) {
                if (object instanceof $root.aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard)
                    return object;
                var message = new $root.aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard();
                if (object.player_card_list) {
                    if (!Array.isArray(object.player_card_list))
                        throw TypeError(".aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.player_card_list: array expected");
                    message.player_card_list = [];
                    for (var i = 0; i < object.player_card_list.length; ++i) {
                        if (typeof object.player_card_list[i] !== "object")
                            throw TypeError(".aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.player_card_list: object expected");
                        message.player_card_list[i] = $root.aslivecasino.CardData.fromObject(object.player_card_list[i]);
                    }
                }
                if (object.banker_card_list) {
                    if (!Array.isArray(object.banker_card_list))
                        throw TypeError(".aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.banker_card_list: array expected");
                    message.banker_card_list = [];
                    for (var i = 0; i < object.banker_card_list.length; ++i) {
                        if (typeof object.banker_card_list[i] !== "object")
                            throw TypeError(".aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.banker_card_list: object expected");
                        message.banker_card_list[i] = $root.aslivecasino.CardData.fromObject(object.banker_card_list[i]);
                    }
                }
                if (object.showdown_count_down != null)
                    message.showdown_count_down = object.showdown_count_down | 0;
                switch (object.open_stage) {
                case "START":
                case 0:
                    message.open_stage = 0;
                    break;
                case "WAIT_PLAYEROPEN":
                case 1:
                    message.open_stage = 1;
                    break;
                case "PLAYEROPEN":
                case 2:
                    message.open_stage = 2;
                    break;
                case "WAIT_BANKEROPEN":
                case 3:
                    message.open_stage = 3;
                    break;
                case "BANKEROPEN":
                case 4:
                    message.open_stage = 4;
                    break;
                case "MECARD_STAGE_0":
                case 5:
                    message.open_stage = 5;
                    break;
                case "WAIT_PLAYERDRAW":
                case 6:
                    message.open_stage = 6;
                    break;
                case "PLAYERDRAW":
                case 7:
                    message.open_stage = 7;
                    break;
                case "WAIT_BANKERDRAW":
                case 8:
                    message.open_stage = 8;
                    break;
                case "BANKERDRAW":
                case 9:
                    message.open_stage = 9;
                    break;
                case "COMPUTING":
                case 10:
                    message.open_stage = 10;
                    break;
                case "MECARD_STAGE_1":
                case 11:
                    message.open_stage = 11;
                    break;
                case "WAIT_BTN_NEXTSTATE":
                case 12:
                    message.open_stage = 12;
                    break;
                case "NEXTMAINSTATE":
                case 13:
                    message.open_stage = 13;
                    break;
                }
                if (object.win_div_list) {
                    if (!Array.isArray(object.win_div_list))
                        throw TypeError(".aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.win_div_list: array expected");
                    message.win_div_list = [];
                    for (var i = 0; i < object.win_div_list.length; ++i)
                        switch (object.win_div_list[i]) {
                        default:
                        case "PLAYER":
                        case 0:
                            message.win_div_list[i] = 0;
                            break;
                        case "BANKER":
                        case 1:
                            message.win_div_list[i] = 1;
                            break;
                        case "TIE":
                        case 2:
                            message.win_div_list[i] = 2;
                            break;
                        case "PPAIR":
                        case 3:
                            message.win_div_list[i] = 3;
                            break;
                        case "BPAIR":
                        case 4:
                            message.win_div_list[i] = 4;
                            break;
                        }
                }
                if (object.player_point != null)
                    message.player_point = object.player_point | 0;
                if (object.banker_point != null)
                    message.banker_point = object.banker_point | 0;
                if (object.player_draw != null)
                    message.player_draw = Boolean(object.player_draw);
                if (object.banker_draw != null)
                    message.banker_draw = Boolean(object.banker_draw);
                if (object.player_flip_list) {
                    if (!Array.isArray(object.player_flip_list))
                        throw TypeError(".aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.player_flip_list: array expected");
                    message.player_flip_list = [];
                    for (var i = 0; i < object.player_flip_list.length; ++i) {
                        if (typeof object.player_flip_list[i] !== "object")
                            throw TypeError(".aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.player_flip_list: object expected");
                        message.player_flip_list[i] = $root.aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.FlipCard.fromObject(object.player_flip_list[i]);
                    }
                }
                if (object.banker_flip_list) {
                    if (!Array.isArray(object.banker_flip_list))
                        throw TypeError(".aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.banker_flip_list: array expected");
                    message.banker_flip_list = [];
                    for (var i = 0; i < object.banker_flip_list.length; ++i) {
                        if (typeof object.banker_flip_list[i] !== "object")
                            throw TypeError(".aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.banker_flip_list: object expected");
                        message.banker_flip_list[i] = $root.aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.FlipCard.fromObject(object.banker_flip_list[i]);
                    }
                }
                if (object.me_player_seat_no != null)
                    message.me_player_seat_no = object.me_player_seat_no | 0;
                if (object.me_banker_seat_no != null)
                    message.me_banker_seat_no = object.me_banker_seat_no | 0;
                if (object.me_player_user_name != null)
                    message.me_player_user_name = String(object.me_player_user_name);
                if (object.me_banker_user_name != null)
                    message.me_banker_user_name = String(object.me_banker_user_name);
                return message;
            };

            /**
             * Creates a plain object from a ShowdownStateInfo_MeCard message. Also converts values to other types if specified.
             * @function toObject
             * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard
             * @static
             * @param {aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard} message ShowdownStateInfo_MeCard
             * @param {$protobuf.IConversionOptions} [options] Conversion options
             * @returns {Object.<string,*>} Plain object
             */
            ShowdownStateInfo_MeCard.toObject = function toObject(message, options) {
                if (!options)
                    options = {};
                var object = {};
                if (options.arrays || options.defaults) {
                    object.player_card_list = [];
                    object.banker_card_list = [];
                    object.win_div_list = [];
                    object.player_flip_list = [];
                    object.banker_flip_list = [];
                }
                if (options.defaults) {
                    object.showdown_count_down = 0;
                    object.open_stage = options.enums === String ? "START" : 0;
                    object.player_point = 0;
                    object.banker_point = 0;
                    object.player_draw = false;
                    object.banker_draw = false;
                    object.me_player_seat_no = 0;
                    object.me_banker_seat_no = 0;
                    object.me_player_user_name = "";
                    object.me_banker_user_name = "";
                }
                if (message.player_card_list && message.player_card_list.length) {
                    object.player_card_list = [];
                    for (var j = 0; j < message.player_card_list.length; ++j)
                        object.player_card_list[j] = $root.aslivecasino.CardData.toObject(message.player_card_list[j], options);
                }
                if (message.banker_card_list && message.banker_card_list.length) {
                    object.banker_card_list = [];
                    for (var j = 0; j < message.banker_card_list.length; ++j)
                        object.banker_card_list[j] = $root.aslivecasino.CardData.toObject(message.banker_card_list[j], options);
                }
                if (message.showdown_count_down != null && message.hasOwnProperty("showdown_count_down"))
                    object.showdown_count_down = message.showdown_count_down;
                if (message.open_stage != null && message.hasOwnProperty("open_stage"))
                    object.open_stage = options.enums === String ? $root.aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.EnumMeCardOpenStage[message.open_stage] : message.open_stage;
                if (message.win_div_list && message.win_div_list.length) {
                    object.win_div_list = [];
                    for (var j = 0; j < message.win_div_list.length; ++j)
                        object.win_div_list[j] = options.enums === String ? $root.aslivecasino.Bac_EnumDivType[message.win_div_list[j]] : message.win_div_list[j];
                }
                if (message.player_point != null && message.hasOwnProperty("player_point"))
                    object.player_point = message.player_point;
                if (message.banker_point != null && message.hasOwnProperty("banker_point"))
                    object.banker_point = message.banker_point;
                if (message.player_draw != null && message.hasOwnProperty("player_draw"))
                    object.player_draw = message.player_draw;
                if (message.banker_draw != null && message.hasOwnProperty("banker_draw"))
                    object.banker_draw = message.banker_draw;
                if (message.player_flip_list && message.player_flip_list.length) {
                    object.player_flip_list = [];
                    for (var j = 0; j < message.player_flip_list.length; ++j)
                        object.player_flip_list[j] = $root.aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.FlipCard.toObject(message.player_flip_list[j], options);
                }
                if (message.banker_flip_list && message.banker_flip_list.length) {
                    object.banker_flip_list = [];
                    for (var j = 0; j < message.banker_flip_list.length; ++j)
                        object.banker_flip_list[j] = $root.aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.FlipCard.toObject(message.banker_flip_list[j], options);
                }
                if (message.me_player_seat_no != null && message.hasOwnProperty("me_player_seat_no"))
                    object.me_player_seat_no = message.me_player_seat_no;
                if (message.me_banker_seat_no != null && message.hasOwnProperty("me_banker_seat_no"))
                    object.me_banker_seat_no = message.me_banker_seat_no;
                if (message.me_player_user_name != null && message.hasOwnProperty("me_player_user_name"))
                    object.me_player_user_name = message.me_player_user_name;
                if (message.me_banker_user_name != null && message.hasOwnProperty("me_banker_user_name"))
                    object.me_banker_user_name = message.me_banker_user_name;
                return object;
            };

            /**
             * Converts this ShowdownStateInfo_MeCard to JSON.
             * @function toJSON
             * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard
             * @instance
             * @returns {Object.<string,*>} JSON object
             */
            ShowdownStateInfo_MeCard.prototype.toJSON = function toJSON() {
                return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
            };

            /**
             * EnumMeCardOpenStage enum.
             * @name aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.EnumMeCardOpenStage
             * @enum {string}
             * @property {number} START=0 START value
             * @property {number} WAIT_PLAYEROPEN=1 WAIT_PLAYEROPEN value
             * @property {number} PLAYEROPEN=2 PLAYEROPEN value
             * @property {number} WAIT_BANKEROPEN=3 WAIT_BANKEROPEN value
             * @property {number} BANKEROPEN=4 BANKEROPEN value
             * @property {number} MECARD_STAGE_0=5 MECARD_STAGE_0 value
             * @property {number} WAIT_PLAYERDRAW=6 WAIT_PLAYERDRAW value
             * @property {number} PLAYERDRAW=7 PLAYERDRAW value
             * @property {number} WAIT_BANKERDRAW=8 WAIT_BANKERDRAW value
             * @property {number} BANKERDRAW=9 BANKERDRAW value
             * @property {number} COMPUTING=10 COMPUTING value
             * @property {number} MECARD_STAGE_1=11 MECARD_STAGE_1 value
             * @property {number} WAIT_BTN_NEXTSTATE=12 WAIT_BTN_NEXTSTATE value
             * @property {number} NEXTMAINSTATE=13 NEXTMAINSTATE value
             */
            ShowdownStateInfo_MeCard.EnumMeCardOpenStage = (function() {
                var valuesById = {}, values = Object.create(valuesById);
                values[valuesById[0] = "START"] = 0;
                values[valuesById[1] = "WAIT_PLAYEROPEN"] = 1;
                values[valuesById[2] = "PLAYEROPEN"] = 2;
                values[valuesById[3] = "WAIT_BANKEROPEN"] = 3;
                values[valuesById[4] = "BANKEROPEN"] = 4;
                values[valuesById[5] = "MECARD_STAGE_0"] = 5;
                values[valuesById[6] = "WAIT_PLAYERDRAW"] = 6;
                values[valuesById[7] = "PLAYERDRAW"] = 7;
                values[valuesById[8] = "WAIT_BANKERDRAW"] = 8;
                values[valuesById[9] = "BANKERDRAW"] = 9;
                values[valuesById[10] = "COMPUTING"] = 10;
                values[valuesById[11] = "MECARD_STAGE_1"] = 11;
                values[valuesById[12] = "WAIT_BTN_NEXTSTATE"] = 12;
                values[valuesById[13] = "NEXTMAINSTATE"] = 13;
                return values;
            })();

            ShowdownStateInfo_MeCard.FlipCard = (function() {

                /**
                 * Properties of a FlipCard.
                 * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard
                 * @interface IFlipCard
                 * @property {number|null} [corner] FlipCard corner
                 * @property {number|null} [x] FlipCard x
                 * @property {number|null} [y] FlipCard y
                 * @property {boolean|null} [rotated] FlipCard rotated
                 */

                /**
                 * Constructs a new FlipCard.
                 * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard
                 * @classdesc Represents a FlipCard.
                 * @implements IFlipCard
                 * @constructor
                 * @param {aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.IFlipCard=} [properties] Properties to set
                 */
                function FlipCard(properties) {
                    if (properties)
                        for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                            if (properties[keys[i]] != null)
                                this[keys[i]] = properties[keys[i]];
                }

                /**
                 * FlipCard corner.
                 * @member {number} corner
                 * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.FlipCard
                 * @instance
                 */
                FlipCard.prototype.corner = 0;

                /**
                 * FlipCard x.
                 * @member {number} x
                 * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.FlipCard
                 * @instance
                 */
                FlipCard.prototype.x = 0;

                /**
                 * FlipCard y.
                 * @member {number} y
                 * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.FlipCard
                 * @instance
                 */
                FlipCard.prototype.y = 0;

                /**
                 * FlipCard rotated.
                 * @member {boolean} rotated
                 * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.FlipCard
                 * @instance
                 */
                FlipCard.prototype.rotated = false;

                /**
                 * Creates a new FlipCard instance using the specified properties.
                 * @function create
                 * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.FlipCard
                 * @static
                 * @param {aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.IFlipCard=} [properties] Properties to set
                 * @returns {aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.FlipCard} FlipCard instance
                 */
                FlipCard.create = function create(properties) {
                    return new FlipCard(properties);
                };

                /**
                 * Encodes the specified FlipCard message. Does not implicitly {@link aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.FlipCard.verify|verify} messages.
                 * @function encode
                 * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.FlipCard
                 * @static
                 * @param {aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.IFlipCard} message FlipCard message or plain object to encode
                 * @param {$protobuf.Writer} [writer] Writer to encode to
                 * @returns {$protobuf.Writer} Writer
                 */
                FlipCard.encode = function encode(message, writer) {
                    if (!writer)
                        writer = $Writer.create();
                    if (message.corner != null && message.hasOwnProperty("corner"))
                        writer.uint32(/* id 1, wireType 0 =*/8).sint32(message.corner);
                    if (message.x != null && message.hasOwnProperty("x"))
                        writer.uint32(/* id 2, wireType 5 =*/21).float(message.x);
                    if (message.y != null && message.hasOwnProperty("y"))
                        writer.uint32(/* id 3, wireType 5 =*/29).float(message.y);
                    if (message.rotated != null && message.hasOwnProperty("rotated"))
                        writer.uint32(/* id 4, wireType 0 =*/32).bool(message.rotated);
                    return writer;
                };

                /**
                 * Encodes the specified FlipCard message, length delimited. Does not implicitly {@link aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.FlipCard.verify|verify} messages.
                 * @function encodeDelimited
                 * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.FlipCard
                 * @static
                 * @param {aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.IFlipCard} message FlipCard message or plain object to encode
                 * @param {$protobuf.Writer} [writer] Writer to encode to
                 * @returns {$protobuf.Writer} Writer
                 */
                FlipCard.encodeDelimited = function encodeDelimited(message, writer) {
                    return this.encode(message, writer).ldelim();
                };

                /**
                 * Decodes a FlipCard message from the specified reader or buffer.
                 * @function decode
                 * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.FlipCard
                 * @static
                 * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                 * @param {number} [length] Message length if known beforehand
                 * @returns {aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.FlipCard} FlipCard
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                FlipCard.decode = function decode(reader, length) {
                    if (!(reader instanceof $Reader))
                        reader = $Reader.create(reader);
                    var end = length === undefined ? reader.len : reader.pos + length, message = new $root.aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.FlipCard();
                    while (reader.pos < end) {
                        var tag = reader.uint32();
                        switch (tag >>> 3) {
                        case 1:
                            message.corner = reader.sint32();
                            break;
                        case 2:
                            message.x = reader.float();
                            break;
                        case 3:
                            message.y = reader.float();
                            break;
                        case 4:
                            message.rotated = reader.bool();
                            break;
                        default:
                            reader.skipType(tag & 7);
                            break;
                        }
                    }
                    return message;
                };

                /**
                 * Decodes a FlipCard message from the specified reader or buffer, length delimited.
                 * @function decodeDelimited
                 * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.FlipCard
                 * @static
                 * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                 * @returns {aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.FlipCard} FlipCard
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                FlipCard.decodeDelimited = function decodeDelimited(reader) {
                    if (!(reader instanceof $Reader))
                        reader = new $Reader(reader);
                    return this.decode(reader, reader.uint32());
                };

                /**
                 * Verifies a FlipCard message.
                 * @function verify
                 * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.FlipCard
                 * @static
                 * @param {Object.<string,*>} message Plain object to verify
                 * @returns {string|null} `null` if valid, otherwise the reason why it is not
                 */
                FlipCard.verify = function verify(message) {
                    if (typeof message !== "object" || message === null)
                        return "object expected";
                    if (message.corner != null && message.hasOwnProperty("corner"))
                        if (!$util.isInteger(message.corner))
                            return "corner: integer expected";
                    if (message.x != null && message.hasOwnProperty("x"))
                        if (typeof message.x !== "number")
                            return "x: number expected";
                    if (message.y != null && message.hasOwnProperty("y"))
                        if (typeof message.y !== "number")
                            return "y: number expected";
                    if (message.rotated != null && message.hasOwnProperty("rotated"))
                        if (typeof message.rotated !== "boolean")
                            return "rotated: boolean expected";
                    return null;
                };

                /**
                 * Creates a FlipCard message from a plain object. Also converts values to their respective internal types.
                 * @function fromObject
                 * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.FlipCard
                 * @static
                 * @param {Object.<string,*>} object Plain object
                 * @returns {aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.FlipCard} FlipCard
                 */
                FlipCard.fromObject = function fromObject(object) {
                    if (object instanceof $root.aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.FlipCard)
                        return object;
                    var message = new $root.aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.FlipCard();
                    if (object.corner != null)
                        message.corner = object.corner | 0;
                    if (object.x != null)
                        message.x = Number(object.x);
                    if (object.y != null)
                        message.y = Number(object.y);
                    if (object.rotated != null)
                        message.rotated = Boolean(object.rotated);
                    return message;
                };

                /**
                 * Creates a plain object from a FlipCard message. Also converts values to other types if specified.
                 * @function toObject
                 * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.FlipCard
                 * @static
                 * @param {aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.FlipCard} message FlipCard
                 * @param {$protobuf.IConversionOptions} [options] Conversion options
                 * @returns {Object.<string,*>} Plain object
                 */
                FlipCard.toObject = function toObject(message, options) {
                    if (!options)
                        options = {};
                    var object = {};
                    if (options.defaults) {
                        object.corner = 0;
                        object.x = 0;
                        object.y = 0;
                        object.rotated = false;
                    }
                    if (message.corner != null && message.hasOwnProperty("corner"))
                        object.corner = message.corner;
                    if (message.x != null && message.hasOwnProperty("x"))
                        object.x = options.json && !isFinite(message.x) ? String(message.x) : message.x;
                    if (message.y != null && message.hasOwnProperty("y"))
                        object.y = options.json && !isFinite(message.y) ? String(message.y) : message.y;
                    if (message.rotated != null && message.hasOwnProperty("rotated"))
                        object.rotated = message.rotated;
                    return object;
                };

                /**
                 * Converts this FlipCard to JSON.
                 * @function toJSON
                 * @memberof aslivecasino.Bac_DetailRoomState.ShowdownStateInfo_MeCard.FlipCard
                 * @instance
                 * @returns {Object.<string,*>} JSON object
                 */
                FlipCard.prototype.toJSON = function toJSON() {
                    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
                };

                return FlipCard;
            })();

            return ShowdownStateInfo_MeCard;
        })();

        Bac_DetailRoomState.SettlementStateInfo = (function() {

            /**
             * Properties of a SettlementStateInfo.
             * @memberof aslivecasino.Bac_DetailRoomState
             * @interface ISettlementStateInfo
             * @property {Array.<aslivecasino.Bac_EnumDivType>|null} [win_div_list] SettlementStateInfo win_div_list
             */

            /**
             * Constructs a new SettlementStateInfo.
             * @memberof aslivecasino.Bac_DetailRoomState
             * @classdesc Represents a SettlementStateInfo.
             * @implements ISettlementStateInfo
             * @constructor
             * @param {aslivecasino.Bac_DetailRoomState.ISettlementStateInfo=} [properties] Properties to set
             */
            function SettlementStateInfo(properties) {
                this.win_div_list = [];
                if (properties)
                    for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                        if (properties[keys[i]] != null)
                            this[keys[i]] = properties[keys[i]];
            }

            /**
             * SettlementStateInfo win_div_list.
             * @member {Array.<aslivecasino.Bac_EnumDivType>} win_div_list
             * @memberof aslivecasino.Bac_DetailRoomState.SettlementStateInfo
             * @instance
             */
            SettlementStateInfo.prototype.win_div_list = $util.emptyArray;

            /**
             * Creates a new SettlementStateInfo instance using the specified properties.
             * @function create
             * @memberof aslivecasino.Bac_DetailRoomState.SettlementStateInfo
             * @static
             * @param {aslivecasino.Bac_DetailRoomState.ISettlementStateInfo=} [properties] Properties to set
             * @returns {aslivecasino.Bac_DetailRoomState.SettlementStateInfo} SettlementStateInfo instance
             */
            SettlementStateInfo.create = function create(properties) {
                return new SettlementStateInfo(properties);
            };

            /**
             * Encodes the specified SettlementStateInfo message. Does not implicitly {@link aslivecasino.Bac_DetailRoomState.SettlementStateInfo.verify|verify} messages.
             * @function encode
             * @memberof aslivecasino.Bac_DetailRoomState.SettlementStateInfo
             * @static
             * @param {aslivecasino.Bac_DetailRoomState.ISettlementStateInfo} message SettlementStateInfo message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            SettlementStateInfo.encode = function encode(message, writer) {
                if (!writer)
                    writer = $Writer.create();
                if (message.win_div_list != null && message.win_div_list.length) {
                    writer.uint32(/* id 1, wireType 2 =*/10).fork();
                    for (var i = 0; i < message.win_div_list.length; ++i)
                        writer.int32(message.win_div_list[i]);
                    writer.ldelim();
                }
                return writer;
            };

            /**
             * Encodes the specified SettlementStateInfo message, length delimited. Does not implicitly {@link aslivecasino.Bac_DetailRoomState.SettlementStateInfo.verify|verify} messages.
             * @function encodeDelimited
             * @memberof aslivecasino.Bac_DetailRoomState.SettlementStateInfo
             * @static
             * @param {aslivecasino.Bac_DetailRoomState.ISettlementStateInfo} message SettlementStateInfo message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            SettlementStateInfo.encodeDelimited = function encodeDelimited(message, writer) {
                return this.encode(message, writer).ldelim();
            };

            /**
             * Decodes a SettlementStateInfo message from the specified reader or buffer.
             * @function decode
             * @memberof aslivecasino.Bac_DetailRoomState.SettlementStateInfo
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @param {number} [length] Message length if known beforehand
             * @returns {aslivecasino.Bac_DetailRoomState.SettlementStateInfo} SettlementStateInfo
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            SettlementStateInfo.decode = function decode(reader, length) {
                if (!(reader instanceof $Reader))
                    reader = $Reader.create(reader);
                var end = length === undefined ? reader.len : reader.pos + length, message = new $root.aslivecasino.Bac_DetailRoomState.SettlementStateInfo();
                while (reader.pos < end) {
                    var tag = reader.uint32();
                    switch (tag >>> 3) {
                    case 1:
                        if (!(message.win_div_list && message.win_div_list.length))
                            message.win_div_list = [];
                        if ((tag & 7) === 2) {
                            var end2 = reader.uint32() + reader.pos;
                            while (reader.pos < end2)
                                message.win_div_list.push(reader.int32());
                        } else
                            message.win_div_list.push(reader.int32());
                        break;
                    default:
                        reader.skipType(tag & 7);
                        break;
                    }
                }
                return message;
            };

            /**
             * Decodes a SettlementStateInfo message from the specified reader or buffer, length delimited.
             * @function decodeDelimited
             * @memberof aslivecasino.Bac_DetailRoomState.SettlementStateInfo
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @returns {aslivecasino.Bac_DetailRoomState.SettlementStateInfo} SettlementStateInfo
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            SettlementStateInfo.decodeDelimited = function decodeDelimited(reader) {
                if (!(reader instanceof $Reader))
                    reader = new $Reader(reader);
                return this.decode(reader, reader.uint32());
            };

            /**
             * Verifies a SettlementStateInfo message.
             * @function verify
             * @memberof aslivecasino.Bac_DetailRoomState.SettlementStateInfo
             * @static
             * @param {Object.<string,*>} message Plain object to verify
             * @returns {string|null} `null` if valid, otherwise the reason why it is not
             */
            SettlementStateInfo.verify = function verify(message) {
                if (typeof message !== "object" || message === null)
                    return "object expected";
                if (message.win_div_list != null && message.hasOwnProperty("win_div_list")) {
                    if (!Array.isArray(message.win_div_list))
                        return "win_div_list: array expected";
                    for (var i = 0; i < message.win_div_list.length; ++i)
                        switch (message.win_div_list[i]) {
                        default:
                            return "win_div_list: enum value[] expected";
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                            break;
                        }
                }
                return null;
            };

            /**
             * Creates a SettlementStateInfo message from a plain object. Also converts values to their respective internal types.
             * @function fromObject
             * @memberof aslivecasino.Bac_DetailRoomState.SettlementStateInfo
             * @static
             * @param {Object.<string,*>} object Plain object
             * @returns {aslivecasino.Bac_DetailRoomState.SettlementStateInfo} SettlementStateInfo
             */
            SettlementStateInfo.fromObject = function fromObject(object) {
                if (object instanceof $root.aslivecasino.Bac_DetailRoomState.SettlementStateInfo)
                    return object;
                var message = new $root.aslivecasino.Bac_DetailRoomState.SettlementStateInfo();
                if (object.win_div_list) {
                    if (!Array.isArray(object.win_div_list))
                        throw TypeError(".aslivecasino.Bac_DetailRoomState.SettlementStateInfo.win_div_list: array expected");
                    message.win_div_list = [];
                    for (var i = 0; i < object.win_div_list.length; ++i)
                        switch (object.win_div_list[i]) {
                        default:
                        case "PLAYER":
                        case 0:
                            message.win_div_list[i] = 0;
                            break;
                        case "BANKER":
                        case 1:
                            message.win_div_list[i] = 1;
                            break;
                        case "TIE":
                        case 2:
                            message.win_div_list[i] = 2;
                            break;
                        case "PPAIR":
                        case 3:
                            message.win_div_list[i] = 3;
                            break;
                        case "BPAIR":
                        case 4:
                            message.win_div_list[i] = 4;
                            break;
                        }
                }
                return message;
            };

            /**
             * Creates a plain object from a SettlementStateInfo message. Also converts values to other types if specified.
             * @function toObject
             * @memberof aslivecasino.Bac_DetailRoomState.SettlementStateInfo
             * @static
             * @param {aslivecasino.Bac_DetailRoomState.SettlementStateInfo} message SettlementStateInfo
             * @param {$protobuf.IConversionOptions} [options] Conversion options
             * @returns {Object.<string,*>} Plain object
             */
            SettlementStateInfo.toObject = function toObject(message, options) {
                if (!options)
                    options = {};
                var object = {};
                if (options.arrays || options.defaults)
                    object.win_div_list = [];
                if (message.win_div_list && message.win_div_list.length) {
                    object.win_div_list = [];
                    for (var j = 0; j < message.win_div_list.length; ++j)
                        object.win_div_list[j] = options.enums === String ? $root.aslivecasino.Bac_EnumDivType[message.win_div_list[j]] : message.win_div_list[j];
                }
                return object;
            };

            /**
             * Converts this SettlementStateInfo to JSON.
             * @function toJSON
             * @memberof aslivecasino.Bac_DetailRoomState.SettlementStateInfo
             * @instance
             * @returns {Object.<string,*>} JSON object
             */
            SettlementStateInfo.prototype.toJSON = function toJSON() {
                return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
            };

            return SettlementStateInfo;
        })();

        return Bac_DetailRoomState;
    })();

    aslivecasino.Bac_BetSettlementResult = (function() {

        /**
         * Properties of a Bac_BetSettlementResult.
         * @memberof aslivecasino
         * @interface IBac_BetSettlementResult
         * @property {string|null} [room_no] Bac_BetSettlementResult room_no
         * @property {number|null} [table_no] Bac_BetSettlementResult table_no
         * @property {Array.<aslivecasino.Bac_BetSettlementResult.ISettlementData>|null} [settlement_list] Bac_BetSettlementResult settlement_list
         */

        /**
         * Constructs a new Bac_BetSettlementResult.
         * @memberof aslivecasino
         * @classdesc Represents a Bac_BetSettlementResult.
         * @implements IBac_BetSettlementResult
         * @constructor
         * @param {aslivecasino.IBac_BetSettlementResult=} [properties] Properties to set
         */
        function Bac_BetSettlementResult(properties) {
            this.settlement_list = [];
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Bac_BetSettlementResult room_no.
         * @member {string} room_no
         * @memberof aslivecasino.Bac_BetSettlementResult
         * @instance
         */
        Bac_BetSettlementResult.prototype.room_no = "";

        /**
         * Bac_BetSettlementResult table_no.
         * @member {number} table_no
         * @memberof aslivecasino.Bac_BetSettlementResult
         * @instance
         */
        Bac_BetSettlementResult.prototype.table_no = 0;

        /**
         * Bac_BetSettlementResult settlement_list.
         * @member {Array.<aslivecasino.Bac_BetSettlementResult.ISettlementData>} settlement_list
         * @memberof aslivecasino.Bac_BetSettlementResult
         * @instance
         */
        Bac_BetSettlementResult.prototype.settlement_list = $util.emptyArray;

        /**
         * Creates a new Bac_BetSettlementResult instance using the specified properties.
         * @function create
         * @memberof aslivecasino.Bac_BetSettlementResult
         * @static
         * @param {aslivecasino.IBac_BetSettlementResult=} [properties] Properties to set
         * @returns {aslivecasino.Bac_BetSettlementResult} Bac_BetSettlementResult instance
         */
        Bac_BetSettlementResult.create = function create(properties) {
            return new Bac_BetSettlementResult(properties);
        };

        /**
         * Encodes the specified Bac_BetSettlementResult message. Does not implicitly {@link aslivecasino.Bac_BetSettlementResult.verify|verify} messages.
         * @function encode
         * @memberof aslivecasino.Bac_BetSettlementResult
         * @static
         * @param {aslivecasino.IBac_BetSettlementResult} message Bac_BetSettlementResult message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Bac_BetSettlementResult.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.room_no);
            if (message.table_no != null && message.hasOwnProperty("table_no"))
                writer.uint32(/* id 2, wireType 0 =*/16).sint32(message.table_no);
            if (message.settlement_list != null && message.settlement_list.length)
                for (var i = 0; i < message.settlement_list.length; ++i)
                    $root.aslivecasino.Bac_BetSettlementResult.SettlementData.encode(message.settlement_list[i], writer.uint32(/* id 3, wireType 2 =*/26).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified Bac_BetSettlementResult message, length delimited. Does not implicitly {@link aslivecasino.Bac_BetSettlementResult.verify|verify} messages.
         * @function encodeDelimited
         * @memberof aslivecasino.Bac_BetSettlementResult
         * @static
         * @param {aslivecasino.IBac_BetSettlementResult} message Bac_BetSettlementResult message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Bac_BetSettlementResult.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Bac_BetSettlementResult message from the specified reader or buffer.
         * @function decode
         * @memberof aslivecasino.Bac_BetSettlementResult
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {aslivecasino.Bac_BetSettlementResult} Bac_BetSettlementResult
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Bac_BetSettlementResult.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.aslivecasino.Bac_BetSettlementResult();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.room_no = reader.string();
                    break;
                case 2:
                    message.table_no = reader.sint32();
                    break;
                case 3:
                    if (!(message.settlement_list && message.settlement_list.length))
                        message.settlement_list = [];
                    message.settlement_list.push($root.aslivecasino.Bac_BetSettlementResult.SettlementData.decode(reader, reader.uint32()));
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Bac_BetSettlementResult message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof aslivecasino.Bac_BetSettlementResult
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {aslivecasino.Bac_BetSettlementResult} Bac_BetSettlementResult
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Bac_BetSettlementResult.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Bac_BetSettlementResult message.
         * @function verify
         * @memberof aslivecasino.Bac_BetSettlementResult
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Bac_BetSettlementResult.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                if (!$util.isString(message.room_no))
                    return "room_no: string expected";
            if (message.table_no != null && message.hasOwnProperty("table_no"))
                if (!$util.isInteger(message.table_no))
                    return "table_no: integer expected";
            if (message.settlement_list != null && message.hasOwnProperty("settlement_list")) {
                if (!Array.isArray(message.settlement_list))
                    return "settlement_list: array expected";
                for (var i = 0; i < message.settlement_list.length; ++i) {
                    var error = $root.aslivecasino.Bac_BetSettlementResult.SettlementData.verify(message.settlement_list[i]);
                    if (error)
                        return "settlement_list." + error;
                }
            }
            return null;
        };

        /**
         * Creates a Bac_BetSettlementResult message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof aslivecasino.Bac_BetSettlementResult
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {aslivecasino.Bac_BetSettlementResult} Bac_BetSettlementResult
         */
        Bac_BetSettlementResult.fromObject = function fromObject(object) {
            if (object instanceof $root.aslivecasino.Bac_BetSettlementResult)
                return object;
            var message = new $root.aslivecasino.Bac_BetSettlementResult();
            if (object.room_no != null)
                message.room_no = String(object.room_no);
            if (object.table_no != null)
                message.table_no = object.table_no | 0;
            if (object.settlement_list) {
                if (!Array.isArray(object.settlement_list))
                    throw TypeError(".aslivecasino.Bac_BetSettlementResult.settlement_list: array expected");
                message.settlement_list = [];
                for (var i = 0; i < object.settlement_list.length; ++i) {
                    if (typeof object.settlement_list[i] !== "object")
                        throw TypeError(".aslivecasino.Bac_BetSettlementResult.settlement_list: object expected");
                    message.settlement_list[i] = $root.aslivecasino.Bac_BetSettlementResult.SettlementData.fromObject(object.settlement_list[i]);
                }
            }
            return message;
        };

        /**
         * Creates a plain object from a Bac_BetSettlementResult message. Also converts values to other types if specified.
         * @function toObject
         * @memberof aslivecasino.Bac_BetSettlementResult
         * @static
         * @param {aslivecasino.Bac_BetSettlementResult} message Bac_BetSettlementResult
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Bac_BetSettlementResult.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.arrays || options.defaults)
                object.settlement_list = [];
            if (options.defaults) {
                object.room_no = "";
                object.table_no = 0;
            }
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                object.room_no = message.room_no;
            if (message.table_no != null && message.hasOwnProperty("table_no"))
                object.table_no = message.table_no;
            if (message.settlement_list && message.settlement_list.length) {
                object.settlement_list = [];
                for (var j = 0; j < message.settlement_list.length; ++j)
                    object.settlement_list[j] = $root.aslivecasino.Bac_BetSettlementResult.SettlementData.toObject(message.settlement_list[j], options);
            }
            return object;
        };

        /**
         * Converts this Bac_BetSettlementResult to JSON.
         * @function toJSON
         * @memberof aslivecasino.Bac_BetSettlementResult
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Bac_BetSettlementResult.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        Bac_BetSettlementResult.SettlementData = (function() {

            /**
             * Properties of a SettlementData.
             * @memberof aslivecasino.Bac_BetSettlementResult
             * @interface ISettlementData
             * @property {number|null} [seat_no] SettlementData seat_no
             * @property {string|null} [user_name] SettlementData user_name
             * @property {number|null} [balance] SettlementData balance
             * @property {Array.<aslivecasino.Bac_BetSettlementResult.SettlementData.IWonData>|null} [won_list] SettlementData won_list
             */

            /**
             * Constructs a new SettlementData.
             * @memberof aslivecasino.Bac_BetSettlementResult
             * @classdesc Represents a SettlementData.
             * @implements ISettlementData
             * @constructor
             * @param {aslivecasino.Bac_BetSettlementResult.ISettlementData=} [properties] Properties to set
             */
            function SettlementData(properties) {
                this.won_list = [];
                if (properties)
                    for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                        if (properties[keys[i]] != null)
                            this[keys[i]] = properties[keys[i]];
            }

            /**
             * SettlementData seat_no.
             * @member {number} seat_no
             * @memberof aslivecasino.Bac_BetSettlementResult.SettlementData
             * @instance
             */
            SettlementData.prototype.seat_no = 0;

            /**
             * SettlementData user_name.
             * @member {string} user_name
             * @memberof aslivecasino.Bac_BetSettlementResult.SettlementData
             * @instance
             */
            SettlementData.prototype.user_name = "";

            /**
             * SettlementData balance.
             * @member {number} balance
             * @memberof aslivecasino.Bac_BetSettlementResult.SettlementData
             * @instance
             */
            SettlementData.prototype.balance = 0;

            /**
             * SettlementData won_list.
             * @member {Array.<aslivecasino.Bac_BetSettlementResult.SettlementData.IWonData>} won_list
             * @memberof aslivecasino.Bac_BetSettlementResult.SettlementData
             * @instance
             */
            SettlementData.prototype.won_list = $util.emptyArray;

            /**
             * Creates a new SettlementData instance using the specified properties.
             * @function create
             * @memberof aslivecasino.Bac_BetSettlementResult.SettlementData
             * @static
             * @param {aslivecasino.Bac_BetSettlementResult.ISettlementData=} [properties] Properties to set
             * @returns {aslivecasino.Bac_BetSettlementResult.SettlementData} SettlementData instance
             */
            SettlementData.create = function create(properties) {
                return new SettlementData(properties);
            };

            /**
             * Encodes the specified SettlementData message. Does not implicitly {@link aslivecasino.Bac_BetSettlementResult.SettlementData.verify|verify} messages.
             * @function encode
             * @memberof aslivecasino.Bac_BetSettlementResult.SettlementData
             * @static
             * @param {aslivecasino.Bac_BetSettlementResult.ISettlementData} message SettlementData message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            SettlementData.encode = function encode(message, writer) {
                if (!writer)
                    writer = $Writer.create();
                if (message.seat_no != null && message.hasOwnProperty("seat_no"))
                    writer.uint32(/* id 1, wireType 0 =*/8).sint32(message.seat_no);
                if (message.user_name != null && message.hasOwnProperty("user_name"))
                    writer.uint32(/* id 2, wireType 2 =*/18).string(message.user_name);
                if (message.balance != null && message.hasOwnProperty("balance"))
                    writer.uint32(/* id 3, wireType 1 =*/25).double(message.balance);
                if (message.won_list != null && message.won_list.length)
                    for (var i = 0; i < message.won_list.length; ++i)
                        $root.aslivecasino.Bac_BetSettlementResult.SettlementData.WonData.encode(message.won_list[i], writer.uint32(/* id 4, wireType 2 =*/34).fork()).ldelim();
                return writer;
            };

            /**
             * Encodes the specified SettlementData message, length delimited. Does not implicitly {@link aslivecasino.Bac_BetSettlementResult.SettlementData.verify|verify} messages.
             * @function encodeDelimited
             * @memberof aslivecasino.Bac_BetSettlementResult.SettlementData
             * @static
             * @param {aslivecasino.Bac_BetSettlementResult.ISettlementData} message SettlementData message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            SettlementData.encodeDelimited = function encodeDelimited(message, writer) {
                return this.encode(message, writer).ldelim();
            };

            /**
             * Decodes a SettlementData message from the specified reader or buffer.
             * @function decode
             * @memberof aslivecasino.Bac_BetSettlementResult.SettlementData
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @param {number} [length] Message length if known beforehand
             * @returns {aslivecasino.Bac_BetSettlementResult.SettlementData} SettlementData
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            SettlementData.decode = function decode(reader, length) {
                if (!(reader instanceof $Reader))
                    reader = $Reader.create(reader);
                var end = length === undefined ? reader.len : reader.pos + length, message = new $root.aslivecasino.Bac_BetSettlementResult.SettlementData();
                while (reader.pos < end) {
                    var tag = reader.uint32();
                    switch (tag >>> 3) {
                    case 1:
                        message.seat_no = reader.sint32();
                        break;
                    case 2:
                        message.user_name = reader.string();
                        break;
                    case 3:
                        message.balance = reader.double();
                        break;
                    case 4:
                        if (!(message.won_list && message.won_list.length))
                            message.won_list = [];
                        message.won_list.push($root.aslivecasino.Bac_BetSettlementResult.SettlementData.WonData.decode(reader, reader.uint32()));
                        break;
                    default:
                        reader.skipType(tag & 7);
                        break;
                    }
                }
                return message;
            };

            /**
             * Decodes a SettlementData message from the specified reader or buffer, length delimited.
             * @function decodeDelimited
             * @memberof aslivecasino.Bac_BetSettlementResult.SettlementData
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @returns {aslivecasino.Bac_BetSettlementResult.SettlementData} SettlementData
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            SettlementData.decodeDelimited = function decodeDelimited(reader) {
                if (!(reader instanceof $Reader))
                    reader = new $Reader(reader);
                return this.decode(reader, reader.uint32());
            };

            /**
             * Verifies a SettlementData message.
             * @function verify
             * @memberof aslivecasino.Bac_BetSettlementResult.SettlementData
             * @static
             * @param {Object.<string,*>} message Plain object to verify
             * @returns {string|null} `null` if valid, otherwise the reason why it is not
             */
            SettlementData.verify = function verify(message) {
                if (typeof message !== "object" || message === null)
                    return "object expected";
                if (message.seat_no != null && message.hasOwnProperty("seat_no"))
                    if (!$util.isInteger(message.seat_no))
                        return "seat_no: integer expected";
                if (message.user_name != null && message.hasOwnProperty("user_name"))
                    if (!$util.isString(message.user_name))
                        return "user_name: string expected";
                if (message.balance != null && message.hasOwnProperty("balance"))
                    if (typeof message.balance !== "number")
                        return "balance: number expected";
                if (message.won_list != null && message.hasOwnProperty("won_list")) {
                    if (!Array.isArray(message.won_list))
                        return "won_list: array expected";
                    for (var i = 0; i < message.won_list.length; ++i) {
                        var error = $root.aslivecasino.Bac_BetSettlementResult.SettlementData.WonData.verify(message.won_list[i]);
                        if (error)
                            return "won_list." + error;
                    }
                }
                return null;
            };

            /**
             * Creates a SettlementData message from a plain object. Also converts values to their respective internal types.
             * @function fromObject
             * @memberof aslivecasino.Bac_BetSettlementResult.SettlementData
             * @static
             * @param {Object.<string,*>} object Plain object
             * @returns {aslivecasino.Bac_BetSettlementResult.SettlementData} SettlementData
             */
            SettlementData.fromObject = function fromObject(object) {
                if (object instanceof $root.aslivecasino.Bac_BetSettlementResult.SettlementData)
                    return object;
                var message = new $root.aslivecasino.Bac_BetSettlementResult.SettlementData();
                if (object.seat_no != null)
                    message.seat_no = object.seat_no | 0;
                if (object.user_name != null)
                    message.user_name = String(object.user_name);
                if (object.balance != null)
                    message.balance = Number(object.balance);
                if (object.won_list) {
                    if (!Array.isArray(object.won_list))
                        throw TypeError(".aslivecasino.Bac_BetSettlementResult.SettlementData.won_list: array expected");
                    message.won_list = [];
                    for (var i = 0; i < object.won_list.length; ++i) {
                        if (typeof object.won_list[i] !== "object")
                            throw TypeError(".aslivecasino.Bac_BetSettlementResult.SettlementData.won_list: object expected");
                        message.won_list[i] = $root.aslivecasino.Bac_BetSettlementResult.SettlementData.WonData.fromObject(object.won_list[i]);
                    }
                }
                return message;
            };

            /**
             * Creates a plain object from a SettlementData message. Also converts values to other types if specified.
             * @function toObject
             * @memberof aslivecasino.Bac_BetSettlementResult.SettlementData
             * @static
             * @param {aslivecasino.Bac_BetSettlementResult.SettlementData} message SettlementData
             * @param {$protobuf.IConversionOptions} [options] Conversion options
             * @returns {Object.<string,*>} Plain object
             */
            SettlementData.toObject = function toObject(message, options) {
                if (!options)
                    options = {};
                var object = {};
                if (options.arrays || options.defaults)
                    object.won_list = [];
                if (options.defaults) {
                    object.seat_no = 0;
                    object.user_name = "";
                    object.balance = 0;
                }
                if (message.seat_no != null && message.hasOwnProperty("seat_no"))
                    object.seat_no = message.seat_no;
                if (message.user_name != null && message.hasOwnProperty("user_name"))
                    object.user_name = message.user_name;
                if (message.balance != null && message.hasOwnProperty("balance"))
                    object.balance = options.json && !isFinite(message.balance) ? String(message.balance) : message.balance;
                if (message.won_list && message.won_list.length) {
                    object.won_list = [];
                    for (var j = 0; j < message.won_list.length; ++j)
                        object.won_list[j] = $root.aslivecasino.Bac_BetSettlementResult.SettlementData.WonData.toObject(message.won_list[j], options);
                }
                return object;
            };

            /**
             * Converts this SettlementData to JSON.
             * @function toJSON
             * @memberof aslivecasino.Bac_BetSettlementResult.SettlementData
             * @instance
             * @returns {Object.<string,*>} JSON object
             */
            SettlementData.prototype.toJSON = function toJSON() {
                return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
            };

            /**
             * EnumBetResult enum.
             * @name aslivecasino.Bac_BetSettlementResult.SettlementData.EnumBetResult
             * @enum {string}
             * @property {number} NONE=0 NONE value
             * @property {number} WIN=1 WIN value
             * @property {number} LOSE=2 LOSE value
             * @property {number} RETURNCHIP=3 RETURNCHIP value
             */
            SettlementData.EnumBetResult = (function() {
                var valuesById = {}, values = Object.create(valuesById);
                values[valuesById[0] = "NONE"] = 0;
                values[valuesById[1] = "WIN"] = 1;
                values[valuesById[2] = "LOSE"] = 2;
                values[valuesById[3] = "RETURNCHIP"] = 3;
                return values;
            })();

            SettlementData.WonData = (function() {

                /**
                 * Properties of a WonData.
                 * @memberof aslivecasino.Bac_BetSettlementResult.SettlementData
                 * @interface IWonData
                 * @property {aslivecasino.Bac_EnumDivType|null} [div_type] WonData div_type
                 * @property {number|null} [bet_chips] WonData bet_chips
                 * @property {aslivecasino.Bac_BetSettlementResult.SettlementData.EnumBetResult|null} [bet_result] WonData bet_result
                 * @property {number|null} [won_chips] WonData won_chips
                 */

                /**
                 * Constructs a new WonData.
                 * @memberof aslivecasino.Bac_BetSettlementResult.SettlementData
                 * @classdesc Represents a WonData.
                 * @implements IWonData
                 * @constructor
                 * @param {aslivecasino.Bac_BetSettlementResult.SettlementData.IWonData=} [properties] Properties to set
                 */
                function WonData(properties) {
                    if (properties)
                        for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                            if (properties[keys[i]] != null)
                                this[keys[i]] = properties[keys[i]];
                }

                /**
                 * WonData div_type.
                 * @member {aslivecasino.Bac_EnumDivType} div_type
                 * @memberof aslivecasino.Bac_BetSettlementResult.SettlementData.WonData
                 * @instance
                 */
                WonData.prototype.div_type = 0;

                /**
                 * WonData bet_chips.
                 * @member {number} bet_chips
                 * @memberof aslivecasino.Bac_BetSettlementResult.SettlementData.WonData
                 * @instance
                 */
                WonData.prototype.bet_chips = 0;

                /**
                 * WonData bet_result.
                 * @member {aslivecasino.Bac_BetSettlementResult.SettlementData.EnumBetResult} bet_result
                 * @memberof aslivecasino.Bac_BetSettlementResult.SettlementData.WonData
                 * @instance
                 */
                WonData.prototype.bet_result = 0;

                /**
                 * WonData won_chips.
                 * @member {number} won_chips
                 * @memberof aslivecasino.Bac_BetSettlementResult.SettlementData.WonData
                 * @instance
                 */
                WonData.prototype.won_chips = 0;

                /**
                 * Creates a new WonData instance using the specified properties.
                 * @function create
                 * @memberof aslivecasino.Bac_BetSettlementResult.SettlementData.WonData
                 * @static
                 * @param {aslivecasino.Bac_BetSettlementResult.SettlementData.IWonData=} [properties] Properties to set
                 * @returns {aslivecasino.Bac_BetSettlementResult.SettlementData.WonData} WonData instance
                 */
                WonData.create = function create(properties) {
                    return new WonData(properties);
                };

                /**
                 * Encodes the specified WonData message. Does not implicitly {@link aslivecasino.Bac_BetSettlementResult.SettlementData.WonData.verify|verify} messages.
                 * @function encode
                 * @memberof aslivecasino.Bac_BetSettlementResult.SettlementData.WonData
                 * @static
                 * @param {aslivecasino.Bac_BetSettlementResult.SettlementData.IWonData} message WonData message or plain object to encode
                 * @param {$protobuf.Writer} [writer] Writer to encode to
                 * @returns {$protobuf.Writer} Writer
                 */
                WonData.encode = function encode(message, writer) {
                    if (!writer)
                        writer = $Writer.create();
                    if (message.div_type != null && message.hasOwnProperty("div_type"))
                        writer.uint32(/* id 1, wireType 0 =*/8).int32(message.div_type);
                    if (message.bet_chips != null && message.hasOwnProperty("bet_chips"))
                        writer.uint32(/* id 2, wireType 0 =*/16).int32(message.bet_chips);
                    if (message.bet_result != null && message.hasOwnProperty("bet_result"))
                        writer.uint32(/* id 3, wireType 0 =*/24).int32(message.bet_result);
                    if (message.won_chips != null && message.hasOwnProperty("won_chips"))
                        writer.uint32(/* id 4, wireType 1 =*/33).double(message.won_chips);
                    return writer;
                };

                /**
                 * Encodes the specified WonData message, length delimited. Does not implicitly {@link aslivecasino.Bac_BetSettlementResult.SettlementData.WonData.verify|verify} messages.
                 * @function encodeDelimited
                 * @memberof aslivecasino.Bac_BetSettlementResult.SettlementData.WonData
                 * @static
                 * @param {aslivecasino.Bac_BetSettlementResult.SettlementData.IWonData} message WonData message or plain object to encode
                 * @param {$protobuf.Writer} [writer] Writer to encode to
                 * @returns {$protobuf.Writer} Writer
                 */
                WonData.encodeDelimited = function encodeDelimited(message, writer) {
                    return this.encode(message, writer).ldelim();
                };

                /**
                 * Decodes a WonData message from the specified reader or buffer.
                 * @function decode
                 * @memberof aslivecasino.Bac_BetSettlementResult.SettlementData.WonData
                 * @static
                 * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                 * @param {number} [length] Message length if known beforehand
                 * @returns {aslivecasino.Bac_BetSettlementResult.SettlementData.WonData} WonData
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                WonData.decode = function decode(reader, length) {
                    if (!(reader instanceof $Reader))
                        reader = $Reader.create(reader);
                    var end = length === undefined ? reader.len : reader.pos + length, message = new $root.aslivecasino.Bac_BetSettlementResult.SettlementData.WonData();
                    while (reader.pos < end) {
                        var tag = reader.uint32();
                        switch (tag >>> 3) {
                        case 1:
                            message.div_type = reader.int32();
                            break;
                        case 2:
                            message.bet_chips = reader.int32();
                            break;
                        case 3:
                            message.bet_result = reader.int32();
                            break;
                        case 4:
                            message.won_chips = reader.double();
                            break;
                        default:
                            reader.skipType(tag & 7);
                            break;
                        }
                    }
                    return message;
                };

                /**
                 * Decodes a WonData message from the specified reader or buffer, length delimited.
                 * @function decodeDelimited
                 * @memberof aslivecasino.Bac_BetSettlementResult.SettlementData.WonData
                 * @static
                 * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                 * @returns {aslivecasino.Bac_BetSettlementResult.SettlementData.WonData} WonData
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                WonData.decodeDelimited = function decodeDelimited(reader) {
                    if (!(reader instanceof $Reader))
                        reader = new $Reader(reader);
                    return this.decode(reader, reader.uint32());
                };

                /**
                 * Verifies a WonData message.
                 * @function verify
                 * @memberof aslivecasino.Bac_BetSettlementResult.SettlementData.WonData
                 * @static
                 * @param {Object.<string,*>} message Plain object to verify
                 * @returns {string|null} `null` if valid, otherwise the reason why it is not
                 */
                WonData.verify = function verify(message) {
                    if (typeof message !== "object" || message === null)
                        return "object expected";
                    if (message.div_type != null && message.hasOwnProperty("div_type"))
                        switch (message.div_type) {
                        default:
                            return "div_type: enum value expected";
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                            break;
                        }
                    if (message.bet_chips != null && message.hasOwnProperty("bet_chips"))
                        if (!$util.isInteger(message.bet_chips))
                            return "bet_chips: integer expected";
                    if (message.bet_result != null && message.hasOwnProperty("bet_result"))
                        switch (message.bet_result) {
                        default:
                            return "bet_result: enum value expected";
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                            break;
                        }
                    if (message.won_chips != null && message.hasOwnProperty("won_chips"))
                        if (typeof message.won_chips !== "number")
                            return "won_chips: number expected";
                    return null;
                };

                /**
                 * Creates a WonData message from a plain object. Also converts values to their respective internal types.
                 * @function fromObject
                 * @memberof aslivecasino.Bac_BetSettlementResult.SettlementData.WonData
                 * @static
                 * @param {Object.<string,*>} object Plain object
                 * @returns {aslivecasino.Bac_BetSettlementResult.SettlementData.WonData} WonData
                 */
                WonData.fromObject = function fromObject(object) {
                    if (object instanceof $root.aslivecasino.Bac_BetSettlementResult.SettlementData.WonData)
                        return object;
                    var message = new $root.aslivecasino.Bac_BetSettlementResult.SettlementData.WonData();
                    switch (object.div_type) {
                    case "PLAYER":
                    case 0:
                        message.div_type = 0;
                        break;
                    case "BANKER":
                    case 1:
                        message.div_type = 1;
                        break;
                    case "TIE":
                    case 2:
                        message.div_type = 2;
                        break;
                    case "PPAIR":
                    case 3:
                        message.div_type = 3;
                        break;
                    case "BPAIR":
                    case 4:
                        message.div_type = 4;
                        break;
                    }
                    if (object.bet_chips != null)
                        message.bet_chips = object.bet_chips | 0;
                    switch (object.bet_result) {
                    case "NONE":
                    case 0:
                        message.bet_result = 0;
                        break;
                    case "WIN":
                    case 1:
                        message.bet_result = 1;
                        break;
                    case "LOSE":
                    case 2:
                        message.bet_result = 2;
                        break;
                    case "RETURNCHIP":
                    case 3:
                        message.bet_result = 3;
                        break;
                    }
                    if (object.won_chips != null)
                        message.won_chips = Number(object.won_chips);
                    return message;
                };

                /**
                 * Creates a plain object from a WonData message. Also converts values to other types if specified.
                 * @function toObject
                 * @memberof aslivecasino.Bac_BetSettlementResult.SettlementData.WonData
                 * @static
                 * @param {aslivecasino.Bac_BetSettlementResult.SettlementData.WonData} message WonData
                 * @param {$protobuf.IConversionOptions} [options] Conversion options
                 * @returns {Object.<string,*>} Plain object
                 */
                WonData.toObject = function toObject(message, options) {
                    if (!options)
                        options = {};
                    var object = {};
                    if (options.defaults) {
                        object.div_type = options.enums === String ? "PLAYER" : 0;
                        object.bet_chips = 0;
                        object.bet_result = options.enums === String ? "NONE" : 0;
                        object.won_chips = 0;
                    }
                    if (message.div_type != null && message.hasOwnProperty("div_type"))
                        object.div_type = options.enums === String ? $root.aslivecasino.Bac_EnumDivType[message.div_type] : message.div_type;
                    if (message.bet_chips != null && message.hasOwnProperty("bet_chips"))
                        object.bet_chips = message.bet_chips;
                    if (message.bet_result != null && message.hasOwnProperty("bet_result"))
                        object.bet_result = options.enums === String ? $root.aslivecasino.Bac_BetSettlementResult.SettlementData.EnumBetResult[message.bet_result] : message.bet_result;
                    if (message.won_chips != null && message.hasOwnProperty("won_chips"))
                        object.won_chips = options.json && !isFinite(message.won_chips) ? String(message.won_chips) : message.won_chips;
                    return object;
                };

                /**
                 * Converts this WonData to JSON.
                 * @function toJSON
                 * @memberof aslivecasino.Bac_BetSettlementResult.SettlementData.WonData
                 * @instance
                 * @returns {Object.<string,*>} JSON object
                 */
                WonData.prototype.toJSON = function toJSON() {
                    return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
                };

                return WonData;
            })();

            return SettlementData;
        })();

        return Bac_BetSettlementResult;
    })();

    aslivecasino.Bac_MeCardAction = (function() {

        /**
         * Properties of a Bac_MeCardAction.
         * @memberof aslivecasino
         * @interface IBac_MeCardAction
         * @property {string|null} [room_no] Bac_MeCardAction room_no
         * @property {number|null} [which_side] Bac_MeCardAction which_side
         * @property {boolean|null} [rotated] Bac_MeCardAction rotated
         * @property {number|null} [card_index] Bac_MeCardAction card_index
         * @property {number|null} [corner] Bac_MeCardAction corner
         * @property {number|null} [x] Bac_MeCardAction x
         * @property {number|null} [y] Bac_MeCardAction y
         * @property {boolean|null} [opened] Bac_MeCardAction opened
         */

        /**
         * Constructs a new Bac_MeCardAction.
         * @memberof aslivecasino
         * @classdesc Represents a Bac_MeCardAction.
         * @implements IBac_MeCardAction
         * @constructor
         * @param {aslivecasino.IBac_MeCardAction=} [properties] Properties to set
         */
        function Bac_MeCardAction(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Bac_MeCardAction room_no.
         * @member {string} room_no
         * @memberof aslivecasino.Bac_MeCardAction
         * @instance
         */
        Bac_MeCardAction.prototype.room_no = "";

        /**
         * Bac_MeCardAction which_side.
         * @member {number} which_side
         * @memberof aslivecasino.Bac_MeCardAction
         * @instance
         */
        Bac_MeCardAction.prototype.which_side = 0;

        /**
         * Bac_MeCardAction rotated.
         * @member {boolean} rotated
         * @memberof aslivecasino.Bac_MeCardAction
         * @instance
         */
        Bac_MeCardAction.prototype.rotated = false;

        /**
         * Bac_MeCardAction card_index.
         * @member {number} card_index
         * @memberof aslivecasino.Bac_MeCardAction
         * @instance
         */
        Bac_MeCardAction.prototype.card_index = 0;

        /**
         * Bac_MeCardAction corner.
         * @member {number} corner
         * @memberof aslivecasino.Bac_MeCardAction
         * @instance
         */
        Bac_MeCardAction.prototype.corner = 0;

        /**
         * Bac_MeCardAction x.
         * @member {number} x
         * @memberof aslivecasino.Bac_MeCardAction
         * @instance
         */
        Bac_MeCardAction.prototype.x = 0;

        /**
         * Bac_MeCardAction y.
         * @member {number} y
         * @memberof aslivecasino.Bac_MeCardAction
         * @instance
         */
        Bac_MeCardAction.prototype.y = 0;

        /**
         * Bac_MeCardAction opened.
         * @member {boolean} opened
         * @memberof aslivecasino.Bac_MeCardAction
         * @instance
         */
        Bac_MeCardAction.prototype.opened = false;

        /**
         * Creates a new Bac_MeCardAction instance using the specified properties.
         * @function create
         * @memberof aslivecasino.Bac_MeCardAction
         * @static
         * @param {aslivecasino.IBac_MeCardAction=} [properties] Properties to set
         * @returns {aslivecasino.Bac_MeCardAction} Bac_MeCardAction instance
         */
        Bac_MeCardAction.create = function create(properties) {
            return new Bac_MeCardAction(properties);
        };

        /**
         * Encodes the specified Bac_MeCardAction message. Does not implicitly {@link aslivecasino.Bac_MeCardAction.verify|verify} messages.
         * @function encode
         * @memberof aslivecasino.Bac_MeCardAction
         * @static
         * @param {aslivecasino.IBac_MeCardAction} message Bac_MeCardAction message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Bac_MeCardAction.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.room_no);
            if (message.which_side != null && message.hasOwnProperty("which_side"))
                writer.uint32(/* id 2, wireType 0 =*/16).int32(message.which_side);
            if (message.rotated != null && message.hasOwnProperty("rotated"))
                writer.uint32(/* id 3, wireType 0 =*/24).bool(message.rotated);
            if (message.card_index != null && message.hasOwnProperty("card_index"))
                writer.uint32(/* id 4, wireType 0 =*/32).int32(message.card_index);
            if (message.corner != null && message.hasOwnProperty("corner"))
                writer.uint32(/* id 5, wireType 0 =*/40).int32(message.corner);
            if (message.x != null && message.hasOwnProperty("x"))
                writer.uint32(/* id 6, wireType 5 =*/53).float(message.x);
            if (message.y != null && message.hasOwnProperty("y"))
                writer.uint32(/* id 7, wireType 5 =*/61).float(message.y);
            if (message.opened != null && message.hasOwnProperty("opened"))
                writer.uint32(/* id 8, wireType 0 =*/64).bool(message.opened);
            return writer;
        };

        /**
         * Encodes the specified Bac_MeCardAction message, length delimited. Does not implicitly {@link aslivecasino.Bac_MeCardAction.verify|verify} messages.
         * @function encodeDelimited
         * @memberof aslivecasino.Bac_MeCardAction
         * @static
         * @param {aslivecasino.IBac_MeCardAction} message Bac_MeCardAction message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Bac_MeCardAction.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Bac_MeCardAction message from the specified reader or buffer.
         * @function decode
         * @memberof aslivecasino.Bac_MeCardAction
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {aslivecasino.Bac_MeCardAction} Bac_MeCardAction
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Bac_MeCardAction.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.aslivecasino.Bac_MeCardAction();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.room_no = reader.string();
                    break;
                case 2:
                    message.which_side = reader.int32();
                    break;
                case 3:
                    message.rotated = reader.bool();
                    break;
                case 4:
                    message.card_index = reader.int32();
                    break;
                case 5:
                    message.corner = reader.int32();
                    break;
                case 6:
                    message.x = reader.float();
                    break;
                case 7:
                    message.y = reader.float();
                    break;
                case 8:
                    message.opened = reader.bool();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Bac_MeCardAction message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof aslivecasino.Bac_MeCardAction
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {aslivecasino.Bac_MeCardAction} Bac_MeCardAction
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Bac_MeCardAction.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Bac_MeCardAction message.
         * @function verify
         * @memberof aslivecasino.Bac_MeCardAction
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Bac_MeCardAction.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                if (!$util.isString(message.room_no))
                    return "room_no: string expected";
            if (message.which_side != null && message.hasOwnProperty("which_side"))
                if (!$util.isInteger(message.which_side))
                    return "which_side: integer expected";
            if (message.rotated != null && message.hasOwnProperty("rotated"))
                if (typeof message.rotated !== "boolean")
                    return "rotated: boolean expected";
            if (message.card_index != null && message.hasOwnProperty("card_index"))
                if (!$util.isInteger(message.card_index))
                    return "card_index: integer expected";
            if (message.corner != null && message.hasOwnProperty("corner"))
                if (!$util.isInteger(message.corner))
                    return "corner: integer expected";
            if (message.x != null && message.hasOwnProperty("x"))
                if (typeof message.x !== "number")
                    return "x: number expected";
            if (message.y != null && message.hasOwnProperty("y"))
                if (typeof message.y !== "number")
                    return "y: number expected";
            if (message.opened != null && message.hasOwnProperty("opened"))
                if (typeof message.opened !== "boolean")
                    return "opened: boolean expected";
            return null;
        };

        /**
         * Creates a Bac_MeCardAction message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof aslivecasino.Bac_MeCardAction
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {aslivecasino.Bac_MeCardAction} Bac_MeCardAction
         */
        Bac_MeCardAction.fromObject = function fromObject(object) {
            if (object instanceof $root.aslivecasino.Bac_MeCardAction)
                return object;
            var message = new $root.aslivecasino.Bac_MeCardAction();
            if (object.room_no != null)
                message.room_no = String(object.room_no);
            if (object.which_side != null)
                message.which_side = object.which_side | 0;
            if (object.rotated != null)
                message.rotated = Boolean(object.rotated);
            if (object.card_index != null)
                message.card_index = object.card_index | 0;
            if (object.corner != null)
                message.corner = object.corner | 0;
            if (object.x != null)
                message.x = Number(object.x);
            if (object.y != null)
                message.y = Number(object.y);
            if (object.opened != null)
                message.opened = Boolean(object.opened);
            return message;
        };

        /**
         * Creates a plain object from a Bac_MeCardAction message. Also converts values to other types if specified.
         * @function toObject
         * @memberof aslivecasino.Bac_MeCardAction
         * @static
         * @param {aslivecasino.Bac_MeCardAction} message Bac_MeCardAction
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Bac_MeCardAction.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.room_no = "";
                object.which_side = 0;
                object.rotated = false;
                object.card_index = 0;
                object.corner = 0;
                object.x = 0;
                object.y = 0;
                object.opened = false;
            }
            if (message.room_no != null && message.hasOwnProperty("room_no"))
                object.room_no = message.room_no;
            if (message.which_side != null && message.hasOwnProperty("which_side"))
                object.which_side = message.which_side;
            if (message.rotated != null && message.hasOwnProperty("rotated"))
                object.rotated = message.rotated;
            if (message.card_index != null && message.hasOwnProperty("card_index"))
                object.card_index = message.card_index;
            if (message.corner != null && message.hasOwnProperty("corner"))
                object.corner = message.corner;
            if (message.x != null && message.hasOwnProperty("x"))
                object.x = options.json && !isFinite(message.x) ? String(message.x) : message.x;
            if (message.y != null && message.hasOwnProperty("y"))
                object.y = options.json && !isFinite(message.y) ? String(message.y) : message.y;
            if (message.opened != null && message.hasOwnProperty("opened"))
                object.opened = message.opened;
            return object;
        };

        /**
         * Converts this Bac_MeCardAction to JSON.
         * @function toJSON
         * @memberof aslivecasino.Bac_MeCardAction
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Bac_MeCardAction.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return Bac_MeCardAction;
    })();

    aslivecasino.Bac_SlaveDCMessage = (function() {

        /**
         * Properties of a Bac_SlaveDCMessage.
         * @memberof aslivecasino
         * @interface IBac_SlaveDCMessage
         * @property {number|null} [seat_status] Bac_SlaveDCMessage seat_status
         * @property {number|null} [seat_wish_to_ext_bet] Bac_SlaveDCMessage seat_wish_to_ext_bet
         */

        /**
         * Constructs a new Bac_SlaveDCMessage.
         * @memberof aslivecasino
         * @classdesc Represents a Bac_SlaveDCMessage.
         * @implements IBac_SlaveDCMessage
         * @constructor
         * @param {aslivecasino.IBac_SlaveDCMessage=} [properties] Properties to set
         */
        function Bac_SlaveDCMessage(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Bac_SlaveDCMessage seat_status.
         * @member {number} seat_status
         * @memberof aslivecasino.Bac_SlaveDCMessage
         * @instance
         */
        Bac_SlaveDCMessage.prototype.seat_status = 0;

        /**
         * Bac_SlaveDCMessage seat_wish_to_ext_bet.
         * @member {number} seat_wish_to_ext_bet
         * @memberof aslivecasino.Bac_SlaveDCMessage
         * @instance
         */
        Bac_SlaveDCMessage.prototype.seat_wish_to_ext_bet = 0;

        /**
         * Creates a new Bac_SlaveDCMessage instance using the specified properties.
         * @function create
         * @memberof aslivecasino.Bac_SlaveDCMessage
         * @static
         * @param {aslivecasino.IBac_SlaveDCMessage=} [properties] Properties to set
         * @returns {aslivecasino.Bac_SlaveDCMessage} Bac_SlaveDCMessage instance
         */
        Bac_SlaveDCMessage.create = function create(properties) {
            return new Bac_SlaveDCMessage(properties);
        };

        /**
         * Encodes the specified Bac_SlaveDCMessage message. Does not implicitly {@link aslivecasino.Bac_SlaveDCMessage.verify|verify} messages.
         * @function encode
         * @memberof aslivecasino.Bac_SlaveDCMessage
         * @static
         * @param {aslivecasino.IBac_SlaveDCMessage} message Bac_SlaveDCMessage message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Bac_SlaveDCMessage.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.seat_status != null && message.hasOwnProperty("seat_status"))
                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.seat_status);
            if (message.seat_wish_to_ext_bet != null && message.hasOwnProperty("seat_wish_to_ext_bet"))
                writer.uint32(/* id 2, wireType 0 =*/16).int32(message.seat_wish_to_ext_bet);
            return writer;
        };

        /**
         * Encodes the specified Bac_SlaveDCMessage message, length delimited. Does not implicitly {@link aslivecasino.Bac_SlaveDCMessage.verify|verify} messages.
         * @function encodeDelimited
         * @memberof aslivecasino.Bac_SlaveDCMessage
         * @static
         * @param {aslivecasino.IBac_SlaveDCMessage} message Bac_SlaveDCMessage message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Bac_SlaveDCMessage.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Bac_SlaveDCMessage message from the specified reader or buffer.
         * @function decode
         * @memberof aslivecasino.Bac_SlaveDCMessage
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {aslivecasino.Bac_SlaveDCMessage} Bac_SlaveDCMessage
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Bac_SlaveDCMessage.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.aslivecasino.Bac_SlaveDCMessage();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.seat_status = reader.int32();
                    break;
                case 2:
                    message.seat_wish_to_ext_bet = reader.int32();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Bac_SlaveDCMessage message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof aslivecasino.Bac_SlaveDCMessage
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {aslivecasino.Bac_SlaveDCMessage} Bac_SlaveDCMessage
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Bac_SlaveDCMessage.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Bac_SlaveDCMessage message.
         * @function verify
         * @memberof aslivecasino.Bac_SlaveDCMessage
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Bac_SlaveDCMessage.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.seat_status != null && message.hasOwnProperty("seat_status"))
                if (!$util.isInteger(message.seat_status))
                    return "seat_status: integer expected";
            if (message.seat_wish_to_ext_bet != null && message.hasOwnProperty("seat_wish_to_ext_bet"))
                if (!$util.isInteger(message.seat_wish_to_ext_bet))
                    return "seat_wish_to_ext_bet: integer expected";
            return null;
        };

        /**
         * Creates a Bac_SlaveDCMessage message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof aslivecasino.Bac_SlaveDCMessage
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {aslivecasino.Bac_SlaveDCMessage} Bac_SlaveDCMessage
         */
        Bac_SlaveDCMessage.fromObject = function fromObject(object) {
            if (object instanceof $root.aslivecasino.Bac_SlaveDCMessage)
                return object;
            var message = new $root.aslivecasino.Bac_SlaveDCMessage();
            if (object.seat_status != null)
                message.seat_status = object.seat_status | 0;
            if (object.seat_wish_to_ext_bet != null)
                message.seat_wish_to_ext_bet = object.seat_wish_to_ext_bet | 0;
            return message;
        };

        /**
         * Creates a plain object from a Bac_SlaveDCMessage message. Also converts values to other types if specified.
         * @function toObject
         * @memberof aslivecasino.Bac_SlaveDCMessage
         * @static
         * @param {aslivecasino.Bac_SlaveDCMessage} message Bac_SlaveDCMessage
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Bac_SlaveDCMessage.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.seat_status = 0;
                object.seat_wish_to_ext_bet = 0;
            }
            if (message.seat_status != null && message.hasOwnProperty("seat_status"))
                object.seat_status = message.seat_status;
            if (message.seat_wish_to_ext_bet != null && message.hasOwnProperty("seat_wish_to_ext_bet"))
                object.seat_wish_to_ext_bet = message.seat_wish_to_ext_bet;
            return object;
        };

        /**
         * Converts this Bac_SlaveDCMessage to JSON.
         * @function toJSON
         * @memberof aslivecasino.Bac_SlaveDCMessage
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Bac_SlaveDCMessage.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return Bac_SlaveDCMessage;
    })();

    aslivecasino.UpdateInfo = (function() {

        /**
         * Properties of an UpdateInfo.
         * @memberof aslivecasino
         * @interface IUpdateInfo
         * @property {aslivecasino.UpdateInfo.EnumUpdateType|null} [update_type] UpdateInfo update_type
         * @property {aslivecasino.IPlatformInfo|null} [platform_info] UpdateInfo platform_info
         * @property {aslivecasino.IUserBaseInfo|null} [user_base_info] UpdateInfo user_base_info
         * @property {aslivecasino.ILobbyList|null} [lobby_list] UpdateInfo lobby_list
         * @property {aslivecasino.IRoomList|null} [room_list] UpdateInfo room_list
         * @property {aslivecasino.ITurnTo|null} [turn_to] UpdateInfo turn_to
         * @property {aslivecasino.IRoomSeatsInfo|null} [room_seats] UpdateInfo room_seats
         * @property {aslivecasino.IChatMessage|null} [chat_message] UpdateInfo chat_message
         * @property {aslivecasino.IOnSiteUpdate|null} [onsite_update] UpdateInfo onsite_update
         * @property {aslivecasino.IBac_RoomRoutingUpdate_Outside|null} [bac_room_routing_update_outside] UpdateInfo bac_room_routing_update_outside
         * @property {aslivecasino.IBac_EnterRoomInfo|null} [bac_enter_room_info] UpdateInfo bac_enter_room_info
         * @property {aslivecasino.IBac_RoomRoundUpdate|null} [bac_room_round_update] UpdateInfo bac_room_round_update
         * @property {aslivecasino.IBac_RoomRoutingUpdate_Inside|null} [bac_room_routing_update_inside] UpdateInfo bac_room_routing_update_inside
         * @property {aslivecasino.IBac_BeadPlateRoad|null} [bac_bead_plate_road] UpdateInfo bac_bead_plate_road
         * @property {aslivecasino.IBac_UserEnterTable|null} [bac_user_enter_table] UpdateInfo bac_user_enter_table
         * @property {aslivecasino.IBac_UserLeaveTable|null} [bac_user_leave_table] UpdateInfo bac_user_leave_table
         * @property {aslivecasino.IBac_BettingInfo|null} [bac_betting_info] UpdateInfo bac_betting_info
         * @property {aslivecasino.IBac_BriefRoomState|null} [bac_brief_room_state] UpdateInfo bac_brief_room_state
         * @property {aslivecasino.IBac_DetailRoomState|null} [bac_detail_room_state] UpdateInfo bac_detail_room_state
         * @property {aslivecasino.IBac_BetSettlementResult|null} [bac_settlement_result] UpdateInfo bac_settlement_result
         * @property {aslivecasino.IBac_MeCardAction|null} [bac_me_card_action] UpdateInfo bac_me_card_action
         * @property {aslivecasino.IBac_SlaveDCMessage|null} [bac_slave_dc_message] UpdateInfo bac_slave_dc_message
         */

        /**
         * Constructs a new UpdateInfo.
         * @memberof aslivecasino
         * @classdesc Represents an UpdateInfo.
         * @implements IUpdateInfo
         * @constructor
         * @param {aslivecasino.IUpdateInfo=} [properties] Properties to set
         */
        function UpdateInfo(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * UpdateInfo update_type.
         * @member {aslivecasino.UpdateInfo.EnumUpdateType} update_type
         * @memberof aslivecasino.UpdateInfo
         * @instance
         */
        UpdateInfo.prototype.update_type = 0;

        /**
         * UpdateInfo platform_info.
         * @member {aslivecasino.IPlatformInfo|null|undefined} platform_info
         * @memberof aslivecasino.UpdateInfo
         * @instance
         */
        UpdateInfo.prototype.platform_info = null;

        /**
         * UpdateInfo user_base_info.
         * @member {aslivecasino.IUserBaseInfo|null|undefined} user_base_info
         * @memberof aslivecasino.UpdateInfo
         * @instance
         */
        UpdateInfo.prototype.user_base_info = null;

        /**
         * UpdateInfo lobby_list.
         * @member {aslivecasino.ILobbyList|null|undefined} lobby_list
         * @memberof aslivecasino.UpdateInfo
         * @instance
         */
        UpdateInfo.prototype.lobby_list = null;

        /**
         * UpdateInfo room_list.
         * @member {aslivecasino.IRoomList|null|undefined} room_list
         * @memberof aslivecasino.UpdateInfo
         * @instance
         */
        UpdateInfo.prototype.room_list = null;

        /**
         * UpdateInfo turn_to.
         * @member {aslivecasino.ITurnTo|null|undefined} turn_to
         * @memberof aslivecasino.UpdateInfo
         * @instance
         */
        UpdateInfo.prototype.turn_to = null;

        /**
         * UpdateInfo room_seats.
         * @member {aslivecasino.IRoomSeatsInfo|null|undefined} room_seats
         * @memberof aslivecasino.UpdateInfo
         * @instance
         */
        UpdateInfo.prototype.room_seats = null;

        /**
         * UpdateInfo chat_message.
         * @member {aslivecasino.IChatMessage|null|undefined} chat_message
         * @memberof aslivecasino.UpdateInfo
         * @instance
         */
        UpdateInfo.prototype.chat_message = null;

        /**
         * UpdateInfo onsite_update.
         * @member {aslivecasino.IOnSiteUpdate|null|undefined} onsite_update
         * @memberof aslivecasino.UpdateInfo
         * @instance
         */
        UpdateInfo.prototype.onsite_update = null;

        /**
         * UpdateInfo bac_room_routing_update_outside.
         * @member {aslivecasino.IBac_RoomRoutingUpdate_Outside|null|undefined} bac_room_routing_update_outside
         * @memberof aslivecasino.UpdateInfo
         * @instance
         */
        UpdateInfo.prototype.bac_room_routing_update_outside = null;

        /**
         * UpdateInfo bac_enter_room_info.
         * @member {aslivecasino.IBac_EnterRoomInfo|null|undefined} bac_enter_room_info
         * @memberof aslivecasino.UpdateInfo
         * @instance
         */
        UpdateInfo.prototype.bac_enter_room_info = null;

        /**
         * UpdateInfo bac_room_round_update.
         * @member {aslivecasino.IBac_RoomRoundUpdate|null|undefined} bac_room_round_update
         * @memberof aslivecasino.UpdateInfo
         * @instance
         */
        UpdateInfo.prototype.bac_room_round_update = null;

        /**
         * UpdateInfo bac_room_routing_update_inside.
         * @member {aslivecasino.IBac_RoomRoutingUpdate_Inside|null|undefined} bac_room_routing_update_inside
         * @memberof aslivecasino.UpdateInfo
         * @instance
         */
        UpdateInfo.prototype.bac_room_routing_update_inside = null;

        /**
         * UpdateInfo bac_bead_plate_road.
         * @member {aslivecasino.IBac_BeadPlateRoad|null|undefined} bac_bead_plate_road
         * @memberof aslivecasino.UpdateInfo
         * @instance
         */
        UpdateInfo.prototype.bac_bead_plate_road = null;

        /**
         * UpdateInfo bac_user_enter_table.
         * @member {aslivecasino.IBac_UserEnterTable|null|undefined} bac_user_enter_table
         * @memberof aslivecasino.UpdateInfo
         * @instance
         */
        UpdateInfo.prototype.bac_user_enter_table = null;

        /**
         * UpdateInfo bac_user_leave_table.
         * @member {aslivecasino.IBac_UserLeaveTable|null|undefined} bac_user_leave_table
         * @memberof aslivecasino.UpdateInfo
         * @instance
         */
        UpdateInfo.prototype.bac_user_leave_table = null;

        /**
         * UpdateInfo bac_betting_info.
         * @member {aslivecasino.IBac_BettingInfo|null|undefined} bac_betting_info
         * @memberof aslivecasino.UpdateInfo
         * @instance
         */
        UpdateInfo.prototype.bac_betting_info = null;

        /**
         * UpdateInfo bac_brief_room_state.
         * @member {aslivecasino.IBac_BriefRoomState|null|undefined} bac_brief_room_state
         * @memberof aslivecasino.UpdateInfo
         * @instance
         */
        UpdateInfo.prototype.bac_brief_room_state = null;

        /**
         * UpdateInfo bac_detail_room_state.
         * @member {aslivecasino.IBac_DetailRoomState|null|undefined} bac_detail_room_state
         * @memberof aslivecasino.UpdateInfo
         * @instance
         */
        UpdateInfo.prototype.bac_detail_room_state = null;

        /**
         * UpdateInfo bac_settlement_result.
         * @member {aslivecasino.IBac_BetSettlementResult|null|undefined} bac_settlement_result
         * @memberof aslivecasino.UpdateInfo
         * @instance
         */
        UpdateInfo.prototype.bac_settlement_result = null;

        /**
         * UpdateInfo bac_me_card_action.
         * @member {aslivecasino.IBac_MeCardAction|null|undefined} bac_me_card_action
         * @memberof aslivecasino.UpdateInfo
         * @instance
         */
        UpdateInfo.prototype.bac_me_card_action = null;

        /**
         * UpdateInfo bac_slave_dc_message.
         * @member {aslivecasino.IBac_SlaveDCMessage|null|undefined} bac_slave_dc_message
         * @memberof aslivecasino.UpdateInfo
         * @instance
         */
        UpdateInfo.prototype.bac_slave_dc_message = null;

        // OneOf field names bound to virtual getters and setters
        var $oneOfFields;

        /**
         * UpdateInfo update_oneof.
         * @member {"platform_info"|"user_base_info"|"lobby_list"|"room_list"|"turn_to"|"room_seats"|"chat_message"|"onsite_update"|"bac_room_routing_update_outside"|"bac_enter_room_info"|"bac_room_round_update"|"bac_room_routing_update_inside"|"bac_bead_plate_road"|"bac_user_enter_table"|"bac_user_leave_table"|"bac_betting_info"|"bac_brief_room_state"|"bac_detail_room_state"|"bac_settlement_result"|"bac_me_card_action"|"bac_slave_dc_message"|undefined} update_oneof
         * @memberof aslivecasino.UpdateInfo
         * @instance
         */
        Object.defineProperty(UpdateInfo.prototype, "update_oneof", {
            get: $util.oneOfGetter($oneOfFields = ["platform_info", "user_base_info", "lobby_list", "room_list", "turn_to", "room_seats", "chat_message", "onsite_update", "bac_room_routing_update_outside", "bac_enter_room_info", "bac_room_round_update", "bac_room_routing_update_inside", "bac_bead_plate_road", "bac_user_enter_table", "bac_user_leave_table", "bac_betting_info", "bac_brief_room_state", "bac_detail_room_state", "bac_settlement_result", "bac_me_card_action", "bac_slave_dc_message"]),
            set: $util.oneOfSetter($oneOfFields)
        });

        /**
         * Creates a new UpdateInfo instance using the specified properties.
         * @function create
         * @memberof aslivecasino.UpdateInfo
         * @static
         * @param {aslivecasino.IUpdateInfo=} [properties] Properties to set
         * @returns {aslivecasino.UpdateInfo} UpdateInfo instance
         */
        UpdateInfo.create = function create(properties) {
            return new UpdateInfo(properties);
        };

        /**
         * Encodes the specified UpdateInfo message. Does not implicitly {@link aslivecasino.UpdateInfo.verify|verify} messages.
         * @function encode
         * @memberof aslivecasino.UpdateInfo
         * @static
         * @param {aslivecasino.IUpdateInfo} message UpdateInfo message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        UpdateInfo.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.update_type != null && message.hasOwnProperty("update_type"))
                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.update_type);
            if (message.platform_info != null && message.hasOwnProperty("platform_info"))
                $root.aslivecasino.PlatformInfo.encode(message.platform_info, writer.uint32(/* id 2, wireType 2 =*/18).fork()).ldelim();
            if (message.user_base_info != null && message.hasOwnProperty("user_base_info"))
                $root.aslivecasino.UserBaseInfo.encode(message.user_base_info, writer.uint32(/* id 3, wireType 2 =*/26).fork()).ldelim();
            if (message.lobby_list != null && message.hasOwnProperty("lobby_list"))
                $root.aslivecasino.LobbyList.encode(message.lobby_list, writer.uint32(/* id 4, wireType 2 =*/34).fork()).ldelim();
            if (message.room_list != null && message.hasOwnProperty("room_list"))
                $root.aslivecasino.RoomList.encode(message.room_list, writer.uint32(/* id 5, wireType 2 =*/42).fork()).ldelim();
            if (message.turn_to != null && message.hasOwnProperty("turn_to"))
                $root.aslivecasino.TurnTo.encode(message.turn_to, writer.uint32(/* id 6, wireType 2 =*/50).fork()).ldelim();
            if (message.room_seats != null && message.hasOwnProperty("room_seats"))
                $root.aslivecasino.RoomSeatsInfo.encode(message.room_seats, writer.uint32(/* id 7, wireType 2 =*/58).fork()).ldelim();
            if (message.chat_message != null && message.hasOwnProperty("chat_message"))
                $root.aslivecasino.ChatMessage.encode(message.chat_message, writer.uint32(/* id 8, wireType 2 =*/66).fork()).ldelim();
            if (message.onsite_update != null && message.hasOwnProperty("onsite_update"))
                $root.aslivecasino.OnSiteUpdate.encode(message.onsite_update, writer.uint32(/* id 10, wireType 2 =*/82).fork()).ldelim();
            if (message.bac_room_routing_update_outside != null && message.hasOwnProperty("bac_room_routing_update_outside"))
                $root.aslivecasino.Bac_RoomRoutingUpdate_Outside.encode(message.bac_room_routing_update_outside, writer.uint32(/* id 51, wireType 2 =*/410).fork()).ldelim();
            if (message.bac_enter_room_info != null && message.hasOwnProperty("bac_enter_room_info"))
                $root.aslivecasino.Bac_EnterRoomInfo.encode(message.bac_enter_room_info, writer.uint32(/* id 52, wireType 2 =*/418).fork()).ldelim();
            if (message.bac_room_round_update != null && message.hasOwnProperty("bac_room_round_update"))
                $root.aslivecasino.Bac_RoomRoundUpdate.encode(message.bac_room_round_update, writer.uint32(/* id 53, wireType 2 =*/426).fork()).ldelim();
            if (message.bac_room_routing_update_inside != null && message.hasOwnProperty("bac_room_routing_update_inside"))
                $root.aslivecasino.Bac_RoomRoutingUpdate_Inside.encode(message.bac_room_routing_update_inside, writer.uint32(/* id 54, wireType 2 =*/434).fork()).ldelim();
            if (message.bac_bead_plate_road != null && message.hasOwnProperty("bac_bead_plate_road"))
                $root.aslivecasino.Bac_BeadPlateRoad.encode(message.bac_bead_plate_road, writer.uint32(/* id 56, wireType 2 =*/450).fork()).ldelim();
            if (message.bac_user_enter_table != null && message.hasOwnProperty("bac_user_enter_table"))
                $root.aslivecasino.Bac_UserEnterTable.encode(message.bac_user_enter_table, writer.uint32(/* id 57, wireType 2 =*/458).fork()).ldelim();
            if (message.bac_user_leave_table != null && message.hasOwnProperty("bac_user_leave_table"))
                $root.aslivecasino.Bac_UserLeaveTable.encode(message.bac_user_leave_table, writer.uint32(/* id 58, wireType 2 =*/466).fork()).ldelim();
            if (message.bac_betting_info != null && message.hasOwnProperty("bac_betting_info"))
                $root.aslivecasino.Bac_BettingInfo.encode(message.bac_betting_info, writer.uint32(/* id 59, wireType 2 =*/474).fork()).ldelim();
            if (message.bac_brief_room_state != null && message.hasOwnProperty("bac_brief_room_state"))
                $root.aslivecasino.Bac_BriefRoomState.encode(message.bac_brief_room_state, writer.uint32(/* id 60, wireType 2 =*/482).fork()).ldelim();
            if (message.bac_detail_room_state != null && message.hasOwnProperty("bac_detail_room_state"))
                $root.aslivecasino.Bac_DetailRoomState.encode(message.bac_detail_room_state, writer.uint32(/* id 61, wireType 2 =*/490).fork()).ldelim();
            if (message.bac_settlement_result != null && message.hasOwnProperty("bac_settlement_result"))
                $root.aslivecasino.Bac_BetSettlementResult.encode(message.bac_settlement_result, writer.uint32(/* id 62, wireType 2 =*/498).fork()).ldelim();
            if (message.bac_me_card_action != null && message.hasOwnProperty("bac_me_card_action"))
                $root.aslivecasino.Bac_MeCardAction.encode(message.bac_me_card_action, writer.uint32(/* id 64, wireType 2 =*/514).fork()).ldelim();
            if (message.bac_slave_dc_message != null && message.hasOwnProperty("bac_slave_dc_message"))
                $root.aslivecasino.Bac_SlaveDCMessage.encode(message.bac_slave_dc_message, writer.uint32(/* id 65, wireType 2 =*/522).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified UpdateInfo message, length delimited. Does not implicitly {@link aslivecasino.UpdateInfo.verify|verify} messages.
         * @function encodeDelimited
         * @memberof aslivecasino.UpdateInfo
         * @static
         * @param {aslivecasino.IUpdateInfo} message UpdateInfo message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        UpdateInfo.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes an UpdateInfo message from the specified reader or buffer.
         * @function decode
         * @memberof aslivecasino.UpdateInfo
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {aslivecasino.UpdateInfo} UpdateInfo
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        UpdateInfo.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.aslivecasino.UpdateInfo();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.update_type = reader.int32();
                    break;
                case 2:
                    message.platform_info = $root.aslivecasino.PlatformInfo.decode(reader, reader.uint32());
                    break;
                case 3:
                    message.user_base_info = $root.aslivecasino.UserBaseInfo.decode(reader, reader.uint32());
                    break;
                case 4:
                    message.lobby_list = $root.aslivecasino.LobbyList.decode(reader, reader.uint32());
                    break;
                case 5:
                    message.room_list = $root.aslivecasino.RoomList.decode(reader, reader.uint32());
                    break;
                case 6:
                    message.turn_to = $root.aslivecasino.TurnTo.decode(reader, reader.uint32());
                    break;
                case 7:
                    message.room_seats = $root.aslivecasino.RoomSeatsInfo.decode(reader, reader.uint32());
                    break;
                case 8:
                    message.chat_message = $root.aslivecasino.ChatMessage.decode(reader, reader.uint32());
                    break;
                case 10:
                    message.onsite_update = $root.aslivecasino.OnSiteUpdate.decode(reader, reader.uint32());
                    break;
                case 51:
                    message.bac_room_routing_update_outside = $root.aslivecasino.Bac_RoomRoutingUpdate_Outside.decode(reader, reader.uint32());
                    break;
                case 52:
                    message.bac_enter_room_info = $root.aslivecasino.Bac_EnterRoomInfo.decode(reader, reader.uint32());
                    break;
                case 53:
                    message.bac_room_round_update = $root.aslivecasino.Bac_RoomRoundUpdate.decode(reader, reader.uint32());
                    break;
                case 54:
                    message.bac_room_routing_update_inside = $root.aslivecasino.Bac_RoomRoutingUpdate_Inside.decode(reader, reader.uint32());
                    break;
                case 56:
                    message.bac_bead_plate_road = $root.aslivecasino.Bac_BeadPlateRoad.decode(reader, reader.uint32());
                    break;
                case 57:
                    message.bac_user_enter_table = $root.aslivecasino.Bac_UserEnterTable.decode(reader, reader.uint32());
                    break;
                case 58:
                    message.bac_user_leave_table = $root.aslivecasino.Bac_UserLeaveTable.decode(reader, reader.uint32());
                    break;
                case 59:
                    message.bac_betting_info = $root.aslivecasino.Bac_BettingInfo.decode(reader, reader.uint32());
                    break;
                case 60:
                    message.bac_brief_room_state = $root.aslivecasino.Bac_BriefRoomState.decode(reader, reader.uint32());
                    break;
                case 61:
                    message.bac_detail_room_state = $root.aslivecasino.Bac_DetailRoomState.decode(reader, reader.uint32());
                    break;
                case 62:
                    message.bac_settlement_result = $root.aslivecasino.Bac_BetSettlementResult.decode(reader, reader.uint32());
                    break;
                case 64:
                    message.bac_me_card_action = $root.aslivecasino.Bac_MeCardAction.decode(reader, reader.uint32());
                    break;
                case 65:
                    message.bac_slave_dc_message = $root.aslivecasino.Bac_SlaveDCMessage.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes an UpdateInfo message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof aslivecasino.UpdateInfo
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {aslivecasino.UpdateInfo} UpdateInfo
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        UpdateInfo.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies an UpdateInfo message.
         * @function verify
         * @memberof aslivecasino.UpdateInfo
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        UpdateInfo.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            var properties = {};
            if (message.update_type != null && message.hasOwnProperty("update_type"))
                switch (message.update_type) {
                default:
                    return "update_type: enum value expected";
                case 0:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 10:
                case 51:
                case 52:
                case 53:
                case 54:
                case 56:
                case 57:
                case 58:
                case 59:
                case 60:
                case 61:
                case 62:
                case 64:
                case 65:
                    break;
                }
            if (message.platform_info != null && message.hasOwnProperty("platform_info")) {
                properties.update_oneof = 1;
                {
                    var error = $root.aslivecasino.PlatformInfo.verify(message.platform_info);
                    if (error)
                        return "platform_info." + error;
                }
            }
            if (message.user_base_info != null && message.hasOwnProperty("user_base_info")) {
                if (properties.update_oneof === 1)
                    return "update_oneof: multiple values";
                properties.update_oneof = 1;
                {
                    var error = $root.aslivecasino.UserBaseInfo.verify(message.user_base_info);
                    if (error)
                        return "user_base_info." + error;
                }
            }
            if (message.lobby_list != null && message.hasOwnProperty("lobby_list")) {
                if (properties.update_oneof === 1)
                    return "update_oneof: multiple values";
                properties.update_oneof = 1;
                {
                    var error = $root.aslivecasino.LobbyList.verify(message.lobby_list);
                    if (error)
                        return "lobby_list." + error;
                }
            }
            if (message.room_list != null && message.hasOwnProperty("room_list")) {
                if (properties.update_oneof === 1)
                    return "update_oneof: multiple values";
                properties.update_oneof = 1;
                {
                    var error = $root.aslivecasino.RoomList.verify(message.room_list);
                    if (error)
                        return "room_list." + error;
                }
            }
            if (message.turn_to != null && message.hasOwnProperty("turn_to")) {
                if (properties.update_oneof === 1)
                    return "update_oneof: multiple values";
                properties.update_oneof = 1;
                {
                    var error = $root.aslivecasino.TurnTo.verify(message.turn_to);
                    if (error)
                        return "turn_to." + error;
                }
            }
            if (message.room_seats != null && message.hasOwnProperty("room_seats")) {
                if (properties.update_oneof === 1)
                    return "update_oneof: multiple values";
                properties.update_oneof = 1;
                {
                    var error = $root.aslivecasino.RoomSeatsInfo.verify(message.room_seats);
                    if (error)
                        return "room_seats." + error;
                }
            }
            if (message.chat_message != null && message.hasOwnProperty("chat_message")) {
                if (properties.update_oneof === 1)
                    return "update_oneof: multiple values";
                properties.update_oneof = 1;
                {
                    var error = $root.aslivecasino.ChatMessage.verify(message.chat_message);
                    if (error)
                        return "chat_message." + error;
                }
            }
            if (message.onsite_update != null && message.hasOwnProperty("onsite_update")) {
                if (properties.update_oneof === 1)
                    return "update_oneof: multiple values";
                properties.update_oneof = 1;
                {
                    var error = $root.aslivecasino.OnSiteUpdate.verify(message.onsite_update);
                    if (error)
                        return "onsite_update." + error;
                }
            }
            if (message.bac_room_routing_update_outside != null && message.hasOwnProperty("bac_room_routing_update_outside")) {
                if (properties.update_oneof === 1)
                    return "update_oneof: multiple values";
                properties.update_oneof = 1;
                {
                    var error = $root.aslivecasino.Bac_RoomRoutingUpdate_Outside.verify(message.bac_room_routing_update_outside);
                    if (error)
                        return "bac_room_routing_update_outside." + error;
                }
            }
            if (message.bac_enter_room_info != null && message.hasOwnProperty("bac_enter_room_info")) {
                if (properties.update_oneof === 1)
                    return "update_oneof: multiple values";
                properties.update_oneof = 1;
                {
                    var error = $root.aslivecasino.Bac_EnterRoomInfo.verify(message.bac_enter_room_info);
                    if (error)
                        return "bac_enter_room_info." + error;
                }
            }
            if (message.bac_room_round_update != null && message.hasOwnProperty("bac_room_round_update")) {
                if (properties.update_oneof === 1)
                    return "update_oneof: multiple values";
                properties.update_oneof = 1;
                {
                    var error = $root.aslivecasino.Bac_RoomRoundUpdate.verify(message.bac_room_round_update);
                    if (error)
                        return "bac_room_round_update." + error;
                }
            }
            if (message.bac_room_routing_update_inside != null && message.hasOwnProperty("bac_room_routing_update_inside")) {
                if (properties.update_oneof === 1)
                    return "update_oneof: multiple values";
                properties.update_oneof = 1;
                {
                    var error = $root.aslivecasino.Bac_RoomRoutingUpdate_Inside.verify(message.bac_room_routing_update_inside);
                    if (error)
                        return "bac_room_routing_update_inside." + error;
                }
            }
            if (message.bac_bead_plate_road != null && message.hasOwnProperty("bac_bead_plate_road")) {
                if (properties.update_oneof === 1)
                    return "update_oneof: multiple values";
                properties.update_oneof = 1;
                {
                    var error = $root.aslivecasino.Bac_BeadPlateRoad.verify(message.bac_bead_plate_road);
                    if (error)
                        return "bac_bead_plate_road." + error;
                }
            }
            if (message.bac_user_enter_table != null && message.hasOwnProperty("bac_user_enter_table")) {
                if (properties.update_oneof === 1)
                    return "update_oneof: multiple values";
                properties.update_oneof = 1;
                {
                    var error = $root.aslivecasino.Bac_UserEnterTable.verify(message.bac_user_enter_table);
                    if (error)
                        return "bac_user_enter_table." + error;
                }
            }
            if (message.bac_user_leave_table != null && message.hasOwnProperty("bac_user_leave_table")) {
                if (properties.update_oneof === 1)
                    return "update_oneof: multiple values";
                properties.update_oneof = 1;
                {
                    var error = $root.aslivecasino.Bac_UserLeaveTable.verify(message.bac_user_leave_table);
                    if (error)
                        return "bac_user_leave_table." + error;
                }
            }
            if (message.bac_betting_info != null && message.hasOwnProperty("bac_betting_info")) {
                if (properties.update_oneof === 1)
                    return "update_oneof: multiple values";
                properties.update_oneof = 1;
                {
                    var error = $root.aslivecasino.Bac_BettingInfo.verify(message.bac_betting_info);
                    if (error)
                        return "bac_betting_info." + error;
                }
            }
            if (message.bac_brief_room_state != null && message.hasOwnProperty("bac_brief_room_state")) {
                if (properties.update_oneof === 1)
                    return "update_oneof: multiple values";
                properties.update_oneof = 1;
                {
                    var error = $root.aslivecasino.Bac_BriefRoomState.verify(message.bac_brief_room_state);
                    if (error)
                        return "bac_brief_room_state." + error;
                }
            }
            if (message.bac_detail_room_state != null && message.hasOwnProperty("bac_detail_room_state")) {
                if (properties.update_oneof === 1)
                    return "update_oneof: multiple values";
                properties.update_oneof = 1;
                {
                    var error = $root.aslivecasino.Bac_DetailRoomState.verify(message.bac_detail_room_state);
                    if (error)
                        return "bac_detail_room_state." + error;
                }
            }
            if (message.bac_settlement_result != null && message.hasOwnProperty("bac_settlement_result")) {
                if (properties.update_oneof === 1)
                    return "update_oneof: multiple values";
                properties.update_oneof = 1;
                {
                    var error = $root.aslivecasino.Bac_BetSettlementResult.verify(message.bac_settlement_result);
                    if (error)
                        return "bac_settlement_result." + error;
                }
            }
            if (message.bac_me_card_action != null && message.hasOwnProperty("bac_me_card_action")) {
                if (properties.update_oneof === 1)
                    return "update_oneof: multiple values";
                properties.update_oneof = 1;
                {
                    var error = $root.aslivecasino.Bac_MeCardAction.verify(message.bac_me_card_action);
                    if (error)
                        return "bac_me_card_action." + error;
                }
            }
            if (message.bac_slave_dc_message != null && message.hasOwnProperty("bac_slave_dc_message")) {
                if (properties.update_oneof === 1)
                    return "update_oneof: multiple values";
                properties.update_oneof = 1;
                {
                    var error = $root.aslivecasino.Bac_SlaveDCMessage.verify(message.bac_slave_dc_message);
                    if (error)
                        return "bac_slave_dc_message." + error;
                }
            }
            return null;
        };

        /**
         * Creates an UpdateInfo message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof aslivecasino.UpdateInfo
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {aslivecasino.UpdateInfo} UpdateInfo
         */
        UpdateInfo.fromObject = function fromObject(object) {
            if (object instanceof $root.aslivecasino.UpdateInfo)
                return object;
            var message = new $root.aslivecasino.UpdateInfo();
            switch (object.update_type) {
            case "UNKNOWN":
            case 0:
                message.update_type = 0;
                break;
            case "PLATFORMINFO":
            case 2:
                message.update_type = 2;
                break;
            case "USERBASEINFO":
            case 3:
                message.update_type = 3;
                break;
            case "LOBBYLIST":
            case 4:
                message.update_type = 4;
                break;
            case "ROOMLIST":
            case 5:
                message.update_type = 5;
                break;
            case "TURNTO":
            case 6:
                message.update_type = 6;
                break;
            case "ROOMSEATSINFO":
            case 7:
                message.update_type = 7;
                break;
            case "CHATMSG":
            case 8:
                message.update_type = 8;
                break;
            case "ONSITE_UPDATE":
            case 10:
                message.update_type = 10;
                break;
            case "BAC_ROOMROUTINGUPDATE_OUTSIDE":
            case 51:
                message.update_type = 51;
                break;
            case "BAC_ENTERROOMINFO":
            case 52:
                message.update_type = 52;
                break;
            case "BAC_ROOMROUNDUPDATE":
            case 53:
                message.update_type = 53;
                break;
            case "BAC_ROOMROUTINGUPDATE_INSIDE":
            case 54:
                message.update_type = 54;
                break;
            case "BAC_BEADPLATEROAD":
            case 56:
                message.update_type = 56;
                break;
            case "BAC_USERENTERTABLE":
            case 57:
                message.update_type = 57;
                break;
            case "BAC_USERLEAVETABLE":
            case 58:
                message.update_type = 58;
                break;
            case "BAC_BETTINGINFO":
            case 59:
                message.update_type = 59;
                break;
            case "BAC_BRIEFROOMSTATE":
            case 60:
                message.update_type = 60;
                break;
            case "BAC_DETAILROOMSTATE":
            case 61:
                message.update_type = 61;
                break;
            case "BAC_SETTLEMENTRESULT":
            case 62:
                message.update_type = 62;
                break;
            case "BAC_MECARDACTION":
            case 64:
                message.update_type = 64;
                break;
            case "BAC_SLAVEDCMESSAGE":
            case 65:
                message.update_type = 65;
                break;
            }
            if (object.platform_info != null) {
                if (typeof object.platform_info !== "object")
                    throw TypeError(".aslivecasino.UpdateInfo.platform_info: object expected");
                message.platform_info = $root.aslivecasino.PlatformInfo.fromObject(object.platform_info);
            }
            if (object.user_base_info != null) {
                if (typeof object.user_base_info !== "object")
                    throw TypeError(".aslivecasino.UpdateInfo.user_base_info: object expected");
                message.user_base_info = $root.aslivecasino.UserBaseInfo.fromObject(object.user_base_info);
            }
            if (object.lobby_list != null) {
                if (typeof object.lobby_list !== "object")
                    throw TypeError(".aslivecasino.UpdateInfo.lobby_list: object expected");
                message.lobby_list = $root.aslivecasino.LobbyList.fromObject(object.lobby_list);
            }
            if (object.room_list != null) {
                if (typeof object.room_list !== "object")
                    throw TypeError(".aslivecasino.UpdateInfo.room_list: object expected");
                message.room_list = $root.aslivecasino.RoomList.fromObject(object.room_list);
            }
            if (object.turn_to != null) {
                if (typeof object.turn_to !== "object")
                    throw TypeError(".aslivecasino.UpdateInfo.turn_to: object expected");
                message.turn_to = $root.aslivecasino.TurnTo.fromObject(object.turn_to);
            }
            if (object.room_seats != null) {
                if (typeof object.room_seats !== "object")
                    throw TypeError(".aslivecasino.UpdateInfo.room_seats: object expected");
                message.room_seats = $root.aslivecasino.RoomSeatsInfo.fromObject(object.room_seats);
            }
            if (object.chat_message != null) {
                if (typeof object.chat_message !== "object")
                    throw TypeError(".aslivecasino.UpdateInfo.chat_message: object expected");
                message.chat_message = $root.aslivecasino.ChatMessage.fromObject(object.chat_message);
            }
            if (object.onsite_update != null) {
                if (typeof object.onsite_update !== "object")
                    throw TypeError(".aslivecasino.UpdateInfo.onsite_update: object expected");
                message.onsite_update = $root.aslivecasino.OnSiteUpdate.fromObject(object.onsite_update);
            }
            if (object.bac_room_routing_update_outside != null) {
                if (typeof object.bac_room_routing_update_outside !== "object")
                    throw TypeError(".aslivecasino.UpdateInfo.bac_room_routing_update_outside: object expected");
                message.bac_room_routing_update_outside = $root.aslivecasino.Bac_RoomRoutingUpdate_Outside.fromObject(object.bac_room_routing_update_outside);
            }
            if (object.bac_enter_room_info != null) {
                if (typeof object.bac_enter_room_info !== "object")
                    throw TypeError(".aslivecasino.UpdateInfo.bac_enter_room_info: object expected");
                message.bac_enter_room_info = $root.aslivecasino.Bac_EnterRoomInfo.fromObject(object.bac_enter_room_info);
            }
            if (object.bac_room_round_update != null) {
                if (typeof object.bac_room_round_update !== "object")
                    throw TypeError(".aslivecasino.UpdateInfo.bac_room_round_update: object expected");
                message.bac_room_round_update = $root.aslivecasino.Bac_RoomRoundUpdate.fromObject(object.bac_room_round_update);
            }
            if (object.bac_room_routing_update_inside != null) {
                if (typeof object.bac_room_routing_update_inside !== "object")
                    throw TypeError(".aslivecasino.UpdateInfo.bac_room_routing_update_inside: object expected");
                message.bac_room_routing_update_inside = $root.aslivecasino.Bac_RoomRoutingUpdate_Inside.fromObject(object.bac_room_routing_update_inside);
            }
            if (object.bac_bead_plate_road != null) {
                if (typeof object.bac_bead_plate_road !== "object")
                    throw TypeError(".aslivecasino.UpdateInfo.bac_bead_plate_road: object expected");
                message.bac_bead_plate_road = $root.aslivecasino.Bac_BeadPlateRoad.fromObject(object.bac_bead_plate_road);
            }
            if (object.bac_user_enter_table != null) {
                if (typeof object.bac_user_enter_table !== "object")
                    throw TypeError(".aslivecasino.UpdateInfo.bac_user_enter_table: object expected");
                message.bac_user_enter_table = $root.aslivecasino.Bac_UserEnterTable.fromObject(object.bac_user_enter_table);
            }
            if (object.bac_user_leave_table != null) {
                if (typeof object.bac_user_leave_table !== "object")
                    throw TypeError(".aslivecasino.UpdateInfo.bac_user_leave_table: object expected");
                message.bac_user_leave_table = $root.aslivecasino.Bac_UserLeaveTable.fromObject(object.bac_user_leave_table);
            }
            if (object.bac_betting_info != null) {
                if (typeof object.bac_betting_info !== "object")
                    throw TypeError(".aslivecasino.UpdateInfo.bac_betting_info: object expected");
                message.bac_betting_info = $root.aslivecasino.Bac_BettingInfo.fromObject(object.bac_betting_info);
            }
            if (object.bac_brief_room_state != null) {
                if (typeof object.bac_brief_room_state !== "object")
                    throw TypeError(".aslivecasino.UpdateInfo.bac_brief_room_state: object expected");
                message.bac_brief_room_state = $root.aslivecasino.Bac_BriefRoomState.fromObject(object.bac_brief_room_state);
            }
            if (object.bac_detail_room_state != null) {
                if (typeof object.bac_detail_room_state !== "object")
                    throw TypeError(".aslivecasino.UpdateInfo.bac_detail_room_state: object expected");
                message.bac_detail_room_state = $root.aslivecasino.Bac_DetailRoomState.fromObject(object.bac_detail_room_state);
            }
            if (object.bac_settlement_result != null) {
                if (typeof object.bac_settlement_result !== "object")
                    throw TypeError(".aslivecasino.UpdateInfo.bac_settlement_result: object expected");
                message.bac_settlement_result = $root.aslivecasino.Bac_BetSettlementResult.fromObject(object.bac_settlement_result);
            }
            if (object.bac_me_card_action != null) {
                if (typeof object.bac_me_card_action !== "object")
                    throw TypeError(".aslivecasino.UpdateInfo.bac_me_card_action: object expected");
                message.bac_me_card_action = $root.aslivecasino.Bac_MeCardAction.fromObject(object.bac_me_card_action);
            }
            if (object.bac_slave_dc_message != null) {
                if (typeof object.bac_slave_dc_message !== "object")
                    throw TypeError(".aslivecasino.UpdateInfo.bac_slave_dc_message: object expected");
                message.bac_slave_dc_message = $root.aslivecasino.Bac_SlaveDCMessage.fromObject(object.bac_slave_dc_message);
            }
            return message;
        };

        /**
         * Creates a plain object from an UpdateInfo message. Also converts values to other types if specified.
         * @function toObject
         * @memberof aslivecasino.UpdateInfo
         * @static
         * @param {aslivecasino.UpdateInfo} message UpdateInfo
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        UpdateInfo.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults)
                object.update_type = options.enums === String ? "UNKNOWN" : 0;
            if (message.update_type != null && message.hasOwnProperty("update_type"))
                object.update_type = options.enums === String ? $root.aslivecasino.UpdateInfo.EnumUpdateType[message.update_type] : message.update_type;
            if (message.platform_info != null && message.hasOwnProperty("platform_info")) {
                object.platform_info = $root.aslivecasino.PlatformInfo.toObject(message.platform_info, options);
                if (options.oneofs)
                    object.update_oneof = "platform_info";
            }
            if (message.user_base_info != null && message.hasOwnProperty("user_base_info")) {
                object.user_base_info = $root.aslivecasino.UserBaseInfo.toObject(message.user_base_info, options);
                if (options.oneofs)
                    object.update_oneof = "user_base_info";
            }
            if (message.lobby_list != null && message.hasOwnProperty("lobby_list")) {
                object.lobby_list = $root.aslivecasino.LobbyList.toObject(message.lobby_list, options);
                if (options.oneofs)
                    object.update_oneof = "lobby_list";
            }
            if (message.room_list != null && message.hasOwnProperty("room_list")) {
                object.room_list = $root.aslivecasino.RoomList.toObject(message.room_list, options);
                if (options.oneofs)
                    object.update_oneof = "room_list";
            }
            if (message.turn_to != null && message.hasOwnProperty("turn_to")) {
                object.turn_to = $root.aslivecasino.TurnTo.toObject(message.turn_to, options);
                if (options.oneofs)
                    object.update_oneof = "turn_to";
            }
            if (message.room_seats != null && message.hasOwnProperty("room_seats")) {
                object.room_seats = $root.aslivecasino.RoomSeatsInfo.toObject(message.room_seats, options);
                if (options.oneofs)
                    object.update_oneof = "room_seats";
            }
            if (message.chat_message != null && message.hasOwnProperty("chat_message")) {
                object.chat_message = $root.aslivecasino.ChatMessage.toObject(message.chat_message, options);
                if (options.oneofs)
                    object.update_oneof = "chat_message";
            }
            if (message.onsite_update != null && message.hasOwnProperty("onsite_update")) {
                object.onsite_update = $root.aslivecasino.OnSiteUpdate.toObject(message.onsite_update, options);
                if (options.oneofs)
                    object.update_oneof = "onsite_update";
            }
            if (message.bac_room_routing_update_outside != null && message.hasOwnProperty("bac_room_routing_update_outside")) {
                object.bac_room_routing_update_outside = $root.aslivecasino.Bac_RoomRoutingUpdate_Outside.toObject(message.bac_room_routing_update_outside, options);
                if (options.oneofs)
                    object.update_oneof = "bac_room_routing_update_outside";
            }
            if (message.bac_enter_room_info != null && message.hasOwnProperty("bac_enter_room_info")) {
                object.bac_enter_room_info = $root.aslivecasino.Bac_EnterRoomInfo.toObject(message.bac_enter_room_info, options);
                if (options.oneofs)
                    object.update_oneof = "bac_enter_room_info";
            }
            if (message.bac_room_round_update != null && message.hasOwnProperty("bac_room_round_update")) {
                object.bac_room_round_update = $root.aslivecasino.Bac_RoomRoundUpdate.toObject(message.bac_room_round_update, options);
                if (options.oneofs)
                    object.update_oneof = "bac_room_round_update";
            }
            if (message.bac_room_routing_update_inside != null && message.hasOwnProperty("bac_room_routing_update_inside")) {
                object.bac_room_routing_update_inside = $root.aslivecasino.Bac_RoomRoutingUpdate_Inside.toObject(message.bac_room_routing_update_inside, options);
                if (options.oneofs)
                    object.update_oneof = "bac_room_routing_update_inside";
            }
            if (message.bac_bead_plate_road != null && message.hasOwnProperty("bac_bead_plate_road")) {
                object.bac_bead_plate_road = $root.aslivecasino.Bac_BeadPlateRoad.toObject(message.bac_bead_plate_road, options);
                if (options.oneofs)
                    object.update_oneof = "bac_bead_plate_road";
            }
            if (message.bac_user_enter_table != null && message.hasOwnProperty("bac_user_enter_table")) {
                object.bac_user_enter_table = $root.aslivecasino.Bac_UserEnterTable.toObject(message.bac_user_enter_table, options);
                if (options.oneofs)
                    object.update_oneof = "bac_user_enter_table";
            }
            if (message.bac_user_leave_table != null && message.hasOwnProperty("bac_user_leave_table")) {
                object.bac_user_leave_table = $root.aslivecasino.Bac_UserLeaveTable.toObject(message.bac_user_leave_table, options);
                if (options.oneofs)
                    object.update_oneof = "bac_user_leave_table";
            }
            if (message.bac_betting_info != null && message.hasOwnProperty("bac_betting_info")) {
                object.bac_betting_info = $root.aslivecasino.Bac_BettingInfo.toObject(message.bac_betting_info, options);
                if (options.oneofs)
                    object.update_oneof = "bac_betting_info";
            }
            if (message.bac_brief_room_state != null && message.hasOwnProperty("bac_brief_room_state")) {
                object.bac_brief_room_state = $root.aslivecasino.Bac_BriefRoomState.toObject(message.bac_brief_room_state, options);
                if (options.oneofs)
                    object.update_oneof = "bac_brief_room_state";
            }
            if (message.bac_detail_room_state != null && message.hasOwnProperty("bac_detail_room_state")) {
                object.bac_detail_room_state = $root.aslivecasino.Bac_DetailRoomState.toObject(message.bac_detail_room_state, options);
                if (options.oneofs)
                    object.update_oneof = "bac_detail_room_state";
            }
            if (message.bac_settlement_result != null && message.hasOwnProperty("bac_settlement_result")) {
                object.bac_settlement_result = $root.aslivecasino.Bac_BetSettlementResult.toObject(message.bac_settlement_result, options);
                if (options.oneofs)
                    object.update_oneof = "bac_settlement_result";
            }
            if (message.bac_me_card_action != null && message.hasOwnProperty("bac_me_card_action")) {
                object.bac_me_card_action = $root.aslivecasino.Bac_MeCardAction.toObject(message.bac_me_card_action, options);
                if (options.oneofs)
                    object.update_oneof = "bac_me_card_action";
            }
            if (message.bac_slave_dc_message != null && message.hasOwnProperty("bac_slave_dc_message")) {
                object.bac_slave_dc_message = $root.aslivecasino.Bac_SlaveDCMessage.toObject(message.bac_slave_dc_message, options);
                if (options.oneofs)
                    object.update_oneof = "bac_slave_dc_message";
            }
            return object;
        };

        /**
         * Converts this UpdateInfo to JSON.
         * @function toJSON
         * @memberof aslivecasino.UpdateInfo
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        UpdateInfo.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * EnumUpdateType enum.
         * @name aslivecasino.UpdateInfo.EnumUpdateType
         * @enum {string}
         * @property {number} UNKNOWN=0 UNKNOWN value
         * @property {number} PLATFORMINFO=2 PLATFORMINFO value
         * @property {number} USERBASEINFO=3 USERBASEINFO value
         * @property {number} LOBBYLIST=4 LOBBYLIST value
         * @property {number} ROOMLIST=5 ROOMLIST value
         * @property {number} TURNTO=6 TURNTO value
         * @property {number} ROOMSEATSINFO=7 ROOMSEATSINFO value
         * @property {number} CHATMSG=8 CHATMSG value
         * @property {number} ONSITE_UPDATE=10 ONSITE_UPDATE value
         * @property {number} BAC_ROOMROUTINGUPDATE_OUTSIDE=51 BAC_ROOMROUTINGUPDATE_OUTSIDE value
         * @property {number} BAC_ENTERROOMINFO=52 BAC_ENTERROOMINFO value
         * @property {number} BAC_ROOMROUNDUPDATE=53 BAC_ROOMROUNDUPDATE value
         * @property {number} BAC_ROOMROUTINGUPDATE_INSIDE=54 BAC_ROOMROUTINGUPDATE_INSIDE value
         * @property {number} BAC_BEADPLATEROAD=56 BAC_BEADPLATEROAD value
         * @property {number} BAC_USERENTERTABLE=57 BAC_USERENTERTABLE value
         * @property {number} BAC_USERLEAVETABLE=58 BAC_USERLEAVETABLE value
         * @property {number} BAC_BETTINGINFO=59 BAC_BETTINGINFO value
         * @property {number} BAC_BRIEFROOMSTATE=60 BAC_BRIEFROOMSTATE value
         * @property {number} BAC_DETAILROOMSTATE=61 BAC_DETAILROOMSTATE value
         * @property {number} BAC_SETTLEMENTRESULT=62 BAC_SETTLEMENTRESULT value
         * @property {number} BAC_MECARDACTION=64 BAC_MECARDACTION value
         * @property {number} BAC_SLAVEDCMESSAGE=65 BAC_SLAVEDCMESSAGE value
         */
        UpdateInfo.EnumUpdateType = (function() {
            var valuesById = {}, values = Object.create(valuesById);
            values[valuesById[0] = "UNKNOWN"] = 0;
            values[valuesById[2] = "PLATFORMINFO"] = 2;
            values[valuesById[3] = "USERBASEINFO"] = 3;
            values[valuesById[4] = "LOBBYLIST"] = 4;
            values[valuesById[5] = "ROOMLIST"] = 5;
            values[valuesById[6] = "TURNTO"] = 6;
            values[valuesById[7] = "ROOMSEATSINFO"] = 7;
            values[valuesById[8] = "CHATMSG"] = 8;
            values[valuesById[10] = "ONSITE_UPDATE"] = 10;
            values[valuesById[51] = "BAC_ROOMROUTINGUPDATE_OUTSIDE"] = 51;
            values[valuesById[52] = "BAC_ENTERROOMINFO"] = 52;
            values[valuesById[53] = "BAC_ROOMROUNDUPDATE"] = 53;
            values[valuesById[54] = "BAC_ROOMROUTINGUPDATE_INSIDE"] = 54;
            values[valuesById[56] = "BAC_BEADPLATEROAD"] = 56;
            values[valuesById[57] = "BAC_USERENTERTABLE"] = 57;
            values[valuesById[58] = "BAC_USERLEAVETABLE"] = 58;
            values[valuesById[59] = "BAC_BETTINGINFO"] = 59;
            values[valuesById[60] = "BAC_BRIEFROOMSTATE"] = 60;
            values[valuesById[61] = "BAC_DETAILROOMSTATE"] = 61;
            values[valuesById[62] = "BAC_SETTLEMENTRESULT"] = 62;
            values[valuesById[64] = "BAC_MECARDACTION"] = 64;
            values[valuesById[65] = "BAC_SLAVEDCMESSAGE"] = 65;
            return values;
        })();

        return UpdateInfo;
    })();

    aslivecasino.NotifyInfo = (function() {

        /**
         * Properties of a NotifyInfo.
         * @memberof aslivecasino
         * @interface INotifyInfo
         * @property {aslivecasino.NotifyInfo.EnumNotifyType|null} [notify_type] NotifyInfo notify_type
         * @property {string|null} [disconn_reason] NotifyInfo disconn_reason
         */

        /**
         * Constructs a new NotifyInfo.
         * @memberof aslivecasino
         * @classdesc Represents a NotifyInfo.
         * @implements INotifyInfo
         * @constructor
         * @param {aslivecasino.INotifyInfo=} [properties] Properties to set
         */
        function NotifyInfo(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * NotifyInfo notify_type.
         * @member {aslivecasino.NotifyInfo.EnumNotifyType} notify_type
         * @memberof aslivecasino.NotifyInfo
         * @instance
         */
        NotifyInfo.prototype.notify_type = 0;

        /**
         * NotifyInfo disconn_reason.
         * @member {string} disconn_reason
         * @memberof aslivecasino.NotifyInfo
         * @instance
         */
        NotifyInfo.prototype.disconn_reason = "";

        // OneOf field names bound to virtual getters and setters
        var $oneOfFields;

        /**
         * NotifyInfo notify_oneof.
         * @member {"disconn_reason"|undefined} notify_oneof
         * @memberof aslivecasino.NotifyInfo
         * @instance
         */
        Object.defineProperty(NotifyInfo.prototype, "notify_oneof", {
            get: $util.oneOfGetter($oneOfFields = ["disconn_reason"]),
            set: $util.oneOfSetter($oneOfFields)
        });

        /**
         * Creates a new NotifyInfo instance using the specified properties.
         * @function create
         * @memberof aslivecasino.NotifyInfo
         * @static
         * @param {aslivecasino.INotifyInfo=} [properties] Properties to set
         * @returns {aslivecasino.NotifyInfo} NotifyInfo instance
         */
        NotifyInfo.create = function create(properties) {
            return new NotifyInfo(properties);
        };

        /**
         * Encodes the specified NotifyInfo message. Does not implicitly {@link aslivecasino.NotifyInfo.verify|verify} messages.
         * @function encode
         * @memberof aslivecasino.NotifyInfo
         * @static
         * @param {aslivecasino.INotifyInfo} message NotifyInfo message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        NotifyInfo.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.notify_type != null && message.hasOwnProperty("notify_type"))
                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.notify_type);
            if (message.disconn_reason != null && message.hasOwnProperty("disconn_reason"))
                writer.uint32(/* id 3, wireType 2 =*/26).string(message.disconn_reason);
            return writer;
        };

        /**
         * Encodes the specified NotifyInfo message, length delimited. Does not implicitly {@link aslivecasino.NotifyInfo.verify|verify} messages.
         * @function encodeDelimited
         * @memberof aslivecasino.NotifyInfo
         * @static
         * @param {aslivecasino.INotifyInfo} message NotifyInfo message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        NotifyInfo.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a NotifyInfo message from the specified reader or buffer.
         * @function decode
         * @memberof aslivecasino.NotifyInfo
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {aslivecasino.NotifyInfo} NotifyInfo
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        NotifyInfo.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.aslivecasino.NotifyInfo();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.notify_type = reader.int32();
                    break;
                case 3:
                    message.disconn_reason = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a NotifyInfo message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof aslivecasino.NotifyInfo
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {aslivecasino.NotifyInfo} NotifyInfo
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        NotifyInfo.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a NotifyInfo message.
         * @function verify
         * @memberof aslivecasino.NotifyInfo
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        NotifyInfo.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            var properties = {};
            if (message.notify_type != null && message.hasOwnProperty("notify_type"))
                switch (message.notify_type) {
                default:
                    return "notify_type: enum value expected";
                case 0:
                case 1:
                case 2:
                case 3:
                    break;
                }
            if (message.disconn_reason != null && message.hasOwnProperty("disconn_reason")) {
                properties.notify_oneof = 1;
                if (!$util.isString(message.disconn_reason))
                    return "disconn_reason: string expected";
            }
            return null;
        };

        /**
         * Creates a NotifyInfo message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof aslivecasino.NotifyInfo
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {aslivecasino.NotifyInfo} NotifyInfo
         */
        NotifyInfo.fromObject = function fromObject(object) {
            if (object instanceof $root.aslivecasino.NotifyInfo)
                return object;
            var message = new $root.aslivecasino.NotifyInfo();
            switch (object.notify_type) {
            case "UNKNOWN":
            case 0:
                message.notify_type = 0;
                break;
            case "TIMEOUT_WILLBEKICKOUT":
            case 1:
                message.notify_type = 1;
                break;
            case "TIMEOUT_GETOUT":
            case 2:
                message.notify_type = 2;
                break;
            case "DISCONNECT":
            case 3:
                message.notify_type = 3;
                break;
            }
            if (object.disconn_reason != null)
                message.disconn_reason = String(object.disconn_reason);
            return message;
        };

        /**
         * Creates a plain object from a NotifyInfo message. Also converts values to other types if specified.
         * @function toObject
         * @memberof aslivecasino.NotifyInfo
         * @static
         * @param {aslivecasino.NotifyInfo} message NotifyInfo
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        NotifyInfo.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults)
                object.notify_type = options.enums === String ? "UNKNOWN" : 0;
            if (message.notify_type != null && message.hasOwnProperty("notify_type"))
                object.notify_type = options.enums === String ? $root.aslivecasino.NotifyInfo.EnumNotifyType[message.notify_type] : message.notify_type;
            if (message.disconn_reason != null && message.hasOwnProperty("disconn_reason")) {
                object.disconn_reason = message.disconn_reason;
                if (options.oneofs)
                    object.notify_oneof = "disconn_reason";
            }
            return object;
        };

        /**
         * Converts this NotifyInfo to JSON.
         * @function toJSON
         * @memberof aslivecasino.NotifyInfo
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        NotifyInfo.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * EnumNotifyType enum.
         * @name aslivecasino.NotifyInfo.EnumNotifyType
         * @enum {string}
         * @property {number} UNKNOWN=0 UNKNOWN value
         * @property {number} TIMEOUT_WILLBEKICKOUT=1 TIMEOUT_WILLBEKICKOUT value
         * @property {number} TIMEOUT_GETOUT=2 TIMEOUT_GETOUT value
         * @property {number} DISCONNECT=3 DISCONNECT value
         */
        NotifyInfo.EnumNotifyType = (function() {
            var valuesById = {}, values = Object.create(valuesById);
            values[valuesById[0] = "UNKNOWN"] = 0;
            values[valuesById[1] = "TIMEOUT_WILLBEKICKOUT"] = 1;
            values[valuesById[2] = "TIMEOUT_GETOUT"] = 2;
            values[valuesById[3] = "DISCONNECT"] = 3;
            return values;
        })();

        return NotifyInfo;
    })();

    aslivecasino.Response = (function() {

        /**
         * Properties of a Response.
         * @memberof aslivecasino
         * @interface IResponse
         * @property {number|null} [error_no] Response error_no
         * @property {string|null} [error_string] Response error_string
         * @property {string|null} [response_string] Response response_string
         * @property {Array.<aslivecasino.IUpdateInfo>|null} [update_list] Response update_list
         * @property {Array.<aslivecasino.INotifyInfo>|null} [notify_list] Response notify_list
         */

        /**
         * Constructs a new Response.
         * @memberof aslivecasino
         * @classdesc Represents a Response.
         * @implements IResponse
         * @constructor
         * @param {aslivecasino.IResponse=} [properties] Properties to set
         */
        function Response(properties) {
            this.update_list = [];
            this.notify_list = [];
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Response error_no.
         * @member {number} error_no
         * @memberof aslivecasino.Response
         * @instance
         */
        Response.prototype.error_no = 0;

        /**
         * Response error_string.
         * @member {string} error_string
         * @memberof aslivecasino.Response
         * @instance
         */
        Response.prototype.error_string = "";

        /**
         * Response response_string.
         * @member {string} response_string
         * @memberof aslivecasino.Response
         * @instance
         */
        Response.prototype.response_string = "";

        /**
         * Response update_list.
         * @member {Array.<aslivecasino.IUpdateInfo>} update_list
         * @memberof aslivecasino.Response
         * @instance
         */
        Response.prototype.update_list = $util.emptyArray;

        /**
         * Response notify_list.
         * @member {Array.<aslivecasino.INotifyInfo>} notify_list
         * @memberof aslivecasino.Response
         * @instance
         */
        Response.prototype.notify_list = $util.emptyArray;

        /**
         * Creates a new Response instance using the specified properties.
         * @function create
         * @memberof aslivecasino.Response
         * @static
         * @param {aslivecasino.IResponse=} [properties] Properties to set
         * @returns {aslivecasino.Response} Response instance
         */
        Response.create = function create(properties) {
            return new Response(properties);
        };

        /**
         * Encodes the specified Response message. Does not implicitly {@link aslivecasino.Response.verify|verify} messages.
         * @function encode
         * @memberof aslivecasino.Response
         * @static
         * @param {aslivecasino.IResponse} message Response message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Response.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.error_no != null && message.hasOwnProperty("error_no"))
                writer.uint32(/* id 1, wireType 0 =*/8).sint32(message.error_no);
            if (message.error_string != null && message.hasOwnProperty("error_string"))
                writer.uint32(/* id 2, wireType 2 =*/18).string(message.error_string);
            if (message.response_string != null && message.hasOwnProperty("response_string"))
                writer.uint32(/* id 3, wireType 2 =*/26).string(message.response_string);
            if (message.update_list != null && message.update_list.length)
                for (var i = 0; i < message.update_list.length; ++i)
                    $root.aslivecasino.UpdateInfo.encode(message.update_list[i], writer.uint32(/* id 4, wireType 2 =*/34).fork()).ldelim();
            if (message.notify_list != null && message.notify_list.length)
                for (var i = 0; i < message.notify_list.length; ++i)
                    $root.aslivecasino.NotifyInfo.encode(message.notify_list[i], writer.uint32(/* id 5, wireType 2 =*/42).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified Response message, length delimited. Does not implicitly {@link aslivecasino.Response.verify|verify} messages.
         * @function encodeDelimited
         * @memberof aslivecasino.Response
         * @static
         * @param {aslivecasino.IResponse} message Response message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Response.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Response message from the specified reader or buffer.
         * @function decode
         * @memberof aslivecasino.Response
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {aslivecasino.Response} Response
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Response.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.aslivecasino.Response();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.error_no = reader.sint32();
                    break;
                case 2:
                    message.error_string = reader.string();
                    break;
                case 3:
                    message.response_string = reader.string();
                    break;
                case 4:
                    if (!(message.update_list && message.update_list.length))
                        message.update_list = [];
                    message.update_list.push($root.aslivecasino.UpdateInfo.decode(reader, reader.uint32()));
                    break;
                case 5:
                    if (!(message.notify_list && message.notify_list.length))
                        message.notify_list = [];
                    message.notify_list.push($root.aslivecasino.NotifyInfo.decode(reader, reader.uint32()));
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Response message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof aslivecasino.Response
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {aslivecasino.Response} Response
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Response.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Response message.
         * @function verify
         * @memberof aslivecasino.Response
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Response.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.error_no != null && message.hasOwnProperty("error_no"))
                if (!$util.isInteger(message.error_no))
                    return "error_no: integer expected";
            if (message.error_string != null && message.hasOwnProperty("error_string"))
                if (!$util.isString(message.error_string))
                    return "error_string: string expected";
            if (message.response_string != null && message.hasOwnProperty("response_string"))
                if (!$util.isString(message.response_string))
                    return "response_string: string expected";
            if (message.update_list != null && message.hasOwnProperty("update_list")) {
                if (!Array.isArray(message.update_list))
                    return "update_list: array expected";
                for (var i = 0; i < message.update_list.length; ++i) {
                    var error = $root.aslivecasino.UpdateInfo.verify(message.update_list[i]);
                    if (error)
                        return "update_list." + error;
                }
            }
            if (message.notify_list != null && message.hasOwnProperty("notify_list")) {
                if (!Array.isArray(message.notify_list))
                    return "notify_list: array expected";
                for (var i = 0; i < message.notify_list.length; ++i) {
                    var error = $root.aslivecasino.NotifyInfo.verify(message.notify_list[i]);
                    if (error)
                        return "notify_list." + error;
                }
            }
            return null;
        };

        /**
         * Creates a Response message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof aslivecasino.Response
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {aslivecasino.Response} Response
         */
        Response.fromObject = function fromObject(object) {
            if (object instanceof $root.aslivecasino.Response)
                return object;
            var message = new $root.aslivecasino.Response();
            if (object.error_no != null)
                message.error_no = object.error_no | 0;
            if (object.error_string != null)
                message.error_string = String(object.error_string);
            if (object.response_string != null)
                message.response_string = String(object.response_string);
            if (object.update_list) {
                if (!Array.isArray(object.update_list))
                    throw TypeError(".aslivecasino.Response.update_list: array expected");
                message.update_list = [];
                for (var i = 0; i < object.update_list.length; ++i) {
                    if (typeof object.update_list[i] !== "object")
                        throw TypeError(".aslivecasino.Response.update_list: object expected");
                    message.update_list[i] = $root.aslivecasino.UpdateInfo.fromObject(object.update_list[i]);
                }
            }
            if (object.notify_list) {
                if (!Array.isArray(object.notify_list))
                    throw TypeError(".aslivecasino.Response.notify_list: array expected");
                message.notify_list = [];
                for (var i = 0; i < object.notify_list.length; ++i) {
                    if (typeof object.notify_list[i] !== "object")
                        throw TypeError(".aslivecasino.Response.notify_list: object expected");
                    message.notify_list[i] = $root.aslivecasino.NotifyInfo.fromObject(object.notify_list[i]);
                }
            }
            return message;
        };

        /**
         * Creates a plain object from a Response message. Also converts values to other types if specified.
         * @function toObject
         * @memberof aslivecasino.Response
         * @static
         * @param {aslivecasino.Response} message Response
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Response.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.arrays || options.defaults) {
                object.update_list = [];
                object.notify_list = [];
            }
            if (options.defaults) {
                object.error_no = 0;
                object.error_string = "";
                object.response_string = "";
            }
            if (message.error_no != null && message.hasOwnProperty("error_no"))
                object.error_no = message.error_no;
            if (message.error_string != null && message.hasOwnProperty("error_string"))
                object.error_string = message.error_string;
            if (message.response_string != null && message.hasOwnProperty("response_string"))
                object.response_string = message.response_string;
            if (message.update_list && message.update_list.length) {
                object.update_list = [];
                for (var j = 0; j < message.update_list.length; ++j)
                    object.update_list[j] = $root.aslivecasino.UpdateInfo.toObject(message.update_list[j], options);
            }
            if (message.notify_list && message.notify_list.length) {
                object.notify_list = [];
                for (var j = 0; j < message.notify_list.length; ++j)
                    object.notify_list[j] = $root.aslivecasino.NotifyInfo.toObject(message.notify_list[j], options);
            }
            return object;
        };

        /**
         * Converts this Response to JSON.
         * @function toJSON
         * @memberof aslivecasino.Response
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Response.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return Response;
    })();

    aslivecasino.Request = (function() {

        /**
         * Properties of a Request.
         * @memberof aslivecasino
         * @interface IRequest
         * @property {string|null} [json_command] Request json_command
         */

        /**
         * Constructs a new Request.
         * @memberof aslivecasino
         * @classdesc Represents a Request.
         * @implements IRequest
         * @constructor
         * @param {aslivecasino.IRequest=} [properties] Properties to set
         */
        function Request(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Request json_command.
         * @member {string} json_command
         * @memberof aslivecasino.Request
         * @instance
         */
        Request.prototype.json_command = "";

        /**
         * Creates a new Request instance using the specified properties.
         * @function create
         * @memberof aslivecasino.Request
         * @static
         * @param {aslivecasino.IRequest=} [properties] Properties to set
         * @returns {aslivecasino.Request} Request instance
         */
        Request.create = function create(properties) {
            return new Request(properties);
        };

        /**
         * Encodes the specified Request message. Does not implicitly {@link aslivecasino.Request.verify|verify} messages.
         * @function encode
         * @memberof aslivecasino.Request
         * @static
         * @param {aslivecasino.IRequest} message Request message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Request.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.json_command != null && message.hasOwnProperty("json_command"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.json_command);
            return writer;
        };

        /**
         * Encodes the specified Request message, length delimited. Does not implicitly {@link aslivecasino.Request.verify|verify} messages.
         * @function encodeDelimited
         * @memberof aslivecasino.Request
         * @static
         * @param {aslivecasino.IRequest} message Request message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Request.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Request message from the specified reader or buffer.
         * @function decode
         * @memberof aslivecasino.Request
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {aslivecasino.Request} Request
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Request.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.aslivecasino.Request();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.json_command = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Request message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof aslivecasino.Request
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {aslivecasino.Request} Request
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Request.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Request message.
         * @function verify
         * @memberof aslivecasino.Request
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Request.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.json_command != null && message.hasOwnProperty("json_command"))
                if (!$util.isString(message.json_command))
                    return "json_command: string expected";
            return null;
        };

        /**
         * Creates a Request message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof aslivecasino.Request
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {aslivecasino.Request} Request
         */
        Request.fromObject = function fromObject(object) {
            if (object instanceof $root.aslivecasino.Request)
                return object;
            var message = new $root.aslivecasino.Request();
            if (object.json_command != null)
                message.json_command = String(object.json_command);
            return message;
        };

        /**
         * Creates a plain object from a Request message. Also converts values to other types if specified.
         * @function toObject
         * @memberof aslivecasino.Request
         * @static
         * @param {aslivecasino.Request} message Request
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Request.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults)
                object.json_command = "";
            if (message.json_command != null && message.hasOwnProperty("json_command"))
                object.json_command = message.json_command;
            return object;
        };

        /**
         * Converts this Request to JSON.
         * @function toJSON
         * @memberof aslivecasino.Request
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Request.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return Request;
    })();

    return aslivecasino;
})();

module.exports = $root;
