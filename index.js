// const root = require('./api');
const WebSocket = require('ws');

// console.log(root);

const wss = new WebSocket.Server({ port: 8080 });

wss.on('connection', function connection(ws) {

    // 剛連線進來 say hello
    ws.send('hello');

    // when client send message
    ws.on('message', function incoming(message) {
        ws.send('echo');
        console.log('received: %s', message);
    });

    // 每隔 x 秒, send to client
    let _id;
    let delay = 1000; // 毫秒
    let counterA = 1;
    let counterB = 1;
    let sw = 'A';
    let timer = function () {
        _id = setTimeout(function () {

            switch (sw) {
                case 'A':
                    ws.send('A count: ' + counterA);
                    console.log('server A count: ' + counterA);
                    counterA++;
                    if (counterA > 30) sw = 'B';
                    break;
                case 'B':
                    ws.send('B count: ' + counterB);
                    console.log('server B count: ' + counterB);
                    counterB++;
                    if (counterB > 10) sw = '';
                    break;
                default:
                    clearTimeout(_id);
                    break;

            }
            timer();
        }, delay);
    };
    timer();

    // when client close
    ws.on('close', function () {
        console.log('client close');
        if (!_id) clearTimeout(_id);
    });
});